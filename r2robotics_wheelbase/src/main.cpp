//
// Created by ksyy on 21/12/16.
//

#include <ros/ros.h>

#include <memory>

#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

#include "motor_Controller.h"
#include "Servo_Controller.h"
#include "r2robotics_srvs/set_position.h"
#include "r2robotics_msgs/Position.h"


ros::Publisher setPositionPub;
bool setPosition( r2robotics_srvs::set_position::Request  &req,
                  r2robotics_srvs::set_position::Response &answer )
{
	static r2robotics_msgs::Position positionMsg{};

	positionMsg.x = req.x;
	positionMsg.y = req.y;
	positionMsg.theta = req.theta;

	setPositionPub.publish( positionMsg );

	return true;
}


int main( int argc, char** argv )
{
	ros::init( argc, argv, "lowLevelNode" );
	auto n = std::make_unique< ros::NodeHandle >();

	MotorController motorsController{};
	motorsController.registerServices( n.get() );

	ServoController servoController{};
	servoController.registerServices( n.get() );

	setPositionPub = n->advertise< r2robotics_msgs::Position >( "setPosition", 2 );
	auto heartbeatPub = n->advertise< std_msgs::Empty >( "heartbeat", 1 );
	auto setPositionSrv{ n->advertiseService( "setPosition", &setPosition ) };


	ros::Rate loop_rate( 100 );

	std_msgs::Empty empty{};

	while( ros::ok() )
	{
		heartbeatPub.publish( empty );

		ros::spinOnce();

		loop_rate.sleep();
	}

	return 0;
}
