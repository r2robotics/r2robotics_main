//
// Created by ksyy on 09/01/17.
//


#include "motor_Controller.h"
#include <vector>
#include <algorithm>


MotorController::MotorController()
		: _nodeHandle{ nullptr }
		, _motorsControlTopic{ "motors_speed_request" }
		, _motorsFeedbackRequestTopic{ "motors_feedback_rq" }
    , _motorsFeedbackAnswerTopic{ "motors_feedback_ans" }
{
}
void MotorController::registerServices( ros::NodeHandle *nh )
{
	_nodeHandle = nh;

	_motorsControl = _nodeHandle->advertise< r2robotics_msgs::motors_speed_request >( _motorsControlTopic, 1 );
	_motorsFeedbackRequest = _nodeHandle->advertise< r2robotics_msgs::motors_feedback_request >( _motorsFeedbackRequestTopic, 1 );
	_motorsFeedbackAnswer = _nodeHandle->subscribe< r2robotics_msgs::motors_feedback_answer >( _motorsFeedbackAnswerTopic, 10,
	                                                                                 &MotorController::processAnswer, this );

	_servers.push_back( _nodeHandle->advertiseService( "motorControl", &MotorController::control, this ) );
	_servers.push_back( _nodeHandle->advertiseService( "motorDisable", &MotorController::disable, this ) );
	_servers.push_back( _nodeHandle->advertiseService( "motorGetSpeed", &MotorController::getSpeed, this ) );
	_servers.push_back( _nodeHandle->advertiseService( "motorGetTemp", &MotorController::getTemp, this ) );

	ROS_INFO( "Services advertised." );
}

bool MotorController::control( r2robotics_srvs::motors_control::Request  &req,
                               r2robotics_srvs::motors_control::Response &answer )
{
	if( req.idMotors.size() != req.CW.size() ||
			req.idMotors.size() != req.speeds.size() )
		return false;

	_motorsSpeedRequestMsg.idMotors = req.idMotors;
	_motorsSpeedRequestMsg.enables.resize( req.idMotors.size() );
	std::fill( _motorsSpeedRequestMsg.enables.begin(), _motorsSpeedRequestMsg.enables.end(), true );
	_motorsSpeedRequestMsg.directions = req.CW;
	_motorsSpeedRequestMsg.speeds = req.speeds;

	_motorsControl.publish( _motorsSpeedRequestMsg );

	return true;
}

bool MotorController::disable( r2robotics_srvs::motors_disable::Request  &req,
                               r2robotics_srvs::motors_disable::Response &answer )
{
	_motorsSpeedRequestMsg.idMotors = req.idMotors;
	_motorsSpeedRequestMsg.enables.resize( req.idMotors.size() );
	std::fill( _motorsSpeedRequestMsg.enables.begin(), _motorsSpeedRequestMsg.enables.end(), false );

	_motorsControl.publish( _motorsSpeedRequestMsg );

	return true;
}

bool MotorController::getSpeed( r2robotics_srvs::motors_speed::Request  &req,
                                r2robotics_srvs::motors_speed::Response &answer )
{
	_lastAnswerReceived.clear();

	_motorsFeedbackRequestMsg.idMotors = req.idMotors;
	_motorsFeedbackRequestMsg.tempOrSpeed = false;
	_motorsFeedbackRequest.publish( _motorsFeedbackRequestMsg );

	// Spins the node to get the message (ros::topic::waitForMessage is too slow to set up and miss the answer)
	ros::Rate loopRate( 250 );
	for( int i{ 0 } ; i < 10 && _lastAnswerReceived.size() < req.idMotors.size() ; ++i )
	{
		ros::spinOnce();
		loopRate.sleep();
	}

	if( _lastAnswerReceived.size() == req.idMotors.size() )
	{
		answer.speeds = _lastAnswerReceived;
		return true;
	}

	return false;
}

bool MotorController::getTemp( r2robotics_srvs::motors_temp::Request  &req,
                               r2robotics_srvs::motors_temp::Response &answer )
{
	_lastAnswerReceived.clear();

	_motorsFeedbackRequestMsg.idMotors = req.idMotors;
	_motorsFeedbackRequestMsg.tempOrSpeed = true;
	_motorsFeedbackRequest.publish( _motorsFeedbackRequestMsg );

	// Spins the node to get the message (ros::topic::waitForMessage is too slow to set up and miss the answer)
	ros::Rate loopRate( 250 );
	for( int i{ 0 } ; i < 10 && _lastAnswerReceived.size() < req.idMotors.size() ; ++i )
	{
		ros::spinOnce();
		loopRate.sleep();
	}

	if( _lastAnswerReceived.size() == req.idMotors.size() )
	{
		answer.celsius.resize( req.idMotors.size() );
		std::transform( _lastAnswerReceived.begin(), _lastAnswerReceived.end(), answer.celsius.begin(),
		                []( const uint16_t &temp ) { return static_cast< float >( temp ) / 100.f; } );
		return true;
	}

	return true;
}

void MotorController::processAnswer( const r2robotics_msgs::motors_feedback_answer::ConstPtr& msg )
{
	_lastAnswerReceived.clear();
	std::copy( msg->feedback.begin(), msg->feedback.end(), std::back_inserter( _lastAnswerReceived ) );
}
