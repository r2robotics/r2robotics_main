//
// Created by ksyy on 07/02/17.
//

#ifndef ROS_HIGH_LEVEL_CONTROL_H
#define ROS_HIGH_LEVEL_CONTROL_H

#include <ros/ros.h>

#include <vector>
#include <array>

#include "r2robotics_srvs/set_direction.h"
#include "r2robotics_srvs/set_self_rotation.h"
#include "r2robotics_srvs/set_circle.h"
#include "r2robotics_srvs/move_turrets_robot_frame.h"
#include "r2robotics_srvs/get_turrets_robot_frame.h"


class HighLevelControl
{
public:
	HighLevelControl();

	void registerServices( ros::NodeHandle *nh, std::string prefix = "" );


	bool setDirection( r2robotics_srvs::set_direction::Request  &req,
	                   r2robotics_srvs::set_direction::Response &answer );

	bool setSelfRotation( r2robotics_srvs::set_self_rotation::Request  &req,
	                      r2robotics_srvs::set_self_rotation::Response &answer );

	bool setCircle( r2robotics_srvs::set_circle::Request  &req,
	                r2robotics_srvs::set_circle::Response &answer );

	bool moveTurretsRobotFrame( r2robotics_srvs::move_turrets_robot_frame::Request  &req,
	                            r2robotics_srvs::move_turrets_robot_frame::Response &answer );

	bool getTurretsRobotFrame( r2robotics_srvs::get_turrets_robot_frame::Request  &req,
	                           r2robotics_srvs::get_turrets_robot_frame::Response &answer );

private:

	void robot_frame_to_turrets( std::vector< float > &angles );

	void turrets_to_robot_frame( std::vector< float > &angles );


	std::vector< ros::ServiceServer > _servers;

	ros::NodeHandle* _nodeHandle;
	std::string _prefix;
};



#endif //ROS_HIGH_LEVEL_CONTROL_H
