//
// Created by ksyy on 07/02/17.
//

#include "high_level_control.h"

#include "../common/servomotors_calibration.h"
#include "../common/common_operators.h"
#include "../common/odo_parameters.h"

#include "r2robotics_srvs/move_simultaneous.h"
#include "r2robotics_srvs/get_position.h"


HighLevelControl::HighLevelControl()
	: _servers{}
  , _nodeHandle{ nullptr }
{}


void HighLevelControl::registerServices( ros::NodeHandle *nh, std::string prefix )
{
	_nodeHandle = nh;
	_prefix = prefix;

	_servers.push_back( _nodeHandle->advertiseService( _prefix + "setDirection", &HighLevelControl::setDirection, this ) );
	_servers.push_back( _nodeHandle->advertiseService( _prefix + "setSelfRotation", &HighLevelControl::setSelfRotation, this ) );
	_servers.push_back( _nodeHandle->advertiseService( _prefix + "setCircle", &HighLevelControl::setCircle, this ) );
	_servers.push_back( _nodeHandle->advertiseService( _prefix + "moveTurretsRobotFrame", &HighLevelControl::moveTurretsRobotFrame, this ) );
	_servers.push_back( _nodeHandle->advertiseService( _prefix + "getTurretsRobotFrame", &HighLevelControl::getTurretsRobotFrame, this ) );
}

bool HighLevelControl::setDirection( r2robotics_srvs::set_direction::Request &req,
                                     r2robotics_srvs::set_direction::Response &answer )
{
	auto client = _nodeHandle->serviceClient< r2robotics_srvs::move_simultaneous >( _prefix + "moveSimultaneous" );

	r2robotics_srvs::move_simultaneous srv{};
	srv.request.time = 500;
	srv.request.idServos = std::vector< uint8_t >{ 1, 2, 3 };

	std::vector< float > angles{ req.angle, req.angle, req.angle };

	robot_frame_to_turrets( angles );
	srv.request.angles = std::vector< float >{ angles };

	turrets_to_robot_frame( angles );
	answer.angles = std::vector< float >{ angles };

	if( !client.call( srv ) )
		return false;

	return true;
}

bool HighLevelControl::setSelfRotation( r2robotics_srvs::set_self_rotation::Request &req,
                                        r2robotics_srvs::set_self_rotation::Response &answer )
{
	auto client = _nodeHandle->serviceClient< r2robotics_srvs::move_simultaneous >( _prefix + "moveSimultaneous" );

	r2robotics_srvs::move_simultaneous srv{};
	srv.request.time = 500;
	srv.request.idServos = std::vector< uint8_t >{ 1, 2, 3 };
	srv.request.angles = std::vector< float >{ 99.27f, 0.f, -99.27f };

	if( !client.call( srv ) )
		return false;

	return true;
}

bool HighLevelControl::setCircle( r2robotics_srvs::set_circle::Request &req,
                                  r2robotics_srvs::set_circle::Response &answer )
{
	double xCentre{ req.radiusFromCentre * std::cos( R2Robotics::degToRad( req.angleFromCentre ) ) },
	       yCentre{ req.radiusFromCentre * std::sin( R2Robotics::degToRad( req.angleFromCentre ) ) };

	auto client = _nodeHandle->serviceClient< r2robotics_srvs::move_turrets_robot_frame >( _prefix + "moveTurretsRobotFrame" );

	r2robotics_srvs::move_turrets_robot_frame srv{};
	srv.request.time = 500;

	srv.request.angles = std::vector< float >{};
	srv.request.angles.push_back( R2Robotics::constrainModulo( -180., 180., R2Robotics::radToDeg( std::atan2( yCentre - GR_TURRET_Y[ 0 ], xCentre - GR_TURRET_X[ 0 ] ) ) + 90. ) );
	srv.request.angles.push_back( R2Robotics::constrainModulo( -180., 180., R2Robotics::radToDeg( std::atan2( yCentre - GR_TURRET_Y[ 1 ], xCentre - GR_TURRET_X[ 1 ] ) ) + 90. ) );
	srv.request.angles.push_back( R2Robotics::constrainModulo( -180., 180., R2Robotics::radToDeg( std::atan2( yCentre - GR_TURRET_Y[ 2 ], xCentre - GR_TURRET_X[ 2 ] ) ) + 90. ) );

	if( !moveTurretsRobotFrame( srv.request, srv.response ) )
		return false;

	answer.motorFactors = std::vector< float >{};
	answer.motorFactors.push_back( ( std::abs( srv.response.angles[ 0 ] - srv.request.angles[ 0 ] ) > 1. ? -1.f : 1.f ) *
	                               std::sqrt( std::pow( xCentre - GR_TURRET_X[ 0 ], 2. ) + std::pow( yCentre - GR_TURRET_Y[ 0 ], 2. ) ) );
	answer.motorFactors.push_back( ( std::abs( srv.response.angles[ 1 ] - srv.request.angles[ 1 ] ) > 1. ? -1.f : 1.f ) *
	                               std::sqrt( std::pow( xCentre - GR_TURRET_X[ 1 ], 2. ) + std::pow( yCentre - GR_TURRET_Y[ 1 ], 2. ) ) );
	answer.motorFactors.push_back( ( std::abs( srv.response.angles[ 2 ] - srv.request.angles[ 2 ] ) > 1. ? -1.f : 1.f ) *
	                               std::sqrt( std::pow( xCentre - GR_TURRET_X[ 2 ], 2. ) + std::pow( yCentre - GR_TURRET_Y[ 2 ], 2. ) ) );

	return true;
}

bool HighLevelControl::moveTurretsRobotFrame( r2robotics_srvs::move_turrets_robot_frame::Request &req,
                                              r2robotics_srvs::move_turrets_robot_frame::Response &answer )
{
	if( req.angles.size() != 3 )
		return false;


	auto client = _nodeHandle->serviceClient< r2robotics_srvs::move_simultaneous >( _prefix + "moveSimultaneous" );
	answer.angles = std::vector< float >{};

	r2robotics_srvs::move_simultaneous srv{};
	srv.request.time = req.time;
	srv.request.idServos = std::vector< uint8_t >{ 1, 2, 3 };
	srv.request.angles = std::vector< float >{};

	std::vector< float > angles{ req.angles[ 0 ], req.angles[ 1 ], req.angles[ 2 ] };

	robot_frame_to_turrets( angles );
	srv.request.angles = std::vector< float >{ angles };

	turrets_to_robot_frame( angles );
	answer.angles = std::vector< float >{ angles };

	if( !client.call( srv ) )
		return false;

	return true;
}

bool HighLevelControl::getTurretsRobotFrame( r2robotics_srvs::get_turrets_robot_frame::Request &req,
                                             r2robotics_srvs::get_turrets_robot_frame::Response &answer )
{
	auto client = _nodeHandle->serviceClient< r2robotics_srvs::get_position >( _prefix + "getPosition" );

	answer.angles = std::vector< float >{};

	r2robotics_srvs::get_position srv{};

	std::vector< float > angles{ 0.f, 0.f, 0.f };

	for( auto id : std::array< int, 3 >{ 1, 2, 3 } )
	{
		srv.request.idServo = id;
		if( !client.call( srv ) )
			return false;

		angles[ id - 1 ] = srv.response.angle;
	}

	turrets_to_robot_frame( angles );
	answer.angles = std::vector< float >{ angles };

	return true;
}

void HighLevelControl::robot_frame_to_turrets( std::vector< float > &angles )
{
	angles[ 0 ] = - R2Robotics::constrainModulo( -140.f, 140.f,
	                                           angles[ 0 ] - static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 0 ] ),
	                                           180.f );
	angles[ 1 ] = - R2Robotics::constrainModulo( -140.f, 140.f,
	                                           angles[ 1 ] - static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 1 ] ),
	                                           180.f );
	angles[ 2 ] = - R2Robotics::constrainModulo( -140.f, 140.f,
	                                           angles[ 2 ] - static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 2 ] ),
	                                           180.f );
}

void HighLevelControl::turrets_to_robot_frame( std::vector< float > &angles )
{
	angles[ 0 ] = - R2Robotics::constrainModulo( -180.f, 180.f,
	                                           angles[ 0 ] + static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 0 ] ) );
	angles[ 1 ] = - R2Robotics::constrainModulo( -180.f, 180.f,
	                                           angles[ 1 ] + static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 1 ] ) );
	angles[ 2 ] = - R2Robotics::constrainModulo( -180.f, 180.f,
	                                           angles[ 2 ] + static_cast< float >( GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG[ 2 ] ) );
}


int main( int argc, char** argv )
{
	ros::init( argc, argv, "HighLevelControl" );
	auto n = std::make_unique< ros::NodeHandle >();

	HighLevelControl highLevelControl{};
	highLevelControl.registerServices( n.get() );

	ros::spin();

	return 0;
}
