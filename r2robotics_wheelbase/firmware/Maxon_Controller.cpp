#include "Maxon_Controller.h"

Motor::Motor( const uint8_t enablePin, const uint8_t speedPin, const uint8_t directionPin, const uint8_t stopPin,
              const uint8_t tempPin, const uint8_t actualSpeedPin )
		: _enablePin{ enablePin }
		, _speedPin{ speedPin }
		, _directionPin{ directionPin }
		, _stopPin{ stopPin }
		, _tempPin{ tempPin }
		, _actualSpeedPin{ actualSpeedPin }
{
	pinMode( _enablePin, OUTPUT );
	pinMode( _speedPin, OUTPUT );
	pinMode( _directionPin, OUTPUT );
	pinMode( _stopPin, OUTPUT );
	
	analogWriteResolution( PWM_RESOLUTION );
	analogReadResolution( ANALOG_RESOLUTION ); // anything more seemed like overkill
	analogReadAveraging( ANALOG_SAMPLES_AVERAGING );
	
//	enable();
//	unstop();
	setSpeed( 0 );
	setDirection( Direction::CW );
}

void Motor::enable()
{
	_enabled = true;
	digitalWrite( _enablePin, HIGH );
}

void Motor::disable()
{
	_enabled = false;
	digitalWrite( _enablePin, LOW );
}

bool Motor::isEnabled()
{
	return _enabled;
}

void Motor::stop()
{
	_stopped = true;
	digitalWrite( _stopPin, HIGH );
}

void Motor::unstop()
{
	_stopped = false;
	digitalWrite( _stopPin, LOW );
}

bool Motor::isStopped()
{
	return _stopped;
}

uint16_t Motor::setSpeed( const uint32_t speed)
{
	if( ( speed < SPEED_VALUE_MIN ) || ( speed > SPEED_VALUE_MAX ) )
		return -1; // speed not in valid range, quitting
	
	uint16_t speedPwm{ static_cast< uint16_t >( ( speed * RANGE( TEENSY_MAX_SPEED_PWM, TEENSY_MIN_SPEED_PWM ) ) /
	                                            RANGE( SPEED_VALUE_MAX, SPEED_VALUE_MIN ) +
	                                            TEENSY_MIN_SPEED_PWM ) }; // PWM value between 0 and TEENSY_MAX_PWM
	
	analogWrite( _speedPin, speedPwm );
	
	return speedPwm;
}

void Motor::setDirection( const Direction newDirection )
{
	_direction = newDirection;
	
	digitalWrite( _directionPin, _direction == Direction::CCW ? HIGH : LOW );
}

bool Motor::getDirection()
{
	return _direction;
}

uint16_t Motor::getActualSpeed()
{
	uint32_t readValue{ static_cast< uint32_t >( analogRead( _actualSpeedPin ) ) };
	uint32_t actualSpeedMillivolt{ ( readValue * ANALOG_MAX_MILLIVOLTS ) / ANALOG_MAX_VALUE };
	if( actualSpeedMillivolt < ACTUAL_SPEED_VOLTAGE_MIN ) // Negative Speed
		actualSpeedMillivolt = ACTUAL_SPEED_VOLTAGE_MAX - actualSpeedMillivolt;
	
	uint16_t actualSpeedRpm{ static_cast< uint16_t >( ( ( actualSpeedMillivolt - ACTUAL_SPEED_VOLTAGE_MIN ) *
	                                                    RANGE( ACTUAL_SPEED_VALUE_MAX, ACTUAL_SPEED_VALUE_MIN ) ) /
	                                                  RANGE( ACTUAL_SPEED_VOLTAGE_MAX, ACTUAL_SPEED_VOLTAGE_MIN ) ) };
	
	return actualSpeedRpm;
}

uint16_t Motor::getTemp()
{
	uint32_t readValue{ static_cast< uint32_t >( analogRead( _tempPin ) ) };
	uint32_t temperatureMillivolt{ ( readValue * ANALOG_MAX_MILLIVOLTS ) / ANALOG_MAX_VALUE };
	uint16_t temperatureCelsius{ static_cast< uint16_t >( ( temperatureMillivolt *
	                                                        RANGE( TEMP_VALUE_MAX, TEMP_VALUE_MIN ) ) /
	                                                      RANGE( TEMP_VOLTAGE_MAX, TEMP_VOLTAGE_MIN ) ) };
	
	return temperatureCelsius;
}
