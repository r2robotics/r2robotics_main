//
// Created by ksyy on 10/01/17
// Modified by Ksyy on 03/03/18
//

//#include "position_computing.h"
#include "../common/common_operators.h"
#include <math.h>



template< unsigned int nbTurrets >
PositionComputing< nbTurrets >::OdoTurret::OdoTurret( const mm odo1Gain, const mm odo1DistFromCentre, 
													  const mm odo2Gain, const mm odo2DistFromCentre )
	: _x{}, _y{}, _angle{}
	, _tempDistTotalOdo{ 0, 0 }
	, _odoGain{ odo1Gain, odo2Gain }
	, _odoDistFromCentre{ odo1DistFromCentre, odo2DistFromCentre }
	, _odoCurrentStep{ 0, 0 }, _odoLastStep{ 0, 0 }
	, _odoTotalTicks{ 0, 0 }
{}

template< unsigned int nbTurrets >
bool PositionComputing< nbTurrets >::OdoTurret::computePosition( const std::array< int32_t, 2 > values )
{
    for( int i : { 0, 1 } )
	{
		_odoLastStep[ i ] = _odoCurrentStep[ i ];
		if( R2Robotics::absoluteValue( values[ i ] - _odoCurrentStep[ i ] ) >= 3 )
			_odoCurrentStep[ i ] = values[ i ];
	}
	
	if( _odoLastStep[ 0 ] == _odoCurrentStep[ 0 ] && _odoLastStep[ 1 ] == _odoCurrentStep[ 1 ] )
		return false;

	// Total distance traveled by the wheel
	std::array< mm, 2 > distOdos{};
	for( int i : { 0, 1 } )
	{
		int32_t ticks{ R2Robotics::constrainModulo( static_cast< int32_t >( -512 ),
	                                                static_cast< int32_t >( 512 ),
	                                                _odoCurrentStep[ i ] - _odoLastStep[ i ] ) };
		_odoTotalTicks[ i ] += ticks;
		distOdos[ i ] = _odoGain[ i ] * ticks;
	}

	mm delta{ ( distOdos[ 0 ] + distOdos[ 1 ] ) / 2. };
	rad deltaAngle{ ( - distOdos[ 0 ] / _odoDistFromCentre[ 0 ] + distOdos[ 1 ] / _odoDistFromCentre[ 1 ] ) / 2. };

	// Computes the wheel new coordinates
	// https://fr.wikipedia.org/wiki/Segment_circulaire
	if( deltaAngle > 0.000001 )
		delta = ( delta * 2. * sin( deltaAngle / 2. ) ) / deltaAngle;

	mm deltaX{ delta * cos( _angle + deltaAngle / 2. ) };
	mm deltaY{ delta * sin( _angle + deltaAngle / 2. ) };

	_tempDistTotalOdo[ 0 ] += distOdos[ 0 ];
	_tempDistTotalOdo[ 1 ] += distOdos[ 1 ];

	// Computes the wheel new coordinates in the table frame
	_x += deltaX;
	_y += deltaY;
	_angle = R2Robotics::constrainModulo( -3.141592653, 3.141592653, _angle + deltaAngle );

	return true;
}


template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::OdoTurret::getX() const
{
	return _x;
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::OdoTurret::getY() const
{
	return _y;
}

template< unsigned int nbTurrets >
const rad PositionComputing< nbTurrets >::OdoTurret::getAngle() const
{
	return _angle;
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::OdoTurret::getOdoDistSinceLastCall( const size_t i )
{
	mm temp{ _tempDistTotalOdo[ i ] };
	_tempDistTotalOdo[ i ] = 0.0;
	return temp;
}

template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::OdoTurret::setX( const mm x )
{
	_x = x;
}

template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::OdoTurret::setY( const mm y )
{
	_y = y;
}

template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::OdoTurret::setAngle( const rad angle )
{
	_angle = R2Robotics::constrainModulo( -3.141592653, 3.141592653, angle, 0.0 );
}


template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::OdoTurret::resetTotalTicksTravelled()
{
	_odoTotalTicks[ 0 ] = 0;
	_odoTotalTicks[ 1 ] = 0;
}

template< unsigned int nbTurrets >
const int32_t PositionComputing< nbTurrets >::OdoTurret::getTotalTicksTravelled( const size_t i ) const
{
	return _odoTotalTicks[ i ];
}

template< unsigned int nbTurrets >
const int32_t PositionComputing< nbTurrets >::OdoTurret::getOdoCurrentStep( const size_t i ) const
{
	return _odoCurrentStep[ i ];
}

//**********

template<>
PositionComputing< 1 >::PositionComputing()
	: _x{ 0 }
	, _y{ 0 }
	, _theta{ 0 }
	, _odoChain{ ODO_CHAIN_CLK, ODO_CHAIN_CS, ODO_CHAIN_DO }
	, _turrets{ make_turret( 0, 
							 PMI_ODO_GAIN, PMI_ODO_DIST_FROM_CENTRE, 
							 std::array< mm, 1 >{ 0 }, std::array< mm, 1 >{ 0 }, std::array< rad, 1 >{ 0/*3.141592653*/ } ) }
{
	_odoChain.begin();
	_odoChain.read();

	std::get< 0 >( _turrets[ 0 ] ).computePosition( std::array< int32_t, 2 >{ _odoChain.value( 0 ), _odoChain.value( 1 ) } );
	std::get< 0 >( _turrets[ 0 ] ).setX( 0 );
	std::get< 0 >( _turrets[ 0 ] ).setY( 0 );
	std::get< 0 >( _turrets[ 0 ] ).setAngle( 0/*.141592653*/ );
	std::get< 0 >( _turrets[ 0 ] ).resetTotalTicksTravelled();

	centreTurrets();
}

template<>
PositionComputing< 3 >::PositionComputing()
	: _x{ 0 }
	, _y{ 0 }
	, _theta{ 0 }
	, _odoChain{ ODO_CHAIN_CLK, ODO_CHAIN_CS, ODO_CHAIN_DO }
	, _turrets{ make_turret( 0,
							 GR_ODO_GAIN, GR_ODO_DIST_FROM_CENTRE, 
							 GR_TURRET_X, GR_TURRET_Y, GR_TURRET_INIT_YAW_ROBOT_FRAME_RAD ),
				make_turret( 1,
							 GR_ODO_GAIN, GR_ODO_DIST_FROM_CENTRE, 
							 GR_TURRET_X, GR_TURRET_Y, GR_TURRET_INIT_YAW_ROBOT_FRAME_RAD ),
				make_turret( 2,
							 GR_ODO_GAIN, GR_ODO_DIST_FROM_CENTRE, 
							 GR_TURRET_X, GR_TURRET_Y, GR_TURRET_INIT_YAW_ROBOT_FRAME_RAD ) }
{
	_odoChain.begin();
	_odoChain.read();

	for( int i{ 0 } ; i < 3 ; ++i )
	{
		std::get< 0 >( _turrets[ i ] ).computePosition( std::array< int32_t, 2 >{ _odoChain.value( i * 2 ), _odoChain.value( i * 2 + 1 ) } );
		std::get< 0 >( _turrets[ i ] ).setX( GR_TURRET_X[ i ] );
		std::get< 0 >( _turrets[ i ] ).setY( GR_TURRET_Y[ i ] );
		std::get< 0 >( _turrets[ i ] ).setAngle( GR_TURRET_INIT_YAW_ROBOT_FRAME_RAD[ i ] );
		std::get< 0 >( _turrets[ i ] ).resetTotalTicksTravelled();
	}

	auto [x, y] = centreTurrets();
}

template< unsigned int nbTurrets >
typename PositionComputing< nbTurrets >::Turret 
PositionComputing< nbTurrets >::make_turret( const int id,
											 const auto gains, const auto dists, 
											 const auto Xs, const auto Ys, const auto initYaws )
{
	// Odometers' id in the chain and in the arrays
	int id1{ id * 2 }, id2{ id * 2 + 1 };

	mm distCentre{ std::sqrt( std::pow( Xs[ id ], 2.0 ) + std::pow( Ys[ id ], 2.0 ) ) };
	rad angleCentre{ std::atan2( Ys[ id ], Xs[ id ] ) };

	auto turret{ std::make_tuple( OdoTurret{ gains[ id1 ], dists[ id1 ], 
											 gains[ id2 ], dists[ id2 ] },
						  				 	 initYaws[ id ], distCentre, angleCentre ) };
	
	return turret;
}

template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::setPosition( const mm x, const mm y, const rad theta )
{
	std::array< rad, nbTurrets > angles{};
	for( int i{ 0 } ; i < _turrets.size() ; ++i ) angles[ i ] = getTurretAngleDiff( i );
	
	_x = x;
	_y = y;
	_theta = theta;
	
	for( auto& turret : _turrets )
	{
		std::get< 0 >( turret ).setX( x + std::get< 2 >( turret ) * cos( theta + std::get< 3 >( turret ) ) );
		std::get< 0 >( turret ).setY( y + std::get< 2 >( turret ) * sin( theta + std::get< 3 >( turret ) ) );
	}

	for( int i{ 0 } ; i < _turrets.size() ; ++ i) 
		setTurretAngleDiff( angles[ i ], i );
}

template< unsigned int nbTurrets >
bool PositionComputing< nbTurrets >::computePosition()
{
	_odoChain.read();

	for( int i{ 0 } ; i < nbTurrets * 2 ; ++i )
		if( !_odoChain.valid( i ) )
			return false;

	bool compute{ false };
	for( int i{ 0 } ; i < nbTurrets ; ++i )
		compute = compute | std::get< 0 >( _turrets[ i ] ).computePosition( std::array< int32_t, 2 >{ _odoChain.value( i * 2 ), _odoChain.value( i * 2 + 1 ) } );

	if( !compute )
		return true;

	auto [x, y] = centreTurrets();
	_x = x; 
	_y = y;

	if( nbTurrets == 3 )
	{
		std::array< rad, nbTurrets > angles{};
		for( int i{ 0 } ; i < 3 ; ++i ) 
			angles[ i ] = std::atan2( std::get< 0 >( _turrets[ i ] ).getY() - y, std::get< 0 >( _turrets[ i ] ).getX() - x) - std::get< 3 >( _turrets[ i ] );
		_theta = std::atan2( sin( angles[ 0 ] ) + sin( angles[ 1 ] ) + sin( angles[ 2 ] ), cos( angles[ 0 ] ) + cos( angles[ 1 ] ) + cos( angles[ 2 ] ) );
	}
	else
	{
		//_theta = R2Robotics::constrainModulo( -3.141592653, 3.141592653, std::get< 0 >( _turrets[ 0 ] ).getAngle() - std::get< 1 >( _turrets[ 0 ] ), 0.0 );
		_theta = std::get< 0 >( _turrets[ 0 ] ).getAngle();
		return true;
	}

	for( int i{ 0 } ; i < 3 ; ++i )
	{
		if( R2Robotics::absoluteValue( std::sqrt( std::pow( std::get< 0 >( _turrets[ i ] ).getX() - _x, 2. ) + 
												  std::pow( std::get< 0 >( _turrets[ i ] ).getY() - _y, 2. ) ) -
									   std::get< 2 >( _turrets[ i ] ) ) > 2. )
		{
			setPosition( _x, _y, _theta );
			return true;
		}
	}

	return true;
}


template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getX() const
{
	return _x;
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getY() const
{
	return _y;
}

template< unsigned int nbTurrets >
const rad PositionComputing< nbTurrets >::getTheta() const
{
	return _theta;
}


template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getTurretX( const size_t i ) const
{
	return std::get< 0 >( _turrets[ i ] ).getX();
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getTurretY( const size_t i ) const
{
	return std::get< 0 >( _turrets[ i ] ).getY();
}

template< unsigned int nbTurrets >
const rad PositionComputing< nbTurrets >::getTurretAngleAbsolute( const size_t i ) const
{
	return std::get< 0 >( _turrets[ i ] ).getAngle();
}


template<>
const rad PositionComputing< 1 >::getTurretAngleDiff( const size_t i ) const	
{
	return 0;//.141592653;
}

template<>
const rad PositionComputing< 3 >::getTurretAngleDiff( const size_t i ) const  // Angle given by the servomotor, Angle diff = Angle absolute - Angle wheelbase
{
	return R2Robotics::constrainModulo( -3.141592653, 3.141592653, std::get< 0 >( _turrets[ i ] ).getAngle() - ( std::get< 1 >( _turrets[ i ] ) + _theta ), 0.0 );
}

template<>
void PositionComputing< 1 >::setTurretAngleDiff( const rad angle, const size_t i )
{
	std::get< 0 >( _turrets[ i ] ).setAngle( /*std::get< 1 >( _turrets[ i ] ) + */_theta );
}

template<>
void PositionComputing< 3 >::setTurretAngleDiff( const rad angle, const size_t i )
{
	std::get< 0 >( _turrets[ i ] ).setAngle( angle + std::get< 1 >( _turrets[ i ] ) + _theta );
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getOdoDistSinceLastCall( const size_t i )
{
	return std::get< 0 >( _turrets[ i / 2 ] ).getOdoDistSinceLastCall( i % 2 );
}

template< unsigned int nbTurrets >
const mm PositionComputing< nbTurrets >::getTurretDistSinceLastCall( const size_t i )
{
	return ( std::get< 0 >( _turrets[ i ] ).getOdoDistSinceLastCall( 0 ) +
	         std::get< 0 >( _turrets[ i ] ).getOdoDistSinceLastCall( 1 ) ) / 2.0;
}

template< unsigned int nbTurrets >
void PositionComputing< nbTurrets >::resetTotalTicksTravelled()
{
	for( auto& turret : _turrets )
		std::get< 0 >( turret ).resetTotalTicksTravelled();
}

template< unsigned int nbTurrets >
const int32_t PositionComputing< nbTurrets >::getTotalTicksTravelled( const size_t i ) const
{
	return std::get< 0 >( _turrets[ i / 2 ] ).getTotalTicksTravelled( i % 2 );
}

template< unsigned int nbTurrets >
const int32_t PositionComputing< nbTurrets >::getOdoCurrentStep( const size_t i ) const
{
	return _odoChain.value( i );
}

template< unsigned int nbTurrets >
const int32_t PositionComputing< nbTurrets >::getOdoStatus( const size_t i ) const
{
	return _odoChain.status( i );
}


template< unsigned int nbTurrets >
const std::tuple< mm, mm > PositionComputing< nbTurrets >::centreTurrets()
{
	if( nbTurrets == 1 )
		return { std::get< 0 >( _turrets[ 0 ] ).getX(), std::get< 0 >( _turrets[ 0 ] ).getY() };
		
	double bX{ std::get< 0 >( _turrets[ 1 ] ).getX() - std::get< 0 >( _turrets[ 0 ] ).getX() };
	double bY{ std::get< 0 >( _turrets[ 1 ] ).getY() - std::get< 0 >( _turrets[ 0 ] ).getY() };
	double cX{ std::get< 0 >( _turrets[ 2 ] ).getX() - std::get< 0 >( _turrets[ 0 ] ).getX() };
	double cY{ std::get< 0 >( _turrets[ 2 ] ).getY() - std::get< 0 >( _turrets[ 0 ] ).getY() };
	
	double d{ 2. * ( bX * cY - bY * cX ) };
	double xTemp{ cY * ( bX * bX + bY * bY ) - bY * ( cX * cX + cY * cY ) };
	double yTemp{ bX * ( cX * cX + cY * cY ) - cX * ( bX * bX + bY * bY ) };
	mm x{ xTemp / d + std::get< 0 >( _turrets[ 0 ] ).getX() };
	mm y{ yTemp / d + std::get< 0 >( _turrets[ 0 ] ).getY() };

	return { x, y };
}
