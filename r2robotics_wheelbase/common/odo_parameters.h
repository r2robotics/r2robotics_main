//
// Created by ksyy on 21/03/18.
//

#ifndef ROS_ODO_PARAMETERS_H
#define ROS_ODO_PARAMETERS_H

#include <array>

//*****
// Useful constants
//*****
const int32_t DEG_TO_MICRORAD{ 3141592 / 180 };
const int32_t HKL_MILLIDEG_PER_STEP{ 320 };
const int32_t HKL_MICRORAD_PER_STEP{ ( DEG_TO_MICRORAD * HKL_MILLIDEG_PER_STEP ) / 1000 };

//*****
// Odometers chain pins
//*****
const uint8_t ODO_CHAIN_CLK{ 0 };
const uint8_t ODO_CHAIN_CS{ 1 };
const uint8_t ODO_CHAIN_DO{ 2 };
const uint8_t ODO_CHAIN_NB{ 6 };

//*****
// PMI parameters
//*****
const std::array< double, 2 > PMI_ODO_GAIN = { -0.062112171, 0.063176562 };
const std::array< double, 2 > PMI_ODO_DIST_FROM_CENTRE = { 83.29518271, 85.50997542};

//*****
// GR parameters
//*****
const std::array< double, 6 > GR_ODO_GAIN = { -0.062326915, 0.063392392, -0.062698083, 0.062908676, -0.063009812, 0.062668439 }; // TODO : Measure gain more precisely with wheel and 3D contraption
const std::array< double, 6 > GR_ODO_DIST_FROM_CENTRE = { 29.64704813, 30.02241875, 29.86132580, 29.88942973, 29.83314058, 29.56817518 }; 
const std::array< double, 3 > GR_TURRET_X = { 27.75, -172.25, 27.75 };
const std::array< double, 3 > GR_TURRET_Y = { 170.0, 0.0, -170.0 };
const std::array< double, 3 > GR_TURRET_INIT_YAW_ROBOT_FRAME_RAD = { -3.141592 / 2.0, -3.141592 / 2.0, -3.141592 / 2.0 };
const std::array< double, 3 > GR_TURRET_INIT_YAW_ROBOT_FRAME_DEG = { 270., 270., 270. };

#endif //ROS_ODO_PARAMETERS_H
