# r2robotics_main

## Récupérer les sources

Le dépôt r2robotics_main est à cloner dans le dossier 'src' d'un catkin workspace. Pour mettre en place un catkin workspace, [suivez les instructions ici](http://wiki.ros.org/catkin/Tutorials/create_a_workspace). Version de ros utilisée actuellement : kinetic.
Pour cloner le dépôt r2robotics_main : 

``` 
git clone git@gitlab.com:r2robotics/r2robotics_main.git
```

Les subrepo sont clonés automatiquement.


## Prérequis


Requis :
 - ROS Kinetic (http://wiki.ros.org/kinetic/Installation/Ubuntu)

Packages à installer :
 - ros-kinetic-joy
 - ros-kinetic-rosserial
 - ros-kinetic-rosserial-arduino

Pour compiler le projet :
 - Installez gcc-6 et g++-6 (nécessite sûrement Ubuntu 16.04 - /!\ Surtout ne pas prendre la 16.10, ros-kinetic n'est pas disponible dessus /!\ )

    ```
    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install gcc-6 g++-6
    ```

Pour la compilation du firmware Teensy :
 - Installez Arduino SDK (https://www.arduino.cc/en/Main/Software). Si erreur ("cc.arduino.arduinoide.xml does not exist"), utiliser ce fix (https://github.com/arduino/Arduino/issues/6116), ie remplacer "RESOURCE\_NAME=cc.arduino.arduinoide" par "RESOURCE_NAME=arduino-arduinoide" dans le install.sh
 - Installez Teensyduino (http://www.pjrc.com/teensy/td_download.html)
 - Installez le compilateur : 
  

    ```
    sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
    sudo apt-get update
    sudo apt-get install gcc-arm-embedded
    ```
    

 --> Si vous avez installé gcc-arm-none-eabi, il faut le supprimer "sudo apt-get remove gcc-arm-none-eabi & sudo apt-get autoremove"
 - Si vous avez un soucis de path pour le SDK Arduino, ajoutez dans votre ~/.bashrc un "export arduino_location=/location/of/your/arduino/sdk"

## Compilation

Dans le dossier 'r2robotics_main', lancez le fichier compilation.bash (ou no_firmware_compilation.bash si vous souhaitez compiler sans le firmware) : 

```
source compilation.bash
```