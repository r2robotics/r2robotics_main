# V.Guinet, R2Robotics, 2018

# Import necessary packages
from __future__ import print_function
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy as np
import time
import cv2

from colorTools import *
	
# This function combines a list of masks of the same size (recursive OR operation)
def combineMasks(masksList):
	if len(masksList) > 2:
		firstMask = masksList.pop(0)
		masksList[0] = masksList[0] | firstMask
		return combineMasks(masksList)
	elif len(masksList) == 1:
		return masksList[0]
	else:
		return masksList[0] | masksList[1]


# Main detection function	
def detectBalls(image, greenBool, orangeBool, yellowBool):
	
	print("Preparing color masks")
	orange = np.uint8([[[35,40,210]]])
	green = np.uint8([[[65,155,120]]])
	yellow = np.uint8([[[0,184,228]]])
	greenMask = getMaskFromColor(image, green, 5, 100, 100);
	orangeMask = getMaskFromColor(image, orange, 5, 100, 100);
	yellowMask = getMaskFromColor(image, yellow, 5, 100, 150);
	
	maskslist = []
	if greenBool:
		maskslist.append(greenMask)
	if orangeBool:
		maskslist.append(orangeMask)
	if yellowBool:
		maskslist.append(yellowMask)
	mask = combineMasks(maskslist)
	print("Color masks generated")
	
	# find contours in the mask and initialize the current
	# (x, y) center of the ball
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
	centers = []
	
	nbContours = len(cnts)
	
	print("Processing ", nbContours, "  contours")
	
	ind = 0
	# only proceed if at least one contour was found
	if len(cnts) > 0:
		for i in range(0, nbContours):
			# find the largest contour in the mask, then use
			# it to compute the minimum enclosing circle and
			# centroid
			c = cnts[i]
			((x, y), radius) = cv2.minEnclosingCircle(c)
			#M = cv2.moments(c)
			#center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"])) #To use centroid instead of center
			
			# only proceed if the radius meets a minimum size
			if radius > 7.5 and radius <30:
				ind += 1
				centers.append([x, y])
				# draw the circle and centroid on the frame,
				# then update the list of tracked points
				cv2.circle(image, (int(x), int(y)), int(radius),
				(255, 255, 255), 5)
				cv2.circle(image, (int(x), int(y)), 5, (255, 255, 255), -1)
				#cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
				#cv2.imshow("Image", image)
				#cv2.waitKey(0)
	
	print(ind, " valid contours")
	
	return centers, image
	
def detectBallsFromHsvColor(image, hsvColor, clearances, verbose):
    
    # Color mask
    if verbose:
        print("Preparing color mask")
    mask = getMaskFromHSVColor(image, hsvColor, clearances[0], clearances[1], clearances[2])
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", mask)
    cv2.waitKey(0)
    if verbose:
        print("Color masks generated")
    
    # find contours in the mask
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    nbContours = len(cnts)
    
    print("Processing ", nbContours, "  contours")
    centers = []
    ind = 0
    # only proceed if at least one contour was found
    if len(cnts) > 0:
        for i in range(0, nbContours):
			# find the largest contour in the mask, then use
			# it to compute the minimum enclosing circle and
			# centroid
            c = cnts[i]
            contourArea = cv2.contourArea(c)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            circleArea = np.pi*radius*radius
            
            #M = cv2.moments(c)
			#center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"])) #To use centroid instead of center
			
			# only proceed if the radius meets a minimum size
            if radius > 12 and radius <60:
                print(contourArea, circleArea)
                if float(circleArea)/float(contourArea) > 0.5 and float(circleArea)/float(contourArea) < 1.5:
                    ind += 1
                    centers.append([x, y])
                    # draw the circle and centroid on the frame,
                    # then update the list of tracked points
                    cv2.circle(image, (int(x), int(y)), int(radius),(255, 255, 255), 5)
                    cv2.circle(image, (int(x), int(y)), 5, (255, 255, 255), -1)
    
    print(ind, " valid contours")
    return centers, image
	
