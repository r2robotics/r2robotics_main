# V.Guinet, R2Robotics, 2018

# Import necessary packages
import cv2
import numpy as np
from calibrationTools import *

# Parameters
picturesDir = "./pictures_rot_newRES2/"
chessboardSizeTuple = (7,6)
parametersFile = "./newCameraOutput"
calculatePrecision = True
displayImage = True

print("Image-taking loop")
try:
    takePicturesUntilStop(False, picturesDir, True)
except :
    print("Ended picture-taking loop")
    pass
print("Time to go remove bad pictures in folder " + picturesDir)
input("Press Enter to launch camera intrinsic parameters determination...")

print("Camera calibration")
determineCameraParameters(picturesDir, chessboardSizeTuple, parametersFile, calculatePrecision, displayImage)

print("Done.")
print("'calibration_camera_test.py' EOF")
