import time
import cv2
import numpy as np
from pictureTools import *

image = getDefaultBaliseImage()

cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.imshow("Image", image)

cv2.waitKey(0)
