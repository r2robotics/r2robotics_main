# V.Guinet, R2Robotics, 2018

# Import necessary packages
import cv2
import time
import glob
import numpy as np
import os
from pictureTools import *

# This function is supposed to perform camera intrinsic parameters determination
# picturesDir is the folder with all the pictures to use
# chessboardSizeTuple is (7,6) if a 7x6 chessboard grid is used
# parametersFile shall be a filepath that will be used for storing the camera parameters
# calculatePrecision is a boolean that activates precision calculation at the end of determination
# displayImage is a boolean to display the images with chessboard detection layer
def determineCameraParameters(picturesDir, chessboardSizeTuple, parametersFile, calculatePrecision, displayImage):
    # Preparation
    if not os.path.isdir(picturesDir):
        print("Input pictures directory doesn't exist")
        return None
    xChess = chessboardSizeTuple[0]
    yChess = chessboardSizeTuple[1]
    
    #termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((xChess*yChess,3), np.float32)
    objp[:,:2] = np.mgrid[0:xChess,0:yChess].T.reshape(-1,2)
    
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    
    images = glob.glob(picturesDir + '*.jpg')
    imageIndex = 1
    print("...processing " + str(len(images)) + " images")
    
    for fname in images:
        ti = time.time()
        print("\timage " + str(imageIndex) + "/" + str(len(images)) + ": " + fname )
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (xChess,yChess),None)
    
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2=cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)
            
            if displayImage:
                #Draw and display the corners
                cv2.drawChessboardCorners(img, (xChess, yChess), corners2,ret)
                r = 800.0 / img.shape[1]
                dim = (800, int(img.shape[0] * r))
                #perform the actual resizing of the image and show it
                resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                cv2.imshow('img',resized)
                cv2.waitKey(250)
            
        else:
            print("\t\t impossible to find chessboard in current image")
        
        tf = time.time()
        print("\t...done in " + str(tf-ti) + "s")
        imageIndex += 1
    
    
    cv2.destroyAllWindows()
    
    print("...determination of camera intrinsic characteristics from data")
    ti = time.time()
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    np.savez(parametersFile, ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
    tf = time.time()
    print("...took " + str(tf-ti) + "s, camera matrixes stored in " + parametersFile)
    
    if calculatePrecision:
        print("...calculating error")       
        mean_error = 0
        for i in range(0,len(objpoints)):
            imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
            error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
            mean_error  += error
        print("...total mean error: ", mean_error/len(objpoints))
    
    return True
