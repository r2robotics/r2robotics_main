# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import cv2
import glob
import time
from detection import *
from colorTools import *
from pictureTools import *


# Import image
imgPath = './colors.jpg'
image = cv2.imread(imgPath)
displayImages = False


# Generate masks
print("Calculating masks...")
t = time.time()
masks = buildColorMasks(image)
elapsed = time.time() - t
print("\t... took", elapsed, "s")


# Detect contours and areas
print("Finding countours and colored areas based on masks...")
colorplates = []
for maskIndex in range(0, len(masks)):
	if isUsefulColor(masks[maskIndex][0]):
		base = image.copy()
		eroded = cv2.erode(masks[maskIndex][1] , None, iterations=4)
		dilated = cv2.dilate(eroded , None, iterations=6)
		edges = cv2.Canny(np.uint8(dilated),0,1)
		newimg, contours,hierarchy = cv2.findContours(edges, 1, cv2.CHAIN_APPROX_SIMPLE)
		cv2.drawContours(base, contours, -1, (0, 0, 255), 3)
		if len(contours) >0:
			maxArea = 0
			for contour in contours:
				area = cv2.contourArea(contours[0])
				if area > maxArea :
					maxArea = area
					moments = cv2.moments(contours[0])
					cx = int(moments['m10']/moments['m00'])
					cy = int(moments['m01']/moments['m00'])
			print("\t>Countour area:", maxArea, ",center:", (cx, cy))
			if displayImages:
				cv2.circle(base, (cx, cy), 3, (255, 255, 255), -1)
				cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
				cv2.imshow("Image", base)
				cv2.waitKey(0)
			colorplates.append((masks[maskIndex][0], cx, cy, maxArea))


print("Sorting and choosing colored areas...")
# If more than 3 colors, keep only 3 biggest areas
if len(colorplates) > 3:
	print(len(colorplates), " colored areas detected. Selecting only 3 largest areas")
	colorplates = sorted(colorplates, key=lambda tup: tup[3], reverse = True)[:3]
	print("Selected areas:", colorplates)
# Sort by X
pattern = sorted(colorplates, key=lambda tup: tup[1], reverse = False)


print("Detected pattern:")
for element in pattern:
	print(element[0])
