# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import cv2
import glob
import time

from pictureTools import *
from poseEstimationTools import *

### INPUTS
# Input image
imgPath = './testB_4.jpg' #Or './testB_4b.jpg'
image = rotate180deg(cv2.imread(imgPath))
# World coordinates for POIs
worldCoordinates =  np.array([[40,80,2.25],
							  [80,120,2.25], # Remove is testB_4b.jpg
							  [160,80,2.25],
							  [80,200,2.25]  # Add [160,160,2.25] after is testB_4b.jpg
                              ], dtype=np.float)
displayImages = True
verbose = True
# 3D points to project to image plane
projectPoints = True
pointsToProject = np.float32([[40,40,0], [40,80,0], [40,120,0], [40,160,0], [40,200,0],
				   [80,40,0], [80,80,0], [80,120,0], [80,160,0], [80,200,0],
				   [120,40,0], [120,80,0], [120,120,0], [120,160,0], [120,200,0],
				   [160,40,0], [160,80,0], [160,120,0], [160,160,0], [160,200,0],
				   [200,40,0], [200,80,0], [200,120,0], [200,160,0], [200,200,0],
				   [200,163,0], [200,163,22], [200,142,0], [200,142,22]]).reshape(-1,3)

### SCRIPT
# Load camera intrinsic parameters data
mtx, dist, ret, rvecs, tvecs = getPiCameraParameters()

# Detect balls in image
centers, detectedImg = detectBalls(image, False, True, False)
centers_by_x = sorted(centers, key=lambda tup: tup[1], reverse = True)
if displayImages:
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", detectedImg)
    cv2.waitKey(0)

# Prepare PnP. This array manipulation prevent errors in solvePnp
#criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
imageNP = np.array(centers_by_x)
(N, M) = imageNP.shape
imageCoordinates = np.ascontiguousarray(imageNP, dtype=np.float).reshape((N,1,2))

# Find the rotation and translation vectors.
if verbose:
    print("world: ", worldCoordinates)
    print("image: ", imageCoordinates)
    print("\nExecuting PnP solver...")
(success, rvecs, tvecs) = cv2.solvePnP(worldCoordinates, imageCoordinates, mtx, dist, flags=cv2.SOLVEPNP_P3P)
if verbose:
    print("PnP solver success? ", success, ", results: ", rvecs, tvecs)

# Project points and display image (useful for debug :))
if projectPoints & displayImages:
    imgpts, jac = cv2.projectPoints(pointsToProject, rvecs, tvecs, mtx, dist)
    for i in range(0, len(imgpts)):
        cv2.circle(detectedImg, tuple(imgpts[i][0]), 5, (255, 255, 0), -1)
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", detectedImg)
    cv2.waitKey(0)

print("Done.")
print("'camera_pose-estimation_test.py' EOF")
