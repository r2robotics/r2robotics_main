# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import cv2
import glob
import time

from colorTools import *
from pictureTools import *
from poseEstimationTools import *

### INPUTS
# Input image
imgPath = './realistic.jpg'
image = rotate180deg(cv2.imread(imgPath))
# World coordinates for POIs
worldCoordinates =  np.array([[1550, 610,2.25],
							  [ 840, 610,2.25],
							  [ 350,1130,2.25],
							  [ 350,1500,2.25]
                              ], dtype=np.float)
displayImages = True
verbose = True
# 3D points to project to image plane
projectPoints = True
pointsToProject = np.float32([[1550, 610,2.25],[ 840, 610,2.25],[ 350,1130,2.25],[ 350,1500,2.25],
                              [0,1400,95], [0,1400,175], [0,1600,95], [0,1600,175]]).reshape(-1,3)

### SCRIPT
# Load camera intrinsic parameters data
mtx, dist, ret, rvecs, tvecs = getPiCameraParameters()

# Detect balls in image
hsvViolet = BGR2HSV(getBgrColorFromPixel(image, 774, 594))
cv2.circle(image, (int(774), int(594)), 5, (255, 255, 255), -1)
print(hsvViolet)
centers, detectedImg = detectBallsFromHsvColor(image, hsvViolet, (10,50,50), verbose)
centers_by_x = sorted(centers, key=lambda tup: tup[1], reverse = True)
if displayImages:
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", detectedImg)
    cv2.waitKey(0)

# Prepare PnP. This array manipulation prevent errors in solvePnp
#criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
imageNP = np.array(centers_by_x)
(N, M) = imageNP.shape
imageCoordinates = np.ascontiguousarray(imageNP, dtype=np.float).reshape((N,1,2))

# Find the rotation and translation vectors.
if verbose:
    print("world: ", worldCoordinates)
    print("image: ", imageCoordinates)
    print("\nExecuting PnP solver...")
(success, rvecs, tvecs) = cv2.solvePnP(worldCoordinates, imageCoordinates, mtx, dist, flags=cv2.SOLVEPNP_P3P)
if verbose:
    print("PnP solver success? ", success, ", results: ", rvecs, tvecs)

# Project points and display image (useful for debug :))
if projectPoints & displayImages:
    imgpts, jac = cv2.projectPoints(pointsToProject, rvecs, tvecs, mtx, dist)
    for i in range(0, len(imgpts)):
        cv2.circle(detectedImg, tuple(imgpts[i][0]), 5, (255, 255, 0), -1)
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", detectedImg)
    cv2.waitKey(0)

print("Done.")
print("'camera_pose-estimation_test.py' EOF")
