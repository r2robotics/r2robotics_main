# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import time
import cv2
import math
from enum import Enum

# Carefull, OpenCV convention for color values used throughout this file !

# HSV color class definition
class HSVColor(object):
    def __init__(self, h, s, v):
        self.h = h
        self.s = s
        self.v = v
    def __repr__(self):
        return 'HSV color (%r, %r, %r)' % (self.h, self.s, self.v)
    def getValue(self):
        return np.uint8([[[self.h,self.s,self.v]]])


# Main Enum, defines our colors for detection + grey value for detection of useless light colors
class Color(Enum):
    YELLOW = HSVColor(22,255,247)
    GREEN = HSVColor(47,156,153)
    BLACK = HSVColor(120,33,15)
    BLUE = HSVColor(106,191,179) # not working very well with RAL equivalent 198,255,176
    ORANGE = HSVColor(8,207,209)
    GREY = HSVColor(23,28,181)


# Define manually which colors are not useful
def isUsefulColor(colorEnumElem):
    if colorEnumElem != Color.GREY:
        return True
    else:
        return False


# Normalize hsv Values
def getNormalizedHsvValues(hsvColor):
	# Extract color values
	h, s, v = hsvColor.ravel()
	# Normalize
	h = h / 180.0 * np.pi
	s = s / 255.0
	v = v / 255.0
	#Return values
	return h, s, v


# BGR/HSV conversion
def BGR2HSV(bgrColor):
    return cv2.cvtColor(bgrColor, cv2.COLOR_BGR2HSV)


# Return a distance between 2 hsv colors in conical HSV space
def hsvDistance(hsvColor1, hsvColor2):
	# Get normalized color values
    h1, s1, v1 = getNormalizedHsvValues(hsvColor1)
    h2, s2, v2 = getNormalizedHsvValues(hsvColor2)
	# Compute norm in conical HSV space
    distance = (math.sin(h1)*s1*v1 - math.sin(h2)*s2*v2)**2 \
    + (math.cos(h1)*s1*v1 - math.cos(h2)*s2*v2 )**2	\
    + 2*((v1 - v2)**2)
	# Return distance
    return distance


# This function returns a mask based on RGB color
def getMaskFromBGRColor(image, bgrColor, hrange, s_lower_bound, v_lower_bound):
	# Run fucntion with HSV
	return getMaskFromHSVColor(image, BGR2HSV(bgrColor), hrange, s_lower_bound, v_lower_bound)


# This function returns a mask based on HSV color
def getMaskFromHSVColor(BGRimage, hsv, hrange, s_lower_bound, v_lower_bound):
	# Convert image to HSV colorspace
	image = cv2.cvtColor(BGRimage, cv2.COLOR_BGR2HSV)
	# Color range
	hsvLower = (hsv.item(0)-hrange, max(hsv.item(1)-s_lower_bound,0), max(hsv.item(2)-v_lower_bound,0))
	hsvUpper = (hsv.item(0)+hrange, min(hsv.item(1)+s_lower_bound,255), min(hsv.item(2)+v_lower_bound,255))
	print("Created color range")
	# Creare mask
	mask = cv2.inRange(image, hsvLower, hsvUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)
	print("Created mask")
	return mask;


# Check if a given color is the closest to a given value
def isClosestToColor(hsvValue, ColorEnumElem):
    refDistance = hsvDistance(hsvValue, ColorEnumElem.value.getValue())
    isClosest = True
    for color in Color:
        if color != ColorEnumElem:
            dst = hsvDistance(hsvValue, color.value.getValue())
            if dst < refDistance:
                isClosest = False
                break
    return isClosest


# Returns the closest color as defined in Color enum
def getClosestColor(hsvValue):
    refDistance = 5 # Big value for intialization
    for color in Color:
        dst = hsvDistance(hsvValue, color.value.getValue())
        if dst < refDistance:
            refColor = color
            refDistance = dst
    return refColor


# Build the color masks for the different colors from Color Enum
# This function loops on all pixels from input image and is quite slow
# Similar to 'getColorMask_depr', but builds all masks at once to greatly reduce run time
def buildColorMasks(BGRimage):
	# Convert image to HSV colorspace
    image = cv2.cvtColor(BGRimage, cv2.COLOR_BGR2HSV)
    
    # Prepare masks
    xSize,ySize = image.shape[:2]
    masks = []
    for color in Color:
        masks.append((color,np.zeros((xSize,ySize))))
    
    # Main loop
    for x in range(0, xSize):
        for y in range(0, ySize):
            closestColor = getClosestColor(image[x,y])
            for mask in masks:
                if mask[0] == closestColor:
                    mask[1][x][y] = True
    
    # Done
    return masks


# Return the mask associated to a given color from Color enum
# Input is a list of (Color.XXXX, mask)
def getMaskFromColor(masksList, colorEnumElem):
    for elem in masksList:
        if elem[0] == colorEnumElem:
            return elem[1]


# This function returns a list of tuples with 3 colors representing the
# possible patterns as defined in the 2018 Robotic French Cup Rules.
def getPossiblePatterns():
    patterns = [
        (Color.ORANGE, Color.BLACK, Color.GREEN),
        (Color.YELLOW, Color.BLACK, Color.BLUE),
        (Color.BLUE, Color.GREEN, Color.ORANGE),
        (Color.YELLOW, Color.GREEN, Color.BLACK),
        (Color.BLACK, Color.YELLOW, Color.ORANGE),
        (Color.GREEN, Color.YELLOW, Color.BLUE),
        (Color.BLUE, Color.ORANGE, Color.BLACK),
        (Color.GREEN, Color.ORANGE, Color.YELLOW),
        (Color.BLACK, Color.BLUE, Color.GREEN),
        (Color.ORANGE, Color.BLUE, Color.YELLOW)
        ]
    return patterns


# This functions checks whether a set of 3 colors matches a pattern
# Checks in both directions
def matches2018Patterns(sideColor1, centerColor, sideColor2):
    for pattern in getPossiblePatterns():
        if (sideColor1, centerColor, sideColor2) == pattern:
            return true
        elif (sideColor2, centerColor, sideColor1) == pattern:
            return true
    return false


# Provides pattern completion based on 2 adjacent colors
def completeMatchesFromNeighbours(sideColor, centerColor):
    for pattern in getPossiblePatterns():
        if centerColor == pattern[1]:
            if sideColor == pattern[0]:
                return pattern[2]
            if sideColor == pattern[2]:
                return pattern[0]
    return None


# Provides pattern completion based on 2 side colors
def completeMatchesFromNeighbours(sideColor1, sideColor2):
    for pattern in getPossiblePatterns():
            if sideColor1 == pattern[0] and sideColor2 == pattern[2]:
                return pattern[1]
            if sideColor2 == pattern[0] and sideColor1 == pattern[2]:
                return pattern[1]
    return None


# DEPRECATED
# Function to build mask for a unique Color value
# Very slow as it loops on all pixels of image
def getColorMask_depr(BGRimage, ColorEnumElem):
	# Convert image to HSV colorspace
    image = cv2.cvtColor(BGRimage, cv2.COLOR_BGR2HSV)
    # Prepare pixels scan
    xSize,ySize = image.shape[:2]
    mask = np.zeros((xSize,ySize))
    # Main loop
    for x in range(0, xSize):
        for y in range(0, ySize):
            mask[x][y] = isClosestToColor(image[x,y], ColorEnumElem)
    mask = cv2.erode(mask, None, iterations=4)
    mask = cv2.dilate(mask, None, iterations=10)
    return mask

# Below, functions for BGR-HSV conversion
def getBgrColorFromPixel(image, x, y):
    return getBgrColor(image[x][y])

def getBgrColor(bgrValues):
    return np.uint8([[bgrValues]])
    
def getHsvColor(bgrValues):
    return BGR2HSV(getBgrColor(bgrValues))
