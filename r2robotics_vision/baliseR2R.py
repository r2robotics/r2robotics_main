# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import cv2

from

# HSV color class definition
class BaliseR2R(object):
    def __init__(self, name):
        self.name = name
        self.initialized = False
        self.rvec = None
        self.tvec = None

    def __repr__(self):
        return 'Balise R2R (%s)' % (self.name)

    def estimatePosition(self):
        # Do position estimation, store tvec & rvec
    
    def loopPatternFinder(self):
        # Prevent call if position and rotation vectors not initialized
        if not self.initialized :
            print("Balise not ready, use 'estimatePosition'")
            return None
        
        # If everything ready, look for pattern
        
        
        # Return result to ROS management
        
