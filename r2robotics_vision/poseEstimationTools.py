# V.Guinet, R2Robotics, 2018

# Import necessary packages
import numpy as np
import cv2
import glob
import time

from detectionTools import *

# Loads fils with intrinsic parameters determined from preliminary calibration
def getPiCameraParameters():
    # Load camera intrinsic parameters data
    with np.load('./camera_out.npz') as X:
        mtx, dist, ret, rvecs, tvecs = [X[i] for i in ('mtx','dist','ret','rvecs','tvecs')]
    return mtx, dist, ret, rvecs, tvecs


