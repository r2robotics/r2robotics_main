# V.Guinet, R2Robotics, 2018

# Import necessary packages
import time
import cv2
import numpy as np
import os

from picamera.array import PiRGBArray
from picamera import PiCamera

# Low-level function used to retrieve picture
def takePicture(resX, resY):
	camera = PiCamera()
	camera.framerate = 1
	camera.resolution = (resX,resY)
	rawCapture = PiRGBArray(camera)
	#time.sleep(0.1)
	camera.capture(rawCapture, format="bgr") #BGR instead of RGB because that's OpenCV's way
	return rawCapture.array

# This function applies a 180deg rotation to an input image
def rotate180deg(image):
	rows,cols = image.shape[:2]
	M = cv2.getRotationMatrix2D((cols/2,rows/2),180,1)
	return cv2.warpAffine(image,M,(cols,rows))

# Default function in our case, take picture at 2400x1808 resolution and apply rotation
def getDefaultBaliseImage():
	image = takePicture(2400, 1808)
	return rotate180deg(image)
	
# Picture-taking loop
def takePicturesUntilStop(displayImage, folder, rotateImages):
    # If folder doesn't exist, create it
    if not os.path.isdir(folder):
        os.mkdir(folder)
    
    # Initialize camera and get reference to the raw capture
    camera = PiCamera()
    camera.framerate = 1
    camera.resolution = (2400,1808)
    rawCapture = PiRGBArray(camera)
    
    # Allow some delay for camera start
    time.sleep(0.1)
    
    print("Initialized camera")
    
    # Pictures loop
    index = 0
    while 1:
        # Take image and save file
        camera.capture(rawCapture, format="bgr") #BGR instead of RGB because that's OpenCV's way
        image = rawCapture.array
        rawCapture.truncate(0) # Clear stream
        imageName = folder + "calibCapture" +str(index) + ".jpg"
        cv2.imwrite(imageName, image)
        
        # Display image if requested
        if displayImage :
            r = 800.0 / image.shape[1]
            dim = (800, int(image.shape[0] * r))
            # perform the actual resizing of the image and show it
            resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
            cv2.imshow("Frame", resized)
        
        print("Picture", index, "saved. CTRL+C to stop") 

        index += 1
    
