#ifndef COMMON_CONTROL_H
#define COMMON_CONTROL_H

#include <cmath>

namespace control_common{

inline float sqr(float a)
{
	return a*a;
}

struct Position2{
	double x;
	double y;

	Position2() = default;

	Position2(std::vector<double> vect)
	{
		x = vect.at(0);
		y = vect.at(1);
	}

	Position2(std::vector<float> vect)
	{
		x = vect.at(0);
		y = vect.at(1);
	}

	Position2(double x_, double y_):
		x(x_), y(y_){}

	void operator= (std::vector<double> vect){
		x = vect[0];
		y = vect[1];
	}

	double norm()
	{
		return sqrt(sqr(x) + sqr(y));
	}

	Position2 getNormalized()
	{
		auto n = norm();
		auto new_x = x / n;
		auto new_y = y / n;

		return {new_x,new_y};
	}

};


struct Pose2{
	double x;
	double y;
	double yaw = 0.0;

	Pose2() = default;

	Pose2(std::vector<double> vect)
	{
		x = vect.at(0);
		y = vect.at(1);
		yaw = vect.at(2);
	}

	Pose2(double x_, double y_, double yaw_):
		x(x_), y(y_), yaw(yaw_){}

	void operator= (std::vector<double> vect){
		x = vect[0];
		y = vect[1];
		yaw = vect[2];
	}

	void operator+ (Pose2 p){
		x += p.x;
		y += p.y;
		yaw += p.yaw;
	}

	void operator+= (Pose2 p) {
		x += p.x;
		y += p.y;
		yaw += p.yaw;
	}

	double scalarProd(Position2 target_position)
	{
		auto normalized_direction = target_position.getNormalized();

		return (x * normalized_direction.x +  y * normalized_direction.y);
	}

};

inline Position2 computeVector(Position2 v1, Pose2 v2)
{
	Position2 p{v1.x - v2.x, v1.y - v2.y};
	return p;
}

inline Position2 computeVector(Pose2 v1, Pose2 v2)
{
	Position2 p{v1.x - v2.x, v1.y - v2.y};
	return p;
}

inline void normalizeAndThresholding(std::vector<double>& wheels_speeds, double max_speed)
{
	auto m = std::max_element(wheels_speeds.begin(), wheels_speeds.end());
	if (*m > max_speed)
	{
		for (auto& wh: wheels_speeds)
		{
			if (wh > max_speed)
			{
				wh = wh / *m * max_speed ;
			}
		}
	}
}

inline Position2 computeVector(Position2 v1, Position2 v2)
{
	Position2 p{v1.x - v2.x, v1.y - v2.y};
	return p;
}

inline double distance(Position2 p1, Position2 p2){
	return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)); //more efficient than pow
}


inline double distance(Pose2 p1, Position2 p2){
	return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)); //more efficient than pow
}


inline double distance(Position2 p1, Pose2 p2){
	return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)); //more efficient than pow
}


inline double distance(Pose2 p1, Pose2 p2){
	return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)); //more efficient than pow
}

inline double distance(std::vector<float> vect, Pose2 p){
	return sqrt((vect[0] - p.x)*(vect[0] - p.x) + (vect[1] - p.y)*(vect[1] - p.y));
}

inline Position2 displacement(Position2 p1, Position2 p2)
{
	return {{p2.x - p1.x}, {p2.y - p1.y}};
}

inline bool angleEqual(float angle1, float angle2) {

    int a1 = (int)angle1 % 360;
    int a2 = (int)angle2 % 360;
    if (a1 < 0)
    a1 += 360;
    if (a2 < 0)
    a2 += 360;
    return (std::abs(a1 - a2) < 1.);

}

inline float modulo2PI(float angle)
{
	if (std::fabs(angle) > 180.)
	{
		if (angle < 0.)
			return angle + 360.;
		else
			return angle - 360.;
	} else
	{
		return angle;
	}
}


inline float modulo2PIrad(float angle)
{
	if (std::fabs(angle) > M_PI)
	{
		if (angle < 0.)
			return angle + 2*M_PI;
		else
			return angle - 2*M_PI;
	} else
	{
		return angle;
	}
}


//TODO: make an independent node for this, to estimate position (KF)
inline void delta_position(std::vector<Position2> wheels_position,
		Pose2& pose,
		std::vector<double> wheels_steering,
		std::vector<double> deltas,
		std::vector<double> ref_self_rotation,
		Position2 instant_rotation_center)
{
	Pose2 delta{0., 0., 0.};
	if (wheels_steering[0] == wheels_steering[1] && wheels_steering[2] == wheels_steering[0])
	{
		if (wheels_steering == ref_self_rotation){
			std::vector<double> d = {0., 0., (deltas[0] + deltas[1] + deltas[2])/
					(distance(wheels_position[0], {0.,0.}) +
					distance(wheels_position[1], {0.,0.}) +
					distance(wheels_position[2], {0.,0.}))};
			delta = d;
		} else {
			std::vector<double> d= {(deltas[0] + deltas[1] + deltas[2]) / 3 * cos(pose.yaw + (wheels_steering[0] + wheels_steering[1] + wheels_steering[2]) / 3),
					(deltas[0] + deltas[1] + deltas[2]) / 3 * sin(pose.yaw + (wheels_steering[0] + wheels_steering[1] + wheels_steering[2]) / 3), 0.};
			delta = d;
		}

	} else {

		std::vector<double> d = {(deltas[0] * cos(pose.yaw + (wheels_steering[0] + pose.yaw)) +
				deltas[1] * cos(pose.yaw + (wheels_steering[1] + pose.yaw)) +
				deltas[2] * cos(pose.yaw + (wheels_steering[2] + pose.yaw))) / 3,
				(deltas[0] * sin(pose.yaw + (wheels_steering[0] + pose.yaw)) +
				deltas[1] * sin(pose.yaw + (wheels_steering[1] + pose.yaw)) +
				deltas[2] * sin(pose.yaw + (wheels_steering[2] + pose.yaw))) / 3,
				(deltas[0] + deltas[1] + deltas[2])/
				(distance(wheels_position[0], instant_rotation_center) +
				distance(wheels_position[1], instant_rotation_center) +
				distance(wheels_position[2], instant_rotation_center))};
		delta = d;
	}

	pose += delta;
	return;
}

enum Movement{
	crab = 0,
	on_place = 1,
	arc_circle = 2,
	composed_movement = 3
};

enum Mode{
	global,
	potential,
	recalibration
};

struct PID{
 	double Kp_;
 	double Ki_;
 	double Kd_;

    double previous_command_tn = 0.0;
    double command_tn = 0.0;
    double command_tn_derivate = 0.0;

    double tn_1_secs;
    double tn_secs;

    double instruction_tn = 0.0;
    double feedback_tn = 0.0;

    double max_speed = 1.0;
    double min_speed = 0.05;

    double integral_ = 0.0;
    double derivate_ = 0.0;
    double proportional_ = 0.0;
    double error = 0.0;
    double previous_error = 0.0;

 	PID(double kp, double ki, double kd, double mi_s, double ma_s):
 		Kp_(kp), Ki_(ki), Kd_(kd), min_speed(mi_s), max_speed(ma_s){}

 	PID() = default;

 	void computePID()
 	{
 		// ERROR COMPUTATION
 		error = (instruction_tn - feedback_tn);

 		// PID COMPUTATION
 		proportional_ = Kp_ * error;
 		integral_ += Ki_ * error * (tn_secs - tn_1_secs);
 		derivate_ = Kd_ * (error - previous_error) / (tn_secs - tn_1_secs);

 		// COMMAND COMPUTATION
 		command_tn = derivate_ + integral_ + proportional_;

 		// SAVING PREVIOUS VALUES
 		previous_error = error;
 		command_tn_derivate = (command_tn - previous_command_tn) / (tn_secs - tn_1_secs);
 		previous_command_tn = command_tn;
 	}

 	void setFeedback(double f)
 	{
 		feedback_tn = f;
 	}

 	void updateTime(double date)
 	{
 		if (date == tn_secs) return;
 		tn_1_secs = tn_secs;
 		tn_secs = date;
 	}

 	void reset()
 	{
 		error = 0.;
 		previous_error = 0.;
 		command_tn = 0.;
 		integral_ = 0.;
 		derivate_ = 0.;
 		proportional_ = 0.;
 		command_tn_derivate = 0.;
 		previous_command_tn = 0.;
 	}

 };

struct Wheel
{
	double radius;
	Pose2 coordinates;
	double reduction_factor;

	Wheel();
	Wheel(double r, std::vector<double> v, double rf):
		radius{r}, reduction_factor(rf)
		{
			coordinates = Pose2{v[0], v[1], v[2]};
		}

	void updateOrientation(float orientation)
	{
		coordinates.yaw = orientation;
	}

	double getDistanceToRobotCenter()
	{
		return sqrt(coordinates.x * coordinates.x + coordinates.y * coordinates.y);
	}

	Position2 getPosition()
	{
		return {coordinates.x, coordinates.y};
	}

	double getYaw()
	{
		return coordinates.yaw;
	}
};


}


#endif

