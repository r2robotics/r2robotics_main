#ifndef AVOIDANCE
#define AVOIDANCE

#include "ros/ros.h"
#include <cmath>

#include "pose.h"
#include "angles.h"

#include <std_srvs/Trigger.h>
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <r2robotics_msgs/SonarArray.h>
#include <r2robotics_msgs/Position.h>
#include <r2robotics_srvs/triggerAvoidance.h>
#include <r2robotics_srvs/testAvoidance.h>
#include <r2robotics_srvs/stopControl.h>
#include <r2robotics_srvs/resumeControl.h>

typedef std::vector<double>::size_type size_type;
typedef r2robotics_srvs::testAvoidance testAvoidance;

class Point
{
  public:
    double x;
    double y;
    Point( double x_, double y_): x(x_), y(y_) {}
    Point(): x(0), y(0) {}
};

class Avoidance
{
  public:
    Avoidance( ros::NodeHandle node );
    
    // Avoidance loop
    void loop();

  private:
    // ROS variables
    std::string node_name;
    ros::NodeHandle nh;

    // Pose
    Pose currentPose;
    Pose previousPose;
    void setCurrentPose( int x, int y, double yaw );
    void setCurrentPoseAndSpeed( double x, double y, double yaw, double vx, double vy);
    // Speed
    double dx;
    double dy;
    double speedDirection_rad;
    double lastSpeedDirection_rad;
    // Sonars values
    std::vector<double> sonarValues;
    // The robot has been ordered to stop because of an obstacle
    bool stopped;
    bool CHECK_AVOIDANCE;

    // Parameters
    int nb_sonars;
    std::vector<int> sonarIndex;
    std::vector<double> sonarOrientation; // orientation of each sonar, in degrees
    std::vector<double> sonarOrientation_rad; // orientation of each sonar, in radian
    std::vector<Point> sonarPosition; // position of each sonar relative to the robot
    double distanceMin;
    double distanceResume;
    double margin;
    double thetaMax;
    double SONAR_EPSILON;
    double distanceSonarMax;
    double V_EPSILON;
    double freq;
    // Topic
    ros::Subscriber current_position_topic;
    ros::Subscriber simu_pose_topic;
    ros::Subscriber sonar_array_topic;
    // Service
    ros::ServiceClient srv_trigger_avoidance;
    ros::ServiceClient stop_control_client;
    ros::ServiceClient resume_control_client;
    ros::ServiceServer srv_stop_avoidance;
    ros::ServiceServer srv_resume_avoidance;
    ros::ServiceServer srv_test_stop; // for tests
    ros::ServiceServer srv_test_resume; // for tests
    ros::ServiceServer srv_test; // for tests

    // Callback functions for topics
    void currentPositionCallback(const r2robotics_msgs::Position pos);
    void linkStatesCallback(const gazebo_msgs::LinkStates feedback);
    void sonarCallback(const r2robotics_msgs::SonarArray sonar);
    // Callback functions for services
    bool serviceStop( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res );
    bool serviceResume( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res );
    bool serviceTestStop( testAvoidance::Request& req, testAvoidance::Response& res );
    bool serviceTestResume( testAvoidance::Request& req, testAvoidance::Response& res );
    bool serviceTestStopPosition( testAvoidance::Request& req, testAvoidance::Response& res );

    // Initialization methods
    void initParams();
    void initTopics();
    void initServices();

    double getRealVelocity( double dx );
    std::vector<size_type> getActiveSonars();
    std::vector<size_type> getActiveSonars( double speedDirection );
    std::vector<size_type> getActiveSonarsNoSpeedCheck( double speedDirection );
    bool inSameDirection(  double t1_rad, double t2_rad );
    bool inSameDirectionNoSpeedCheck(  double t1_rad, double t2_rad );
    Point getObstaclePoint( size_type sonarIndex );
    Point getCurrentSonarPosition( size_type sonarIndex );
    bool isOutsideTable( Point p );
    bool mustStop( size_type i );
    void sendStop();
    void sendResume();
    bool obstacleStillThere();
    bool checkMustStop();
    bool checkMustResume();
};

#endif // AVOIDANCE