#include "avoidance.h"
#include "colors.h"

using namespace std;

Avoidance::Avoidance( ros::NodeHandle node ):
  node_name( ros::this_node::getName() )
  ,nh ( node )
{
  ROS_AVOIDANCE_STREAM("Init starting");
  ros::Duration(0.5).sleep(); // sleep for half a second
  initParams();
  initTopics();
  initServices();
  ros::Duration(0.5).sleep(); // sleep for half a second
}

//
// INITIALIZATION METHODS
//

void Avoidance::initParams()
{
  ROS_AVOIDANCE_STREAM("Init params ...");
  setCurrentPose(0,0,0);
  setCurrentPose(0,0,0);
  stopped = false;
  SONAR_EPSILON = 5; // 5 mm
  CHECK_AVOIDANCE = true;

  ROS_AVOIDANCE_STREAM("Getting params");

  distanceSonarMax = 1000; // 100cm
  V_EPSILON = 50; // 2cm/s

  if ( !nh.getParam("avoidance_frequency", freq) ) {
    ROS_ERROR("Failed to get avoidance_frequency rate !");
  }

  if( !nh.getParam("avoidance_distance_min", distanceMin) )
  {
    ROS_WARN_STREAM("Failed to get minimum distance for avoidance, setting default : " << 55 << " cm");
    distanceMin = 550; // 55 cm
  }
  if( !nh.getParam("avoidance_distance_resume", distanceResume) )
  {
    ROS_WARN_STREAM("Failed to get resuming distance for avoidance, setting default : " << 60 << " cm");
    distanceMin = 600; // 60 cm
  }
  if( !nh.getParam("avoidance_margin", margin) )
  {
    ROS_WARN_STREAM("Failed to get margin distance for avoidance, setting default : " << 10 << " cm");
    distanceMin = 100; // 10 cm
  }
  if( !nh.getParam("avoidance_theta_max", thetaMax) )
  {
    ROS_WARN_STREAM("Failed to get max angle for avoidance, setting default : " << 45 << " degrees");
    thetaMax = DEG2RAD(45); // 45 degrees
  }

  nb_sonars = 0;
  if( !nh.getParam("sonars_number", nb_sonars) )
  {
    ROS_ERROR("Failded to get number of sonars ! Avoidance will NOT work");
  }

  for(int i=0; i < nb_sonars; i++)
  {
    double t = 0, x = 0, y = 0;
    int index = -1;
    if( nh.getParam("sonar_"+to_string(i)+"_theta", t) &&
        nh.getParam("sonar_"+to_string(i)+"_x", x) &&
        nh.getParam("sonar_"+to_string(i)+"_y", y) &&
        nh.getParam("sonar_"+to_string(i)+"_array_index", index) )
    {
      sonarOrientation.push_back( DEG_TO_180( t ) );
      sonarOrientation_rad.push_back( DEG_TO_180( t ) );
      sonarPosition.push_back( Point(x,y) );
      sonarIndex.push_back(index-1);
    }
    else
    {
      ROS_ERROR_STREAM("Failded to get sonar " << i << " !");
    }
  }
  ROS_AVOIDANCE_STREAM("Got " << nb_sonars << " sonars");
  sonarValues = std::vector<double> (nb_sonars, 0);
}

void Avoidance::initTopics()
{
  // Connects to topics for listening
  // target_position_topic = nh.subscribe("target_position", 1000, &TrajectoryPlanner::targetPositionCallback, this);
  current_position_topic = nh.subscribe("odometry_feedback", 1, &Avoidance::currentPositionCallback, this);
  // simu_pose = nh.subscribe("gazebo/link_states", 1, &TrajectoryPlanner::linkStatesCallback, this);
  simu_pose_topic = nh.subscribe("simulation_pose", 1, &Avoidance::linkStatesCallback, this);
  sonar_array_topic = nh.subscribe("sonars_feedback", 1, &Avoidance::sonarCallback, this);

  ROS_AVOIDANCE_STREAM("Avoidance Node : topics initialized"); 
}

void Avoidance::initServices()
{
  // Connects to services to call
  stop_control_client = nh.serviceClient<r2robotics_srvs::stopControl>("stop_control"); 
  resume_control_client = nh.serviceClient<r2robotics_srvs::resumeControl>("resume_control");   
  // Advertise services
  srv_stop_avoidance = nh.advertiseService("avoidance_stop", &Avoidance::serviceStop, this);
  srv_resume_avoidance = nh.advertiseService("avoidance_resume", &Avoidance::serviceResume, this);
  // services for tests
  srv_test_stop = nh.advertiseService("avoidance_test_stop", &Avoidance::serviceTestStop, this);
  srv_test_resume = nh.advertiseService("avoidance_test_resume", &Avoidance::serviceTestResume, this);
  srv_test = nh.advertiseService("avoidance_test", &Avoidance::serviceTestStopPosition, this);
  ROS_AVOIDANCE_STREAM("Avoidance Node : services initialized");
}

//
// Callback functions for services
//

bool Avoidance::serviceStop( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
{
  ROS_AVOIDANCE_STREAM("STOP avoidance");
  CHECK_AVOIDANCE = false;
}

bool Avoidance::serviceResume( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
{
  ROS_AVOIDANCE_STREAM("RESUME avoidance");
  CHECK_AVOIDANCE = true;
}

bool Avoidance::serviceTestStop( testAvoidance::Request& req, testAvoidance::Response& res )
{
  if( nb_sonars != req.sonarValues.size() )
  {
    ROS_WARN_STREAM("Wrong number of sonar values, there should be " << nb_sonars << " values");
    return false;
  }
  sonarValues = req.sonarValues;
  setCurrentPoseAndSpeed(req.robotPose.x, req.robotPose.y, req.robotPose.yaw, req.dx, req.dy);
  bool resStop = checkMustStop();
  ROS_AVOIDANCE_STREAM(" MUST STOP ? " << resStop);
  return true;
}

bool Avoidance::serviceTestStopPosition( testAvoidance::Request& req, testAvoidance::Response& res )
{
  setCurrentPoseAndSpeed(req.robotPose.x, req.robotPose.y, req.robotPose.yaw, req.dx, req.dy);
  bool resStop = checkMustStop();
  ROS_AVOIDANCE_STREAM(" MUST STOP ? " << resStop);
  return true;
}

bool Avoidance::serviceTestResume( testAvoidance::Request& req, testAvoidance::Response& res )
{
  if( nb_sonars != req.sonarValues.size() )
  {
    ROS_WARN_STREAM("Wrong number of sonar values, there should be " << nb_sonars << " values");
    return false;
  }
  sonarValues = req.sonarValues;
  setCurrentPoseAndSpeed(req.robotPose.x, req.robotPose.y, req.robotPose.yaw, req.dx, req.dy);
  bool resResume = checkMustResume();
  ROS_AVOIDANCE_STREAM(" MUST RESUME ? " << resResume);
  return true;
}

//
// Callback functions for topics
//

void Avoidance::currentPositionCallback(const r2robotics_msgs::Position pos)
{
  int x = (int)(pos.x);
  int y = (int)(pos.y);
  double yaw = DEG_TO_180(RAD2DEG(pos.theta));
  setCurrentPose(x, y, yaw);
}


void Avoidance::linkStatesCallback(const gazebo_msgs::LinkStates feedback)
{
    // ROS_AVOIDANCE_STREAM("Trajectory Node : link states callback !");
    if( feedback.name.size() == 0)
    {
      return;
    }
    int i = -1;
    int N = feedback.name.size();
    bool stop = false;
    while(i < N && !stop)
    {
        i++;
        if (feedback.name[i] == "gr::base_link")
        {
            stop = true;
        }
    }
    double x = (feedback.pose[i].position.x)*1000.0;
    double y = (feedback.pose[i].position.y)*1000.0;
    double yaw = RAD2DEG( asin(sqrt(feedback.pose[i].orientation.x * feedback.pose[i].orientation.x
                + feedback.pose[i].orientation.y * feedback.pose[i].orientation.y
                + feedback.pose[i].orientation.z * feedback.pose[i].orientation.z))*2 );
    // ROS_AVOIDANCE_STREAM("callback : " << x << ", " << y << " - " << yaw);
  setCurrentPose(x, y, yaw);
}

void Avoidance::sonarCallback(const r2robotics_msgs::SonarArray sonar)
{
  // ROS_AVOIDANCE_STREAM("Sonar array : " << sonar);
  int nb_sonars_feedback = sonar.distances.size();
  for( int i = 0; i < nb_sonars; i++ )
  {
    sonarValues[i] = sonar.distances[ sonarIndex[i] ]*10; // sonar feedback in cm
  }
}

//
// Avoidance calculation functions
//

void Avoidance::setCurrentPose( int x, int y, double yaw )
{
  previousPose.update(currentPose);
  currentPose.update(x,y,yaw);
  dx = currentPose.x - previousPose.x;
  dy = currentPose.y - previousPose.y;
  speedDirection_rad = atan2(dy, dx); // radians
  // ROS_AVOIDANCE_STREAM("x = " << x );
  // ROS_AVOIDANCE_STREAM("previous pose = " << previousPose.x << " - current pose = " << currentPose.x );
  // ROS_AVOIDANCE_STREAM("dx = " << dx << " - real dx = " << getRealVelocity(dx) );
}

double Avoidance::getRealVelocity( double dx )
{
  return dx * freq; // en mm / s
}

void Avoidance::setCurrentPoseAndSpeed( double x, double y, double yaw, double vx, double vy)
{
  currentPose.update(x,y,yaw);
  dx = vx;
  dy = vy;
  speedDirection_rad = atan2(dy, dx); // radians
}

vector<size_type> Avoidance::getActiveSonars()
{
  return getActiveSonars(speedDirection_rad);
}

vector<size_type> Avoidance::getActiveSonars( double _speedDirection_rad )
{
  vector<size_type> activeSonars;
  for( int i=0; i<sonarOrientation_rad.size(); i++ )
  {
    double theta_rad = DEG2RAD(sonarOrientation[i] + currentPose.yaw);

    if( inSameDirection( theta_rad, _speedDirection_rad ) )
    {
      activeSonars.push_back( i );
      if( CHECK_AVOIDANCE ) {
        ROS_AVOIDANCE_STREAM("Active sonar : " << i << " - d = " << sonarValues[i] ); }
    }
  }
  return activeSonars;
}

vector<size_type> Avoidance::getActiveSonarsNoSpeedCheck( double _speedDirection_rad )
{
  vector<size_type> activeSonars;
  for( int i=0; i<sonarOrientation_rad.size(); i++ )
  {
    double theta_rad = DEG2RAD(sonarOrientation[i] + currentPose.yaw);

    if( inSameDirectionNoSpeedCheck( theta_rad, _speedDirection_rad ) )
    {
      activeSonars.push_back( i );
      if( CHECK_AVOIDANCE ) {
        ROS_AVOIDANCE_STREAM("Active sonar : " << i << " - d = " << sonarValues[i] ); }
    }
  }
  return activeSonars;
}


bool Avoidance::inSameDirection(  double t1_rad, double t2_rad )
{
  // if the velocity is almost zero, consider no sonar is in the same direction
  if( fabs(getRealVelocity(dx)) <= V_EPSILON && fabs(getRealVelocity(dy)) <= V_EPSILON)
  {
    // ROS_AVOIDANCE_STREAM("velocity " << fabs(getRealVelocity(dx)) << " < " << V_EPSILON);
    // ROS_AVOIDANCE_STREAM("velocity " << fabs(getRealVelocity(dy)) << " < " << V_EPSILON);
    return false;
  }
  // everything in rad
  if( cos(t1_rad) * cos(t2_rad) + sin(t1_rad) * sin(t2_rad) >= cos(thetaMax) )
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool Avoidance::inSameDirectionNoSpeedCheck(  double t1_rad, double t2_rad )
{
  // everything in rad
  if( cos(t1_rad) * cos(t2_rad) + sin(t1_rad) * sin(t2_rad) >= cos(thetaMax) )
  {
    return true;
  }
  else
  {
    return false;
  }
}

Point Avoidance::getObstaclePoint( size_type sonarIndex )
{
  Point p = getCurrentSonarPosition( sonarIndex );
  double d = sonarValues[sonarIndex];
  double theta = DEG2RAD(ADD_DEG(currentPose.yaw,sonarOrientation[sonarIndex]));
  // ROS_AVOIDANCE_STREAM("robot yaw + orientation sonar = " << theta);
  // ROS_AVOIDANCE_STREAM("d = " << d);
  // ROS_AVOIDANCE_STREAM("d * cos(theta) = " <<  d * cos(theta) << " - d * sin(theta) = " << d * sin(theta) );
  return Point( p.x + d * cos(theta), p.y + d * sin(theta) );
}

Point Avoidance::getCurrentSonarPosition( size_type sonarIndex )
{
  double t = currentPose.yaw;
  double sx = sonarPosition[sonarIndex].x;
  double sy = sonarPosition[sonarIndex].y;
  // ROS_AVOIDANCE_STREAM("sonar " << sonarIndex << " : " << sx << ", " << sy << " - yaw : " << t);
  double x = currentPose.x + (  sx * cos(t) + sy * sin(t) );
  double y = currentPose.y + ( -sx * sin(t) + sy * cos(t) );
  return Point(x, y);
}

bool Avoidance::isOutsideTable( Point p )
{
  // outside of basic table 2000*3000
  if( p.x < 0 + margin )
  {
    return true;
  }
  if( p.x > 2000 - margin)
  {
    return true;
  }
  if( p.y < 0 + margin )
  {
    return true;
  }
  if( p.y > 3000 - margin)
  {
    return true;
  }
  // distributeur de balles considered in the table
  if( (p.x > 786 - margin) && (p.x < 894 + margin) && (p.y < 100 + margin) )
  {
    return true;
  }
  if( (p.x > 786 - margin) && (p.x < 894 + margin) && (p.y > 2800 - margin) )
  {
    return true;
  }
  if( (p.x > 1900 - margin) && (p.y < 2444 + margin) && (p.y > 2336 - margin) )
  {
    return true;
  }
  if( (p.x > 1900 - margin) && (p.y < 664 + margin) && (p.y > 556 - margin) )
  {
    return true;
  }
  return false;
}

bool Avoidance::mustStop( size_type i )
{
  if( sonarValues[i] >= distanceSonarMax )
  {
    return false;
  }
  Point p = getObstaclePoint( i );
  if( isOutsideTable( p ) )
  {
    // ROS_AVOIDANCE_STREAM("Sonar " << i << " --> outside table :");
    // ROS_AVOIDANCE_STREAM("robot x = " << currentPose.x << ", y = " << currentPose.y << ", yaw = " << currentPose.yaw);
    Point psonar = getCurrentSonarPosition( i );
    // ROS_AVOIDANCE_STREAM("sonar x = " << psonar.x << ", sonar y = " << psonar.y);

    double d = sonarValues[i];
    double theta = sonarOrientation_rad[i];
    // ROS_AVOIDANCE_STREAM("x = " << p.x << ", y = " << p.y);
    return false;
  }
  else if( sonarValues[i] < SONAR_EPSILON )
  {
    return false; // default value of sonars is 0, is not an obstacle
  }
  else if( sonarValues[i] < distanceMin )
  {
    ROS_AVOIDANCE_STREAM("Sonar " << i);
    ROS_AVOIDANCE_STREAM("robot x = " << currentPose.x << ", y = " << currentPose.y << ", yaw = " << currentPose.yaw);
    Point psonar = getCurrentSonarPosition( i );
    ROS_AVOIDANCE_STREAM("sonar x = " << psonar.x << ", sonar y = " << psonar.y);
    ROS_AVOIDANCE_STREAM("Obstacle point : " << p.x << " - " << p.y);
    ROS_AVOIDANCE_STREAM("Distance : " << sonarValues[i] << "mm" << " < distanceMin = " << distanceMin << " mm");
    return true;
  }
  ROS_AVOIDANCE_STREAM("Sonar " << i << " = " << sonarValues[i]);
  return false;
}

void Avoidance::sendStop()
{
  r2robotics_srvs::stopControl srv;
  stop_control_client.call(srv);
}

void Avoidance::sendResume()
{
  r2robotics_srvs::resumeControl srv;
  srv.request.resume_last_command = true;
  resume_control_client.call(srv);
}

bool Avoidance::obstacleStillThere()
{
  ROS_AVOIDANCE_STREAM("Last speed direction = " << RAD2DEG(lastSpeedDirection_rad) );
  vector<size_type> activeSonars = getActiveSonarsNoSpeedCheck( lastSpeedDirection_rad );
  ROS_AVOIDANCE_STREAM("Obstacles still there ? " << activeSonars.size() << " sonars active");
  for( auto i: activeSonars )
  {
    // ROS_AVOIDANCE_STREAM("Sonar " << i << " : d = " << sonarValues[i]);
    // For each sonar in the direction where we were going
    if( sonarValues[i] < distanceResume )
    {
      // If there is an obstacle still close
      Point p = getObstaclePoint( i );
      ROS_AVOIDANCE_STREAM("obstacle : " << p.x << ", " << p.y);
      if ( !isOutsideTable( p ) )
      {
        // If obstacle not outside table, it is still there
        return true;
      }
    }
  }
  return false;
}

bool Avoidance::checkMustStop()
{
    if( !stopped )
    {
      // The robot is moving
      vector<size_type> activeSonars = getActiveSonars();
      for( auto i: activeSonars )
      {
        if( mustStop( i ) )
        {
          return true;
        }
      }
    }
  return false;
}

bool Avoidance::checkMustResume()
{
  if( stopped )
  {
    // The robot has been ordered to stop
    // We will resume the movement if the obstacle is gone
    if( obstacleStillThere() )
    {
      ROS_AVOIDANCE_STREAM("stopped and obstacle still there");
      return false;
    }
    ROS_AVOIDANCE_STREAM("stopped and obstacle not there");
    return true;
  }
  return false;
}

//
// Loop for avoidance node
//

void Avoidance::loop()
{
  // ROS_AVOIDANCE_STREAM("Avoidance loop !");
  std::clock_t begin = std::clock();

  if( !CHECK_AVOIDANCE )
  {
    return;
  }

  if( checkMustStop() )
  {
    ROS_AVOIDANCE_STREAM("Must stop !");
    sendStop();
    stopped = true;
    lastSpeedDirection_rad = speedDirection_rad;
  }
  else if( checkMustResume() )
  {
    ROS_AVOIDANCE_STREAM("Must resume !");
    sendResume();
    stopped = false;
  }
  else if(stopped)
  {
    ROS_AVOIDANCE_STREAM("Stopped");
  }
  else
  {
    // ROS_AVOIDANCE_STREAM("No stop no resume");
  }
  clock_t end = std::clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  // std::cout << "AVOIDANCE LOOP : " << elapsed_secs << " s" << "\n";

  return;
}