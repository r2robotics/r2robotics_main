#include "ros/ros.h"
#include <ros/package.h>
#include <sstream>

#include "avoidance.h"
#include "colors.h"

using namespace std;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "avoidance");
  ros::NodeHandle n;

  clock_t start;
  double duration;
  start = clock();

  ROS_AVOIDANCE_STREAM("Starting node Avoidance ......");

  Avoidance avoidance(n);

  double freq;
  if ( n.getParam("avoidance_frequency", freq) ) {
    ROS_AVOIDANCE_STREAM("Avoidance rate = " << freq);
  }
  else {
    ROS_ERROR("Failed to get avoidance_frequency rate !");
  }
  ros::Rate loop_rate(freq);
  int loop_per_second = 1.0 / freq;
  int count = 0;

  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;

  ROS_AVOIDANCE_STREAM("......... Node Avoidance started !");
  ROS_AVOIDANCE_STREAM("Avoidance initialization TIME: " << duration);

  while (ros::ok())
  {
    // ROS_INFO("avoidance loop");
    if (count >= loop_per_second) {
      count = 0;
    }
    avoidance.loop();
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }
  return 0;
}
