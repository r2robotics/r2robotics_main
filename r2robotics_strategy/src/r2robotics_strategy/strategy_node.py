#!/usr/bin/env python

import roslib
import rospy
from r2robotics_strategy import strategy

if __name__ == '__main__':
    strat = strategy.StrategyMaster()
    strat.start_smachine()