#!/usr/bin/env python

import roslib
import rospy
from enum import Enum
from r2robotics_strategy.point import Point
from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import *
from r2robotics_strategy.robot_goals import *
from r2robotics_strategy.little_robot_states import *

def init_homologation_avoidance(self):
    nb_aller_retour = 10
    if self.color == Color.GREEN:
        col = "vert"
        not_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        not_col = "vert"
    self.actions.add_goto(pointID="eau_propre_"+col)
    for k in range(nb_aller_retour):
        self.add_goto(pointID="milieu_"+col)
        self.add_goto(pointID="milieu_"+not_col)
RobotGoals.init_homologation_avoidance = init_homologation_avoidance

def add_bee_arm(self, value):
    self.actions.add_PMI_set_bee_arm(value=value)
RobotGoals.add_bee_arm = add_bee_arm

def init_homolo_abeille(self):
    # self.actions.add_wait(1)
    if self.color == Color.GREEN:
        col = "vert"
        other_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        other_col = "vert"
    # # HOMOLOGATION ABEILLE
    self.add_goto(pointID="eviter_cube_"+col)
    self.add_goto(pointID="debut_abeille_"+col)
    self.add_bee_arm('HIGH')
    self.add_goto(pointID="milieu_abeille_"+col)
    self.add_bee_arm('HORIZONTAL')
    self.add_stop_avoidance()
    self.add_goto(pointID="fin_abeille_"+col)
    self.add_bee_arm('HIGH')
    self.add_resume_avoidance()
    self.add_goto(pointID="milieu_abeille_"+col)
    self.add_goto(pointID="avant_avant_ranger_bras_abeille_"+col)
    self.add_goto(pointID="avant_ranger_bras_abeille_"+col)
    self.add_goto(pointID="ranger_bras_abeille_"+col)
    self.add_bee_arm('INSIDE')
RobotGoals.init_homolo_abeille = init_homolo_abeille

def init_homolo_distributeur(self):
    # self.actions.add_wait(1)
    if self.color == Color.GREEN:
        col = "vert"
        other_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        other_col = "vert"
    # # HOMOLOGATION DISTRIBUTEUR
    self.add_goto(pointID="avant_distributeur_"+col)
    self.add_stop_avoidance()
    self.add_goto(pointID="pre_distributeur_"+col)
    self.add_goto(pointID="distributeur_"+col)
    self.add_goto(pointID="apres_distributeur_"+col)
    self.add_resume_avoidance()
    # # TEST AVOIDANCE
    self.add_goto(pointID="milieu_table_"+col+"_"+col)
    self.add_goto(pointID="milieu_table_"+other_col+"_"+col)
    self.add_goto(pointID="start_"+col)
RobotGoals.init_homolo_distributeur = init_homolo_distributeur

def init_homologation(self):
    # self.actions.add_wait(1)
    if self.color == Color.GREEN:
        col = "vert"
        other_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        other_col = "vert"
    # # HOMOLOGATION DISTRIBUTEUR
    self.add_goto(pointID="avant_distributeur_"+col)
    self.add_stop_avoidance()
    self.add_goto(pointID="pre_distributeur_"+col)
    self.add_goto(pointID="distributeur_"+col)
    self.add_goto(pointID="apres_distributeur_"+col)
    self.add_resume_avoidance()
    # # ON VA
    self.add_goto(pointID="milieu_table_"+col+"_"+col)
    self.add_goto(pointID="debut_abeille_"+col)

RobotGoals.init_homologation = init_homologation

def init_qualif(self):
    # self.actions.add_wait(1)
    if self.color == Color.GREEN:
        col = "vert"
        other_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        other_col = "vert"
    # TEST
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 0)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 0)
    self.actions.add_PMI_pushpush("DEDANS")
    self.actions.add_PMI_canon("TIR")

    self.add_wait(1)
    
    # balle 1 entre dans roue haute
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 0)
    self.add_wait(1)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 1)
    self.add_wait(0.5)
    # balle 2 entre dans roue haute
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 2)
    self.add_wait(1)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 3)
    self.add_wait(0.5)
    # balle 3 entre dans roue haute et balle 1 tombe dans roue basse
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 4)
    self.add_wait(1)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 5)
    self.add_wait(0.5)

    # balle 1 tourne dans roue basse
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 1)
    self.add_wait(1)

    # balle 2 tombe dans roue basse
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 0)
    self.add_wait(1)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 1)
    self.add_wait(0.5)

    # balles 1 et 2 tournent dans roue basse
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 2)
    self.add_wait(1)

    # balle 3 tombe dans roue basse
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 2)
    self.add_wait(1)
    self.actions.add_PMI_roue("ROUE_HAUTE", "FERMETURE", 3)
    self.add_wait(0.5)

    # balle 1, 2, 3 tournent dans roue basse et balle 1 arrive devant la bielle
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 0)

    # balle 1 est lancee
    # self.actions.add_PMI_canon("PRENDRE_BALLE")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    self.actions.add_PMI_pushpush("DEDANS")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    # self.actions.add_PMI_canon("TIR")
    self.actions.add_PMI_pushpush("DEDANS")

    # balle 2 et 3 tournent dans roue basse et balle 2 arrive devant la bielle
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 1)
    # self.actions.add_PMI_canon("PRENDRE_BALLE")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    self.actions.add_PMI_pushpush("DEDANS")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    # self.actions.add_PMI_canon("TIR")
    self.actions.add_PMI_pushpush("DEDANS")
    
    # balle 3 tourne dans roue basse et arrive dvant la bielle
    self.actions.add_PMI_roue("ROUE_BASSE", "CANON", 2)
    # self.actions.add_PMI_canon("PRENDRE_BALLE")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    self.actions.add_PMI_pushpush("DEDANS")
    self.actions.add_PMI_pushpush("PUSHPUSH")
    # self.actions.add_PMI_canon("TIR")
    self.actions.add_PMI_pushpush("DEDANS")

    return

    # ABEILLE
    self.add_bee_arm('INSIDE')
    self.add_goto(pointID="eviter_cube_"+col)
    self.add_goto(pointID="debut_abeille_"+col)
    self.add_bee_arm('HIGH')
    self.add_goto(pointID="milieu_abeille_"+col)
    self.add_bee_arm('HORIZONTAL')
    self.add_stop_avoidance()
    self.add_goto(pointID="fin_abeille_"+col)
    self.add_bee_arm('HIGH')
    self.add_resume_avoidance()
    self.add_goto(pointID="milieu_abeille_"+col)
    self.add_goto(pointID="ranger_bras_abeille_"+col)

    self.add_bee_arm('INSIDE')
    if self.color == Color.ORANGE:
        self.add_goto(pointID="eviter_cube_"+col)
        self.add_goto(pointID="start_"+col)

    self.add_goto(pointID="avant_distributeur_"+col)
    self.add_stop_avoidance()
    self.add_goto(pointID="pre_distributeur_"+col)
    self.add_goto(pointID="distributeur_"+col)
    self.add_goto(pointID="apres_distributeur_"+col)
    self.add_resume_avoidance()
    # # ON VA 
    # self.add_goto(pointID="milieu_table_"+col+"_"+col)
    # self.add_goto(pointID="start_"+col)

RobotGoals.init_qualif = init_qualif

def init_finale(self):
    pass
RobotGoals.init_finale = init_finale

def init_test_table(self):
    pass
RobotGoals.init_test_table = init_test_table

def init_demo(self):
    pass
RobotGoals.init_demo = init_demo