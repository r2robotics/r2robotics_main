#include <ros.h>
#include <std_msgs/String.h>
#include <r2robotics_msgs/SonarArray.h>
#include "NewPing.h"

#define SONAR_NUM     4 // Number of sensors.
#define MAX_DISTANCE 100   // Maximum distance (in cm) to ping.
#define PING_INTERVAL 33 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define MAIN_LOOP    125 // Milliseconds for main loop
#define OFFSET_SEND  100

unsigned long pingTimer[SONAR_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[SONAR_NUM];         // Where the ping distances are stored.
unsigned long nextSend;             // Time of the next send function
unsigned char return_distance[SONAR_NUM];

ros::NodeHandle nh;

r2robotics_msgs::SonarArray answerSonar;
ros::Publisher sonarAnswer( "sonars_feedback", &answerSonar );

float distances[SONAR_NUM];

NewPing sonar[SONAR_NUM] = {      // Sensor object array.
  NewPing(0 , 3 , MAX_DISTANCE),  // Sonar 1
  NewPing(5 , 8 , MAX_DISTANCE),  // Sonar 2
  NewPing(22 , 23 , MAX_DISTANCE),// Sonar 3
  NewPing(17 , 19 , MAX_DISTANCE),// Sonar 4
};

const unsigned int PING_OFFSET[SONAR_NUM] = {
  0                  , // Sonar 1
  PING_INTERVAL      , // Sonar 2
  10                 , // Sonar 3
  PING_INTERVAL + 10 , // Sonar 4
};

unsigned long diff; 
uint8_t order[SONAR_NUM];
unsigned long millis_measured[SONAR_NUM];
unsigned long pingTimerInstant[SONAR_NUM];

//CallbackFunction
void echoCheck_0 () {sonar[ 0 ].check_timer_void();}
void echoCheck_1 () {sonar[ 1 ].check_timer_void();}
void echoCheck_2 () {sonar[ 2 ].check_timer_void();}
void echoCheck_3 () {sonar[ 3 ].check_timer_void();}

void (*echoCheck[SONAR_NUM])() = {
  echoCheck_0,
  echoCheck_1,
  echoCheck_2,
  echoCheck_3,
};

void sendROS(){
    for(int k=0; k<SONAR_NUM; k++)
    {
        distances[k] = (float)return_distance[k];
    }
  answerSonar.distances = distances;
	sonarAnswer.publish( &answerSonar );
}

void oneSensorCycle() { // Sensor ping cycle complete, do something with the results.
  // The following code would be replaced with your code that does something with the ping results.
  for (uint8_t i = 0; i < SONAR_NUM; i++) {
    if (sonar[i].timer_result)
      cm[i] = sonar[i].ping_result_cm; 
    else
      cm[i] = MAX_DISTANCE;

    sonar[i].timer_stop();

    return_distance[i] = (unsigned char) cm[i];
  } 
  sendROS();
}

void setup()
{
    delay(1000); // wait 1s
    pingTimer[0] = millis() + 1000+200;           // First ping starts at 75ms, gives time for the Arduino to chill before starting.
    for (uint8_t i = 0; i < SONAR_NUM; i++) {// Set the starting time for each sensor.
        pingTimer[i] = pingTimer[0] + PING_OFFSET[i];
    }
    nextSend = pingTimer[0] + OFFSET_SEND;

	nh.initNode();
	answerSonar.distances_length = SONAR_NUM;
	nh.advertise( sonarAnswer );
}

uint8_t iOrder = 0;
unsigned long current_milli;

void loop()
{
  // while( !sync ) delay(1); // Prevent overheating
	nh.spinOnce();

    for (uint8_t i = 0; i < SONAR_NUM; i++) { // Loop through all the sensors.
        current_milli = millis();
        if (current_milli >= pingTimer[i]) {// Is it this sensor's time to ping
          order[iOrder] = i;
          millis_measured[iOrder] = current_milli;
          pingTimerInstant[iOrder] = pingTimer[i];
          iOrder++;  
          pingTimer[i] += MAIN_LOOP ;  // Set next time this sensor will be pinged.
          sonar[i].ping_timer_void(*echoCheck[i]); // Do the ping (processing continues, interrupt will call echoCheck to look for echo).
        }
    }
    if (millis() > nextSend){
        iOrder = 0;
        oneSensorCycle();
        nextSend += MAIN_LOOP;
    }

	// delay(10);
	// sync = false;
}
