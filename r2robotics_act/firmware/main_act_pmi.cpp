#include <ros.h>

#include "ServoPWM.h"
#include "Servo_Controller.h"

#include <r2robotics_msgs/Bumpers.h>
#include <r2robotics_msgs/ServosPWM_MSG.h>
#include <r2robotics_msgs/ServoRawMsg.h>

#define NUM_BUMPERS                 2
#define PIN_SERVO_ABEILLE_DROITE    3 // Servo 2
#define PIN_SERVO_ABEILLE_GAUCHE    4 // Servo 1
#define PIN_BUMPER_STARTER          23 // B1
#define PIN_BUMPER_COULEUR          21 // B3

#define COEFF_SERVO_ABEILLE_DROITE  0.8
#define COEFF_SERVO_ABEILLE_GAUCHE  0.8

ros::NodeHandle nh;


// Servomotors
ServoController servoController( Serial2 );

//*****
// Publisher that sends informations about the servomotors.
//*****
uint8_t answerIntern[ HKL_MAX_MSG_SIZE - 5 ];
r2robotics_msgs::ServoRawMsg answer;
ros::Publisher servoAnswer( "servos_act_ans", &answer );
//*****

//*****
// Subscriber that receives the orders for servomotors and handles them.
//****
void servoMsgCallback( const r2robotics_msgs::ServoRawMsg& cmd_msg )
{
	if( cmd_msg.data_length >= 2 )
	{
		servoController.sendMessage( cmd_msg.data[ 0 ], cmd_msg.data[ 1 ], &( cmd_msg.data[ 2 ] ),
		                             cmd_msg.data_length - 2 );
		
		if( servoController.waitingAnswer() )
		{
			answer.data_length = servoController.getAnswer( answerIntern );
			answer.data = answerIntern;
			servoAnswer.publish( &answer );
		}
	}
}
ros::Subscriber< r2robotics_msgs::ServoRawMsg > servoHandling( "servos_act_rq", servoMsgCallback );


//*****
// Actuators objects
//****
ServoPWM servoAbeille_gauche;
ServoPWM servoAbeille_droite;
bool bumpers[NUM_BUMPERS];

r2robotics_msgs::Bumpers answerBumpers;
ros::Publisher bumpersAnswer( "bumpers_feedback", &answerBumpers );

void servosPWMCB(const r2robotics_msgs::ServosPWM_MSG& servoRequest){
    servoAbeille_gauche.servoDegrees((int)servoRequest.bras_abeille_gauche);
    servoAbeille_droite.servoDegrees((int)servoRequest.bras_abeille_droite);
}
ros::Subscriber< r2robotics_msgs::ServosPWM_MSG > servosPWMRequest( "servos_PWM", servosPWMCB );

//*****
// Manage time and loop
//****
IntervalTimer mainLoopTimer{};
constexpr unsigned long mainLoopPeriod{ 20000 }; // μs
volatile bool sync{ false };
void syncing()
{
	sync = true;
}

void setup()
{
    servoAbeille_gauche.init( PIN_SERVO_ABEILLE_GAUCHE, 0.8 );
    servoAbeille_droite.init( PIN_SERVO_ABEILLE_DROITE, 0.8 );

    servoAbeille_gauche.servoDegrees(8);
    servoAbeille_droite.servoDegrees(213);

	pinMode( PIN_BUMPER_STARTER, INPUT_PULLDOWN );
	pinMode( PIN_BUMPER_COULEUR, INPUT_PULLDOWN );

	// Begin comm' at the default baudrate
	servoController.begin( 115200 );
	// Change baudrate to 500000 bauds
	byte changeBaudrate[]{ 0x04, 0x01, 0x03 };
	servoController.sendMessage( 0xFE, 0x01, changeBaudrate, 0x03 );
	// Reboot to apply new baudrate
	servoController.sendMessage( 0xFE, 0x09, nullptr, 0x00 );
	servoController.begin( 500000 );
	// Clear status errors that may occur if baudrate was previously set
	byte clearErrors[]{ 0x30, 0x02, 0x00, 0x00 };
	servoController.sendMessage( 0xFE, 0x03, clearErrors, 0x04 );
	// Start the servomotors
	delay( 500 );
	byte activate[]{ 52, 0x01, 0x60 };
	servoController.sendMessage( 0xFE, 0x03, activate, 0x03 );
	
	nh.initNode();
	
	answerBumpers.bumpers_length = NUM_BUMPERS;
	
	nh.advertise( servoAnswer );
	nh.subscribe( servoHandling );
	
	nh.subscribe( servosPWMRequest );
	
	nh.advertise( bumpersAnswer );
	
	servoController.clearSerial();
	
	mainLoopTimer.begin( syncing, mainLoopPeriod );
	mainLoopTimer.priority( 0 );
}

void loop()
{
	while( !sync ) delay(1); // Prevent overheating
	
	nh.spinOnce();

	bumpers[0] = (digitalRead( PIN_BUMPER_STARTER ) == HIGH); // STARTER
	bumpers[1] = (digitalRead( PIN_BUMPER_COULEUR ) == HIGH); // COULEUR

	answerBumpers.bumpers = bumpers;
	bumpersAnswer.publish( &answerBumpers );
	
	servoController.clearSerial();
	
	sync = false;
}
