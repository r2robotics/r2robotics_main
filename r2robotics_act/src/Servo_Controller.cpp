//
// Created by ksyy on 26/12/16.
//

#include "Servo_Controller.h"
#include <vector>
#include <algorithm>
#include <cmath>

#include "../common/herkulex_keywords.h"


constexpr ArrayDouble< 1024 > ServoController::ServoValues[ 3 ];


ServoController::ServoController()
	: ServoController( "servos_act_rq", "servos_act_ans" )
{}

ServoController::ServoController( std::string servoRequestTopic, std::string servoAnswerTopic )
  : _nodeHandle{ nullptr }
  , _servoRequestTopic{ servoRequestTopic }
  , _servoAnswerTopic{ servoAnswerTopic }
{}

void ServoController::registerServices( ros::NodeHandle *nh, std::string prefix )
{
	_nodeHandle = nh;

	_servoRequest = _nodeHandle->advertise< r2robotics_msgs::ServoRawMsg >( _servoRequestTopic, 10 );
	_servoAnswer = _nodeHandle->subscribe< r2robotics_msgs::ServoRawMsg >( _servoAnswerTopic, 10,
	                                                                                 &ServoController::processAnswer, this );

	_servers.push_back( _nodeHandle->advertiseService( prefix + "eepWrite", &ServoController::eepWrite, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "eepRead", &ServoController::eepRead, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "ramWrite", &ServoController::ramWrite, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "ramRead", &ServoController::ramRead, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "iJog", &ServoController::iJog, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "sJog", &ServoController::sJog, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "stat", &ServoController::stat, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "rollback", &ServoController::rollback, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "reboot", &ServoController::reboot, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "activate", &ServoController::activate, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "deactivate", &ServoController::deactivate, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "move", &ServoController::move, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "moveSimultaneous", &ServoController::moveSimultaneous, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "getPosition", &ServoController::getPosition, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "clearErrors", &ServoController::clearErrors, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "turn", &ServoController::turn, this ) );
	_servers.push_back( _nodeHandle->advertiseService( prefix + "turnSimultaneous", &ServoController::turnSimultaneous, this ) );
}


bool ServoController::eepWrite( r2robotics_srvs::eep_write::Request  &req,
                                r2robotics_srvs::eep_write::Response &answer )
{
	std::vector< uint8_t > message{};
	message.reserve( 4 + req.data.size() );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_EEP_WRITE );
	message.emplace_back( req.address );
	message.emplace_back( req.data.size() );
	std::copy( req.data.begin(), req.data.end(), std::back_inserter( message ) );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::eepRead( r2robotics_srvs::eep_read::Request  &req,
                               r2robotics_srvs::eep_read::Response &answer )
{
	_lastAnswerReceived.clear();

	std::vector< uint8_t > message{};
	message.reserve( 4 );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_EEP_READ );
	message.emplace_back( req.address );
	message.emplace_back( req.length );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	// Spins the node to get the message (ros::topic::waitForMessage is too slow to set up and miss the answer)
	ros::Rate loopRate( 250 );
	for( int i{ 0 } ; i < 10 && _lastAnswerReceived.size() < 4 ; ++i )
	{
		ros::spinOnce();
		loopRate.sleep();
	}

	if( _lastAnswerReceived.size() >= 4)
	{
		std::copy( _lastAnswerReceived.begin() + 4, _lastAnswerReceived.end() - 2, std::back_inserter( answer.data ) );
		decodeStatus( *(_lastAnswerReceived.end() - 2), *(_lastAnswerReceived.end() - 1), answer.status );
		return true;
	}

	return false;
}

bool ServoController::ramWrite( r2robotics_srvs::ram_write::Request  &req,
                                r2robotics_srvs::ram_write::Response &answer )
{
	std::vector< uint8_t > message{};
	message.reserve( 4 + req.data.size() );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_RAM_WRITE );
	message.emplace_back( req.address );
	message.emplace_back( req.data.size() );
	std::copy( req.data.begin(), req.data.end(), std::back_inserter( message ) );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::ramRead( r2robotics_srvs::ram_read::Request  &req,
                               r2robotics_srvs::ram_read::Response &answer )
{
	_lastAnswerReceived.clear();

	std::vector< uint8_t > message{};
	message.reserve( 4 );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_RAM_READ );
	message.emplace_back( req.address );
	message.emplace_back( req.length );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	// Spins the node to get the message (ros::topic::waitForMessage is too slow to set up and miss the answer)
	ros::Rate loopRate( 250 );
	for( int i{ 0 } ; i < 10 && _lastAnswerReceived.size() < 4 ; ++i )
	{
		ros::spinOnce();
		loopRate.sleep();
	}

	if( _lastAnswerReceived.size() >= 4)
	{
		std::copy( _lastAnswerReceived.begin() + 4, _lastAnswerReceived.end() - 2, std::back_inserter( answer.data ) );
		decodeStatus( *(_lastAnswerReceived.end() - 2), *(_lastAnswerReceived.end() - 1), answer.status );
		return true;
	}

	return false;
}

bool ServoController::iJog( r2robotics_srvs::i_jog::Request  &req,
                            r2robotics_srvs::i_jog::Response &answer )
{
	if( req.idServos.size() != req.modeSpeeds.size() ||
			req.idServos.size() != req.positions_speeds.size() ||
			req.idServos.size() != req.playtimes.size() ||
			req.idServos.size() != req.redLeds.size() ||
			req.idServos.size() != req.blueLeds.size() ||
			req.idServos.size() != req.greenLeds.size() )
		return false;

	std::vector< uint8_t > message{};
	message.reserve( 2 + req.idServos.size() * 5 );

	message.emplace_back( HKL_ALL_ADDRESS );
	message.emplace_back( HKL_ACK_I_JOG );

	for( int i{ 0 } ; i < req.idServos.size() ; ++i )
	{
		message.emplace_back( static_cast< uint8_t >( req.positions_speeds[ i ] & 0xFF ) );
		message.emplace_back( static_cast< uint8_t >( ( req.positions_speeds[ i ] >> 8 ) & 0xFF ) );
		message.emplace_back( ( req.modeSpeeds[ i ] ? 0x02 : 0 ) |
				                  ( req.greenLeds[ i ] ? 0x04 : 0 )  |
				                  ( req.blueLeds[ i ] ? 0x08 : 0 )   |
				                  ( req.redLeds[ i ] ? 0x10 : 0 ) );
		message.emplace_back( req.idServos[ i ] );
		message.emplace_back( req.playtimes[ i ] );
	}

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::sJog( r2robotics_srvs::s_jog::Request  &req,
                            r2robotics_srvs::s_jog::Response &answer )
{
	if( req.idServos.size() != req.modeSpeeds.size() ||
	    req.idServos.size() != req.positions_speeds.size() ||
	    req.idServos.size() != req.redLeds.size() ||
	    req.idServos.size() != req.blueLeds.size() ||
	    req.idServos.size() != req.greenLeds.size() )
		return false;

	std::vector< uint8_t > message{};
	message.reserve( 3 + req.idServos.size() * 4 );

	message.emplace_back( HKL_ALL_ADDRESS );
	message.emplace_back( HKL_ACK_S_JOG );
	message.emplace_back( req.playtime );

	for( int i{ 0 } ; i < req.idServos.size() ; ++i )
	{
		message.emplace_back( static_cast< uint8_t >( req.positions_speeds[ i ] & 0xFF ) );
		message.emplace_back( static_cast< uint8_t >( ( req.positions_speeds[ i ] >> 8 ) & 0xFF ) );
		message.emplace_back( ( req.modeSpeeds[ i ] ? 0x02 : 0 ) |
		                        ( req.greenLeds[ i ] ? 0x04 : 0 )  |
		                        ( req.blueLeds[ i ] ? 0x08 : 0 )   |
		                        ( req.redLeds[ i ] ? 0x10 : 0 ) );
		message.emplace_back( req.idServos[ i ] );
	}

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::stat( r2robotics_srvs::stat::Request  &req,
                            r2robotics_srvs::stat::Response &answer )
{
	_lastAnswerReceived.clear();

	std::vector< uint8_t > message{};
	message.reserve( 4 );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_ACK_STAT );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	// Spins the node to get the message (ros::topic::waitForMessage is too slow to set up and miss the answer)
	ros::Rate loopRate( 250 );
	for( int i{ 0 } ; i < 10 && _lastAnswerReceived.size() < 4 ; ++i )
	{
		ros::spinOnce();
		loopRate.sleep();
	}

	if( _lastAnswerReceived.size() >= 4)
	{
		decodeStatus( *(_lastAnswerReceived.end() - 2 ), *(_lastAnswerReceived.end() - 1), answer.status );
		return true;
	}

	return false;
}

bool ServoController::rollback( r2robotics_srvs::rollback::Request  &req,
                                r2robotics_srvs::rollback::Response &answer )
{
	std::vector< uint8_t > message{};
	message.reserve( 4 );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_ACK_ROLLBACK );
	message.emplace_back( req.rollbackID ? 0 : 1 );
	message.emplace_back( req.rollbackBaudrate ? 0 : 1 );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::reboot( r2robotics_srvs::reboot::Request  &req,
                              r2robotics_srvs::reboot::Response &answer )
{
	std::vector< uint8_t > message{};
	message.reserve( 2 );

	message.emplace_back( req.idServo );
	message.emplace_back( HKL_ACK_REBOOT );

	_servoRawMsg.data = message;
	_servoRequest.publish( _servoRawMsg );

	return true;
}

bool ServoController::activate( r2robotics_srvs::activate::Request  &req,
                                r2robotics_srvs::activate::Response &answer )
{
	r2robotics_srvs::ram_write::Request reqInt{};
	r2robotics_srvs::ram_write::Response ansInt{};

	reqInt.idServo = req.idServo;
	reqInt.address = HKL_RAM_TORQUE_CONTROL;
	reqInt.data = std::vector< uint8_t >{ 0x60 };

	return ramWrite( reqInt, ansInt );
}

bool ServoController::deactivate( r2robotics_srvs::deactivate::Request  &req,
                                  r2robotics_srvs::deactivate::Response &answer )
{
	r2robotics_srvs::ram_write::Request reqInt{};
	r2robotics_srvs::ram_write::Response ansInt{};

	reqInt.idServo = req.idServo;
	reqInt.address = HKL_RAM_TORQUE_CONTROL;
	reqInt.data = std::vector< uint8_t >{ 0 };

	return ramWrite( reqInt, ansInt );
}

bool ServoController::move( r2robotics_srvs::move::Request  &req,
                            r2robotics_srvs::move::Response &answer )
{
	r2robotics_srvs::i_jog::Request reqInt{};
	r2robotics_srvs::i_jog::Response ansInt{};

	reqInt.idServos = req.idServos;

	reqInt.modeSpeeds.resize( req.idServos.size() );
	std::fill( reqInt.modeSpeeds.begin(), reqInt.modeSpeeds.end(), false );

	for( int i{ 0 } ; i < req.angles.size() ; ++i )
	{
		if( req.idServos[ i ] < 4 )
			reqInt.positions_speeds.emplace_back( stepFromAngle( req.idServos[ i ], req.angles[ i ] ) );
		else
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( static_cast< int16_t >( req.angles[ i ] / 0.32046f ) + 512 ) );
	}

	for( const auto& time : req.times )
		reqInt.playtimes.emplace_back( static_cast< uint8_t >( static_cast< float >( time ) / 11.2f ) );

	reqInt.redLeds.resize( req.idServos.size() );
	std::fill( reqInt.redLeds.begin(), reqInt.redLeds.end(), false );

	reqInt.blueLeds.resize( req.idServos.size() );
	std::fill( reqInt.blueLeds.begin(), reqInt.blueLeds.end(), false );

	reqInt.greenLeds.resize( req.idServos.size() );
	std::fill( reqInt.greenLeds.begin(), reqInt.greenLeds.end(), false );

	return iJog( reqInt, ansInt );
}

bool ServoController::turn( r2robotics_srvs::turn::Request  &req,
                            r2robotics_srvs::turn::Response &answer )
{
	r2robotics_srvs::i_jog::Request reqInt{};
	r2robotics_srvs::i_jog::Response ansInt{};

	reqInt.idServos = req.idServos;

	reqInt.modeSpeeds.resize( req.idServos.size() );
	std::fill( reqInt.modeSpeeds.begin(), reqInt.modeSpeeds.end(), true );

	for( int i{ 0 } ; i < req.speeds.size() ; ++i )
	{
		if( req.speeds[ i ] > 0)
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( std::clamp( static_cast< int >( req.speeds[ i ] ), 0, 1023 ) ) );
		else
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( 16384 - std::clamp( static_cast< int >( req.speeds[ i ] ), -1023, 0 ) ) );
	}

	for( const auto& time : req.times )
		reqInt.playtimes.emplace_back( static_cast< uint8_t >( static_cast< float >( time ) / 11.2f ) );

	reqInt.redLeds.resize( req.idServos.size() );
	std::fill( reqInt.redLeds.begin(), reqInt.redLeds.end(), false );
	reqInt.greenLeds = reqInt.blueLeds = reqInt.redLeds;

	return iJog( reqInt, ansInt );
}

bool ServoController::moveSimultaneous( r2robotics_srvs::move_simultaneous::Request  &req,
                                        r2robotics_srvs::move_simultaneous::Response &answer )
{
	r2robotics_srvs::s_jog::Request reqInt{};
	r2robotics_srvs::s_jog::Response ansInt{};

	reqInt.playtime = static_cast< uint8_t >( static_cast< float >( req.time ) / 11.2f );

	reqInt.idServos = req.idServos;

	reqInt.modeSpeeds.resize( req.idServos.size() );
	std::fill( reqInt.modeSpeeds.begin(), reqInt.modeSpeeds.end(), false );

	for( int i{ 0 } ; i < req.angles.size() ; ++i )
	{
		if( req.idServos[ i ] < 4 )
			reqInt.positions_speeds.emplace_back( stepFromAngle( req.idServos[ i ], req.angles[ i ] ) );
		else
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( static_cast< int16_t >( req.angles[ i ] / 0.32046f ) + 512 ) );
	}

	reqInt.redLeds.resize( req.idServos.size() );
	std::fill( reqInt.redLeds.begin(), reqInt.redLeds.end(), false );

	reqInt.blueLeds.resize( req.idServos.size() );
	std::fill( reqInt.blueLeds.begin(), reqInt.blueLeds.end(), false );

	reqInt.greenLeds.resize( req.idServos.size() );
	std::fill( reqInt.greenLeds.begin(), reqInt.greenLeds.end(), false );

	return sJog( reqInt, ansInt );
}

bool ServoController::turnSimultaneous( r2robotics_srvs::turn_simultaneous::Request  &req,
                            			r2robotics_srvs::turn_simultaneous::Response &answer )
{
	r2robotics_srvs::s_jog::Request reqInt{};
	r2robotics_srvs::s_jog::Response ansInt{};

	reqInt.playtime = static_cast< uint8_t >( static_cast< float >( req.time ) / 11.2f );

	reqInt.idServos = req.idServos;

	reqInt.modeSpeeds.resize( req.idServos.size() );
	std::fill( reqInt.modeSpeeds.begin(), reqInt.modeSpeeds.end(), true );

	for( int i{ 0 } ; i < req.speeds.size() ; ++i )
	{
		if( req.speeds[ i ] > 0)
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( std::clamp( static_cast< int >( req.speeds[ i ] ), 0, 1023 ) ) );
		else
			reqInt.positions_speeds.emplace_back( static_cast< uint16_t >( 16384 - std::clamp( static_cast< int >( req.speeds[ i ] ), -1023, 0 ) ) );
	}

	reqInt.redLeds.resize( req.idServos.size() );
	std::fill( reqInt.redLeds.begin(), reqInt.redLeds.end(), false );
	reqInt.greenLeds = reqInt.blueLeds = reqInt.redLeds;

	return sJog( reqInt, ansInt );
}

bool ServoController::getPosition( r2robotics_srvs::get_position::Request  &req,
                                   r2robotics_srvs::get_position::Response &answer )
{
	r2robotics_srvs::ram_read::Request reqInt{};
	r2robotics_srvs::ram_read::Response ansInt{};

	reqInt.idServo = req.idServo;
	reqInt.address = HKL_RAM_CALIBRATED_POSITION;
	reqInt.length = 2;

	if( !ramRead( reqInt, ansInt ) )
		return false;

	// - to be in the robot's referential frame.
	if( req.idServo < 4 )
		answer.angle = - R2Robotics::radToDeg( static_cast< float >( ServoValues[ req.idServo - 1 ].
				values[ ansInt.data[ 0 ] + ( ansInt.data[ 1 ] - 96 ) * 256 ] ) );
	else
		answer.angle = ( ( ansInt.data[ 0 ] + ( ansInt.data[ 1 ] - 96 ) * 256 ) - 512 ) * 0.32046f;

	return true;
}

bool ServoController::clearErrors( r2robotics_srvs::clear_errors::Request &req,
                                   r2robotics_srvs::clear_errors::Response &answer )
{
	r2robotics_srvs::ram_write::Request reqInt{};
	r2robotics_srvs::ram_write::Response ansInt{};

	reqInt.idServo = req.idServo;
	reqInt.address = HKL_RAM_STATUS_ERROR;
	reqInt.data = std::vector< uint8_t >{ 0, 0 };

	return ramWrite( reqInt, ansInt );
}

void ServoController::processAnswer( const r2robotics_msgs::ServoRawMsg::ConstPtr& msg )
{
	_lastAnswerReceived.clear();
	std::copy( msg->data.begin(), msg->data.end(), std::back_inserter( _lastAnswerReceived ) );
}

void ServoController::decodeStatus( const uint8_t error, const uint8_t details, r2robotics_msgs::Status &status )
{
	status.voltMaxExceeded = error & 0x01;
	status.posOutOfRange = error & 0x02;
	status.tempMaxExceeded = error & 0x04;
	status.invalidPacket = error & 0x08;
	status.overload = error & 0x10;
	status.driverFault = error & 0x20;
	status.registerDistorted = error & 0x40;
	status.moving = details & 0x01;
	status.inPosition = details & 0x02;
	status.checksumError = details & 0x04;
	status.unknownCommand = details & 0x08;
	status.regOutOfRange = details & 0x10;
	status.garbage = details & 0x20;
	status.motorOn = details & 0x40;
}

int ServoController::stepFromAngle( const int32_t servoId, const float angleDeg ) const
{
	if( servoId > 3 )
		return static_cast< int >( angleDeg / 0.32046f ) + 512;
	
	// - to be in the robot's referential frame.
	int step{ static_cast< int >( angleDeg / 0.32046f ) + 512 };	
	double angleRad{ R2Robotics::degToRad( -angleDeg ) };

	for( const int dir{ ServoValues[ servoId  - 1].values[ step ] < angleRad ? 1 : -1 } ;
	     ( std::abs( ServoValues[ servoId - 1 ].values[ step ] - angleRad ) >
	       std::abs( ServoValues[ servoId - 1 ].values[ step + dir ] - angleRad ) ) &&
	     step >= 0 && step <= 1023 ;
	     step += dir );

	return step;
}
