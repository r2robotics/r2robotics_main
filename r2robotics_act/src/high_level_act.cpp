//
// Created by ksyy on 08/05/18.
//

#include "high_level_act.h"

#include "../common/common_operators.h"

#include "r2robotics_srvs/move_simultaneous.h"
#include "r2robotics_srvs/get_position.h"
#include "r2robotics_srvs/move.h"
#include "r2robotics_srvs/turn.h"


HighLevelAct::HighLevelAct()
  : _servers{}
  , _nodeHandle{ nullptr }
{}


void HighLevelAct::registerServices( ros::NodeHandle *nh, std::string prefix )
{
	_nodeHandle = nh;
	_prefix = prefix;

	_servers.push_back( _nodeHandle->advertiseService( _prefix + "moveDirection", &HighLevelAct::moveDirection, this ) );
}

bool HighLevelAct::moveDirection( r2robotics_srvs::move_direction::Request  &req,
			   		   				  r2robotics_srvs::move_direction::Response &answer )
{
	if( req.angle < -160.f || req.angle > 160.f )
		return false;

	auto clientGetPosition = _nodeHandle->serviceClient< r2robotics_srvs::get_position >( _prefix + "getPosition" );
	r2robotics_srvs::get_position srvGetPosition{};
	srvGetPosition.request.idServo = req.idServo;

	auto clientMove = _nodeHandle->serviceClient< r2robotics_srvs::move >( _prefix + "move" );
	r2robotics_srvs::move srvMove{};
	srvMove.request.idServos = std::vector< uint8_t >{ req.idServo };
	srvMove.request.times = std::vector< uint16_t >{ req.time };
	srvMove.request.angles = std::vector< float >{ req.angle };

	auto clientTurn = _nodeHandle->serviceClient< r2robotics_srvs::turn >( _prefix + "turn" );
	r2robotics_srvs::turn srvTurn{};
	srvTurn.request.idServos = std::vector< uint8_t >{ req.idServo };
	srvTurn.request.times = std::vector< uint16_t >{ req.time };
	srvTurn.request.speeds = std::vector< int16_t >{ ( req.direction ? 1024 : -1024 ) };


	if( !clientGetPosition.call( srvGetPosition ) )
		return false;

	if( R2Robotics::absoluteValue( srvGetPosition.response.angle - req.angle ) < 3.0 )
		return clientMove.call( srvMove );

	// Check if a call to the move command will go in the wrong direction
	if( ( srvGetPosition.response.angle > req.angle && req.direction ) ||
	    ( srvGetPosition.response.angle < req.angle && !req.direction ) )
	{
		float startAngle{ srvGetPosition.response.angle };

		// Range in which we want the angle to be at the end of the turn command
		float goalMin{ req.direction ? -160.f : 145.f };
		float goalMax{ req.direction ? -145.f : 160.f };
		
		// We need to turn the servomotor manually until a better starting position for the move command
		// is found (-160° if clockwise, 160° if anti-clockwise)
		if( !clientTurn.call( srvTurn ) )
			return false;

		// Durant le "trou", passe par 255/99, 255/99, 8/98, 8/98, 0/96, 0/96 (ou l'inverse à l'envers...)
		// Checks current position
		while( srvGetPosition.response.angle < goalMin || srvGetPosition.response.angle > goalMax )
			if( !clientGetPosition.call( srvGetPosition ) )
				return false;
	}
	
	return clientMove.call( srvMove );
}

int main( int argc, char** argv )
{
	ros::init( argc, argv, "HighLevelAct" );
	auto n = std::make_unique< ros::NodeHandle >();

	HighLevelAct highLevelAct{};
	highLevelAct.registerServices( n.get(), "act_" );

	ros::spin();

	return 0;
}
