//
// Created by ksyy on 08/05/18.
//
#include <ros/ros.h>

#include "Servo_Controller.h"



int main( int argc, char** argv ){
    ROS_INFO("Started");
    ros::init(argc, argv, "lowLevelActNode");
    ROS_INFO("LowLevelActNode initiated");
    auto n = std::make_unique< ros::NodeHandle >();

    ServoController servoController{ "servos_act_rq", "servos_act_ans" };
    servoController.registerServices( n.get(), "act_" );

    ros::spin();

    return 0;
}
