#ifndef STATE_H
#define STATE_H

#include "point.h"
#include "robot.h"

class state {
 public:
  state() {};
  state(int _x, int _y) {
    x = _x;
    y = _y;
    cap = 0;
  }
  state(int _x, int _y, float _cap) {
    x = _x;
    y = _y;
    cap = _cap;
  }
  state(RobotPosition pos) {
    x = pos.x;
    y = pos.y;
    cap = pos.cap;
  }
  state(Point p) {
    x = p.x;
    y = p.y;
    cap = 0;
  }
  state(IntermediatePoint p) {
    x = p.x;
    y = p.y;
    cap = 0;
  }
  int x;
  int y;
  float cap;
  pair<double,double> k;

  bool operator == (const state &s2) const {
    return ((x == s2.x) && (y == s2.y));
  }

  bool operator != (const state &s2) const {
    return ((x != s2.x) || (y != s2.y));
  }

  bool operator > (const state &s2) const {
    if (k.first-0.00001 > s2.k.first) return true;
    else if (k.first < s2.k.first-0.00001) return false;
    return k.second > s2.k.second;
  }

  bool operator <= (const state &s2) const {
    if (k.first < s2.k.first) return true;
    else if (k.first > s2.k.first) return false;
    return k.second < s2.k.second + 0.00001;
  }

  bool operator < (const state &s2) const {
    if (k.first + 0.000001 < s2.k.first) return true;
    else if (k.first - 0.000001 > s2.k.first) return false;
    return k.second < s2.k.second;
  }

};

#endif
