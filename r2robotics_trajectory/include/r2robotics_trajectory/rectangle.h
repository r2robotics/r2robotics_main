#ifndef RECTANGLE
#define RECTANGLE

#include "point.h"

class Rectangle {
  public:
    Point p1;
	Point p2;
	Point p3;
	Point p4;

    Rectangle() {};
	Rectangle(Point _p1, Point _p2, Point _p3, Point _p4) {
        update(_p1, _p2, _p3, _p4);
    }
	Rectangle(vector<Point> points) {
        update(points);
    }
    
	int update(Point _p1, Point _p2, Point _p3, Point _p4) {
        // CAREFUL AUX QUADRILATERES CROISES
        if ( not isCrossed(_p1, _p2, _p3, _p4) ) {
            p1 = _p1;
            p2 = _p2;
            p3 = _p3;
            p4 = _p4;
        }
        else if( not isCrossed(_p1,_p2,_p4,_p3) ) {
            p1 = _p1;
            p2 = _p2;
            p3 = _p4;
            p4 = _p3;
        }
        else if( not isCrossed(_p1,_p3,_p2,_p4) ) {
            p1 = _p1;
            p2 = _p3;
            p3 = _p2;
            p4 = _p4;
        }
        else {
            p1 = _p1;
            p2 = _p3;
            p3 = _p4;
            p4 = _p2;
        }
        return 1;
    }

    int update(vector<Point> points) {
        return update(points[0], points[1], points[2], points[3]);
        return 1;
    }

};

#endif //RECTANGLE