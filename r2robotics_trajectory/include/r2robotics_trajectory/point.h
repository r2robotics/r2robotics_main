#ifndef STRUCT_POINT
#define STRUCT_POINT

#include <cmath>

using namespace std;

struct Point
{
  double x;
  double y;
  Point() {}
  Point( double _x, double _y): x(_x), y(_y) {}
  Point Add( Point p2 ) {return Point(x+p2.x, y+p2.y); }
  Point Multiply( double k ) { return Point(x*k, y*k); }
  Point MidPoint( Point p2 ) {return Point( (x+p2.x)/2, (y+p2.y)/2 ); }
  Point Normal( Point p2 ) { return Point( p2.y - y, x - p2.x ).Normalize(); }
  Point Normalize() { return Point( x/Norm(), y/Norm() ); }
  double Norm() { return sqrt( pow(x,2) + pow(y,2) ); }
};

struct IntermediatePoint
{
  double x;
  double y;
  std::vector<double> ok_caps;
  IntermediatePoint() {}
  IntermediatePoint( double _x, double _y): x(_x), y(_y) {}
  IntermediatePoint( double _x, double _y, std::vector<double> _caps): x(_x), y(_y),
    ok_caps(_caps) {}

  bool is_cap_ok(double cap) {
    if( std::find(ok_caps.begin(), ok_caps.end(),cap)!=ok_caps.end() ) {
      return true;
    }
  }
  double distance_to(double x1, double y1) {
    return sqrt( pow(x1-x,2) + pow(y1-y,2) );
  }

  double distance_to_line(Point p1, Point p2) {
    return distance_to_line(p1.x, p1.y, p2.x, p2.y);
  }
  double distance_to_line(double x1, double y1, double x2, double y2) {
    // line is a*x + b*y + c = 0
    double above = fabs( (y2-y1)*x - (x2-x1)*y + x2*y1 - y2*x1 );
    double below = sqrt( pow(y2 - y1,2) + pow(x2-x1,2) );
    double res = above / below;
    return res;
  }
};

double CCW(Point a, Point b, Point c);
int intersect(Point a, Point b, Point c, Point d);
bool isCrossed(const Point p1, const Point p2, const Point p3, const Point p4);


#endif
