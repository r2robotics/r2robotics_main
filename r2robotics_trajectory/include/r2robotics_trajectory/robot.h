#ifndef ROBOT
#define ROBOT

#include "point.h"
#include "angles.h"
#include <r2robotics_msgs/RobotPath.h>
#include <r2robotics_msgs/RobotPosition.h>

class Robot
{
private:
  Point center;
  
public:
  bool active;
  double radius;
  vector<Point> collisionPoints;

  Robot() { 
    active = false; 
  }

  Robot(Point c, int r)  {
    active = true;
    center = c;
    radius = r;
  }

  Robot(Point c, int r, bool seen){
    center = c;
    radius = r;
    active = seen;
  }

  int update(Point c) {
    center = c;
    active = true;
    return 1;
  }

  void initCollisionPoints() {
    collisionPoints.push_back( Point( 198.96, 21.54 ) );
    collisionPoints.push_back( Point( 6.80, 213.69 ) );
    collisionPoints.push_back( Point( -107.65, 169.76 ) );
    collisionPoints.push_back( Point( -125.94, 143.15 ) );
    collisionPoints.push_back( Point( -125.94, -143.15 ) );
    collisionPoints.push_back( Point( -107.65, -169.76 ) );
    collisionPoints.push_back( Point( 6.80, -213.69 ) );
    collisionPoints.push_back( Point( 198.96, -21.54 ) );
  }

  void setRadius(double _radius) {
    radius = _radius; 
  }

  vector<Point> getCollisionPoints(Point centre, double cap) {
    vector<Point> res;
    for(int i=0; i<collisionPoints.size(); i++) {
      double new_x = collisionPoints.at(i).x * cos( DEG2RAD(cap) ) - collisionPoints.at(i).y * sin( DEG2RAD(cap) );
      double new_y = collisionPoints.at(i).x * sin( DEG2RAD(cap) ) + collisionPoints.at(i).y * cos( DEG2RAD(cap) );
      res.push_back ( Point( centre.x + new_x, centre.y + new_y ) );
    }
    return res;
  }
};

#endif
