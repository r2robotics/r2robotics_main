#ifndef CHARARRAY_TO_INT
#define CHARARRAY_TO_INT

#include <iostream>
#include <sstream>

int catoi(const char* value) {
  stringstream strValue;
  strValue << value;
  int intValue;
  strValue >> intValue;
  return intValue;
}

double catod(const char* value) {
  stringstream strValue;
  strValue << value;
  double doubleValue;
  strValue >> doubleValue;
  return doubleValue;
}

#endif
