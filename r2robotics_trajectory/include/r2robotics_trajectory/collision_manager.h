#ifndef COLLISION_MANAGER
#define COLLISION_MANAGER

#include <memory>
#include <string>
#include <vector>
#include <cmath>
#include <limits>

#include "angles.h"
#include "pose.h"

#include "fcl/fcl.h"

using CollisionGeometryPtr_t = std::shared_ptr<fcl::CollisionGeometry<double>>;
using TransformD = fcl::Transform3<double>;
using VectorD = fcl::Vector3<double>;
using Vector3D = fcl::Vector3<double>;
using CollisionObjectD = fcl::CollisionObject<double>;
using CollisionRequestD = fcl::CollisionRequest<double>;
using CollisionResultD = fcl::CollisionResult<double>;
using DistanceRequestD = fcl::DistanceRequest<double>;
using DistanceResultD = fcl::DistanceResult<double>;
using Translation3D = fcl::Translation3<double>;
typedef fcl::Box<double> Box_t;
typedef std::shared_ptr<Box_t> BoxPtr_t;
typedef fcl::Cylinder<double> Cylinder_t;
typedef std::shared_ptr<Cylinder_t> CylinderPtr_t;
typedef fcl::Halfspace<double> Halfspace_t;
typedef std::shared_ptr<Halfspace_t> HalfspacePtr_t;
typedef std::shared_ptr<fcl::ShapeBase<double>> ShapeBasePtr_t;
typedef fcl::CollisionGeometry<double> CollisionGeometry_t;

enum CollisionType
{
    BOX ,
    SPHERE,
    ellipsoid,
    CAPSULE,
    CONE,
    CYLINDER,
    CONVEX,
    PLANE,
    MESH,
    OCTREE
};

struct CollisionPart
{
    std::string name;
    std::string type;
    CollisionGeometryPtr_t geometry;
    BoxPtr_t box;
    CylinderPtr_t cylinder;
    HalfspacePtr_t half_space;
    TransformD pose;

    CollisionPart() 
    {}

    CollisionPart( std::string name_, std::string type_, 
        CollisionGeometryPtr_t geometry_ )
    {
        name = name_;
        type = type_;
        geometry = geometry_;
        pose.setIdentity();
    }

    CollisionPart( std::string name_, std::string type_, 
        BoxPtr_t geometry_, TransformD initial_pose_ )
    {
        name = name_;
        type = type_;
        box = geometry_;
        pose = initial_pose_;
    }

    CollisionPart( std::string name_, std::string type_, 
        CylinderPtr_t geometry_, TransformD initial_pose_ )
    {
        name = name_;
        type = type_;
        cylinder = geometry_;
        pose = initial_pose_;
    }

    CollisionPart( std::string name_, std::string type_, 
        HalfspacePtr_t geometry_ )
    {
        name = name_;
        type = type_;
        half_space = geometry_;
        pose.setIdentity();
    }

    CollisionPart( std::string name_, std::string type_,
        CollisionGeometryPtr_t geometry_, TransformD initial_pose_ )
    {
        name = name_;
        type = type_;
        geometry = geometry_;
        pose = initial_pose_;
    }
    
    static std::shared_ptr<CollisionPart> create( std::string name_,
        std::string type_,
        CollisionGeometryPtr_t geom_ )
    {
      std::shared_ptr<CollisionPart> shPtr (new CollisionPart (name_, type_, geom_));
      return shPtr;
    }
    
    static std::shared_ptr<CollisionPart> create( std::string name_,
        std::string type_,
        CollisionGeometryPtr_t geom_,
        TransformD initial_pose_)
    {
      std::shared_ptr<CollisionPart> shPtr (new CollisionPart (name_, type_, geom_, initial_pose_));
      return shPtr;
    }

    static std::shared_ptr<CollisionPart> create( std::string name_, std::string type_, 
        BoxPtr_t geom_, TransformD initial_pose_ )
    {
      std::shared_ptr<CollisionPart> shPtr (new CollisionPart (name_, type_, geom_, initial_pose_));
      return shPtr;
    }

    static std::shared_ptr<CollisionPart> create( std::string name_, std::string type_, 
        HalfspacePtr_t geom_ )
    {
      std::shared_ptr<CollisionPart> shPtr (new CollisionPart (name_, type_, geom_));
      return shPtr;
    }

    static std::shared_ptr<CollisionPart> create( std::string name_, std::string type_, 
        CylinderPtr_t geom_, TransformD initial_pose_ )
    {
      std::shared_ptr<CollisionPart> shPtr (new CollisionPart (name_, type_, geom_, initial_pose_));
      return shPtr;
    }

};
typedef std::shared_ptr<CollisionPart> CollisionPartPtr_t;
typedef std::vector< CollisionPartPtr_t > CollisionPartsPtr_t;

class CollisionManager
{
    public:
        CollisionManager();
        static std::shared_ptr<CollisionManager> create ();
        void addRobotPart( CollisionPartPtr_t part );
        void addEnvPart( CollisionPartPtr_t part );
        void setRobotPose( double x, double y, double cap );
        void setRobotPose( Pose current );
        bool testCollision( Pose p );
        double getDistanceToObstacles( Pose p );
        Pose getDistancesToObstacles( Pose p );
        static TransformD poseToTransform( Pose pose );
        static TransformD ToTransform( double x, double y, double z, double theta );

    private:
        std::vector< CollisionPartPtr_t > robotParts;
        std::vector< CollisionPartPtr_t > environmentParts;
        TransformD currentRobotPose;
};
typedef std::shared_ptr<CollisionManager> CollisionManagerPtr_t;

#endif //COLLISION_MANAGER