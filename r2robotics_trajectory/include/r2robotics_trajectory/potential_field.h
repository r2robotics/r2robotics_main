#ifndef POTENTIAL_FIELD
#define POTENTIAL_FIELD

#include <cmath>
#include <ctime>

#include "pose.h"
#include "angles.h"
#include "collision_manager.h"

class PotentialField
{
  public:
    PotentialField() {}

    void init( CollisionManagerPtr_t collisionManager_ )
    {
        init( collisionManager_, 1, 100, 1); // 100mm = 10cm
    }

    void init( CollisionManagerPtr_t collisionManager_, double ksi, double rho,
                    double eta)
    {
        collisionManager = collisionManager_;
        setKSI( ksi);
        setRHO( rho );
        setETA( eta );
        setGoal( Pose( 0.0,0.0,0.0 ) );
        std::cout << "Potential Field initialized !" << "\n";
    }

    void setGoal( Pose goal_ )
    {
        goal = goal_;
    }

    void setKSI( double ksi )
    {
        KSI = ksi;
    }

    void setRHO( double rho )
    {
        RHO = rho;
    }

    void setETA( double eta)
    {
        ETA = eta;
    }

    double U( Pose current )
    {
      double ugoal = UGoal( current );
      double uobstacles = UObstacles( current );
    //   std::cout << "U = " << ugoal << " + " << uobstacles << " = " << ugoal + uobstacles << "\n";
      return ugoal + uobstacles;
    }

    double dU_x( Pose p, double d )
    {
        // std::cout << "=== dU_x" << "\n";
        return dUGoal_x( p ) + dUObstacles_x( p, d );
    }

    double dU_y( Pose p, double d )
    {
        // std::cout << "=== dU_y" << "\n";
        return dUGoal_y( p ) + dUObstacles_y( p, d );
    }

    double dU_yaw( Pose p, double d )
    {
        // std::cout << "=== dU_yaw" << "\n";
        return dUGoal_yaw( p ) + dUObstacles_yaw( p, d );
    }

    Pose dU( Pose p )
    {
        std::clock_t begin = std::clock();
        double UO =  UObstacles( p );

        // std::cout << "Starting calculating dU for pose " << x << "\n";
        double _x = -dU_x(p, UO);
        double _y = -dU_y(p, UO);
        double _yaw = -dU_yaw(p, UO);
        Pose res( _x, _y, _yaw );
        // std::cout << "Distance to obstacles = " << distanceToStr(distanceToObstacles(p)) << "\n";
        // std::cout << "dUGoal : " << dUGoal_x(p) << ", " << dUGoal_y(p) << " - " << dUGoal_yaw(p) << "\n";
        // std::cout << "dUObstacles : " << dUObstacles_x(p) << ", " << dUObstacles_y(p) << " - " << dUObstacles_yaw(p) << "\n";
        // std::cout << "Finished calculating dU = " << res << "\n\n";
        
        clock_t end = std::clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        // std::cout << "dU TIME ELAPSED : " << elapsed_secs << " s" << "\n";
        
        
        return res;
    }

    std::string printDistanceToObstacles( Pose p )
    {
        double d = distanceToObstacles( p );
        return distanceToStr( d );
    }

  private:
    CollisionManagerPtr_t collisionManager;
    Pose goal;
    // Positive number used to calculate the actractive potential
    double KSI;
    // Positive number, used for repulsive potential
    // representing the influence distance of obstacles
    double RHO;
    // Positive number used to calculate the repuslvie potential
    double ETA;
    // Small number used to numerically calculate derivatives
    double EPSILON = 0.001;
    double EPSILON_YAW = 0.001;

    double UGoal( Pose current )
    {
        // std::cout << "Starting calculating UGoal ..." << "\n";
        double d = distanceToGoalSquared( current );
        // std::cout << "Distance to goal = " << d << "\n";;
        return 0.5 * d * KSI;
    }

    double dUGoal_x( Pose current )
    {
        // std::cout << "dUGoal_x = " << ( current.x - goal.x ) * KSI << "\n";
        return ( current.x - goal.x ) * KSI; 
    }

    double dUGoal_y( Pose current )
    {
        // std::cout << "dUGoal_y = " << ( current.y - goal.y ) * KSI << "\n";
        return ( current.y - goal.y ) * KSI; 
    }

    double dUGoal_yaw( Pose current )
    {
        // std::cout << "dUGoal_yaw = " << ( current.yaw - goal.yaw ) * KSI << "\n";
        return get_angle_between( current.yaw, goal.yaw ) * KSI; 
    }

    double distanceToGoal( Pose current )
    {
        double d =  pow(goal.x - current.x, 2) + pow(goal.y - current.y, 2);
        double dtheta = pow(get_angle_between(goal.yaw, current.yaw), 2);
        return sqrt(d + dtheta);
    }

    double distanceToGoalSquared( Pose current )
    {
        double d =  pow(goal.x - current.x, 2) + pow(goal.y - current.y, 2);
        double dtheta = pow(get_angle_between(goal.yaw, current.yaw), 2);
        return d + dtheta;
    }

    double UObstacles( Pose current )
    {
        double d = distanceToObstacles( current );
        if( d > RHO )
        {
            return 0;
        }
        else
        {
            // std::cout << distanceToStr(d) << " < RHO" << "\n";
            // std::cout << "1/d : " << 1.0/d << "\n";
            // std::cout << "1/rho : " << 1.0/RHO << "\n";
            // std::cout << "1/d - 1/rho : " << (1.0/d - 1.0/RHO) << "\n";
            // std::cout << "pow(1.0/d - 1.0/RHO,2) : " << std::pow(1.0/d - 1.0/RHO,2) << "\n";
            return 0.5 * ETA * std::pow(1.0/d - 1.0/RHO,2);
        }
    }

    // double d = distanceToObstacles( current );
    // double temp = ETA * ( (1/d) - (1/RHO) ) / pow(d,2);




    double dUObstacles_x( Pose p, double UO )
    {
        double dx = EPSILON;
        Pose dpp (p.x + dx, p.y, p.yaw);
        // Pose dpm (p.x - dx, p.y, p.yaw);
        double res = ( UObstacles(dpp) - UO ) / (dx);
        // std::cout << "dUobstacles +x = " << res << "\n";
        return res;
    }

    double dUObstacles_y( Pose p, double UO )
    {
        double dy = EPSILON;
        Pose dpp (p.x, p.y + dy, p.yaw);
        // Pose dpm (p.x, p.y - dy, p.yaw);
        double res = ( UObstacles(dpp) - UO ) / (dy);
        // std::cout << "dUobstacles +y = " << res << "\n";
        return res;
    }

    double dUObstacles_yaw( Pose p, double UO )
    {
        // std::cout << "dUObstacles_yaw" << "\n";
        double dyaw = EPSILON;
        Pose dpp (p.x, p.y, p.yaw + dyaw);
        // Pose dpm (p.x, p.y , p.yaw - dyaw);
        double udpp = UObstacles(dpp);
        double res = ( udpp - UO ) / (dyaw);
        // std::cout << "dUobstacles +yaw = " << res << "\n";
        return res;
    }

    double distanceToObstacles( Pose current )
    {
        double res = collisionManager->getDistanceToObstacles( current );
        return res;
    }

    std::string distanceToStr( double d )
    {
        if( d < 5000)
        {
            return std::to_string(d);
        }
        else
        {
            // distance more than 5m is considered as infinity
            return "infinity";
        }
    }

};

#endif // POTENTIAL_FIELD