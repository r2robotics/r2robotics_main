## Prerequisite

Pour la visu dans rviz :
```
sudo apt install ros-kinetic-jsk-rviz-plugins
```

Pour enregistrer des vidéos de rviz :

https://jsk-docs.readthedocs.io/en/latest/jsk_visualization/doc/tips/index.html

Install OpenGL and Glut (now uneccesary) :

```
sudo apt-get install libsdl2-dev
sudo apt-get install libglex-dev
sudo apt-get install libglm-dev
sudo apt-get install freeglut3
sudo apt-get install libgl1-mesa-dev
sudo apt-get install libglu1-mesa-dev
```

Install eigen, libccd and fcl. Choose a folder of installation, any will do (here I use ~/local) :

```
sudo apt-get install build-essential cmake doxygen libqt4-dev \
	libqt4-opengl-dev libqglviewer-dev-qt4

cd ~/local
git clone https://github.com/danfis/libccd.git
cd libccd
mkdir build 
cd build 
cmake -G "Unix Makefiles" ..
make
sudo make install

cd ~/local
git clone https://github.com/eigenteam/eigen-git-mirror.git
cd eigen-git-mirror
sudo cp Eigen /usr/include/

cd ~/local
git clone git://github.com/OctoMap/octomap.git
cd octomap
mkdir build
cd build
cmake ..
make
sudo make install

cd ~/local
git clone https://github.com/flexible-collision-library/fcl.git
cd fcl
mkdir build
cd build
cmake -DFCL_STATIC_LIBRARY=ON ..
make
sudo make install
```


## How to use

### Launch 

rosrun r2robotics_trajectory trajectory_node

roslaunch r2robotics_trajectory launcher.launch

### Set target positions

Via action (pour donner comme goal une pose X, Y, YAW)
```
rostopic pub /action_trajectory/goal r2robotics_actions/TargetPointActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  mode: 0
  pointID: 'pose'
  pose:
    x: 1000.0
    y: 1000.0
    yaw: 0.0
  self_rotation_only: false" 
```

Via service pour donner comme goal le nom d'un point du graphe :
```
rosservice call /trajectory_go_to_point "id: nom_du_point_du_graphe''
cap_needed: false
self_rotation_only: false"
```

### Set current position

Par exemple :
```
rosservice call /trajectory_set_position "x: 1710
y: 2390
theta: 0" 
```

### Calculate trajectory and run

rosservice call /calculate_trajectory

Et à chaque fois que l'asservissement fini, il doit appeler le service /waypoint_reached
Sinon, l'appeler à la main :

rosservice call /waypoint_reached
