#include <r2robotics_trajectory/trajectory_planner.h>
#include <r2robotics_trajectory/colors.h>

using namespace std;

TrajectoryPlanner::TrajectoryPlanner( ros::NodeHandle node ):
  node_name( ros::this_node::getName() )
  ,nh ( node )
  ,mapManager ( node )
  ,targetpoint_server( node, "action_trajectory", false )
  ,consigne_server( node, "action_consigne", false )
  ,targetpoint_client( node, "action_trajectory", true )
{
  ROS_TRAJ_STREAM("Trajectory Planner : starting init");
  initTopics();
  initServices();
  initActions();
  ERROR_STOP = false;
  waypointPose = Pose(-1,-1,0);

  GAZEBO = false;
  if( nh.getParam("USE_GAZEBO", GAZEBO) )
  {
    if(GAZEBO)
    {
      ROS_TRAJ_STREAM("Using GAZEBO simulation");
    }
  }

  ROS_TRAJ_STREAM("Trajectory Planner : topics, services and actions initialized");
}

//
// INITIALIZATION METHODS
//

void TrajectoryPlanner::initPose( int x, int y, double yaw )
{
  setCurrentPose(x,y,yaw);
  mapManager.initPose( Pose(x, y, yaw) );
  currentAction = Nothing;
  simulatorSetPosition(x,y,yaw);
}

void TrajectoryPlanner::initTopics()
{
    // Connects to topics for publishing
  trajectory_topic = nh.advertise<r2robotics_msgs::Movement>("global_control", 1000);
  velocity_topic = nh.advertise<r2robotics_msgs::Velocity>("velocity_control", 1000);
  simulator_pose_publish = nh.advertise<gazebo_msgs::LinkState>("gazebo/set_link_state", 1);

  // Connects to topics for listening
  // target_position_topic = nh.subscribe("target_position", 1000, &TrajectoryPlanner::targetPositionCallback, this);
  current_position_topic = nh.subscribe("odometry_feedback", 1, &TrajectoryPlanner::currentPositionCallback, this);
  // simu_pose = nh.subscribe("gazebo/link_states", 1, &TrajectoryPlanner::linkStatesCallback, this);
  simu_pose = nh.subscribe("simulation_pose", 1000, &TrajectoryPlanner::linkStatesCallback, this);

  ROS_TRAJ_STREAM("Trajectory Node : topics initialized");
}

void TrajectoryPlanner::initServices()
{
  // Connects to services to call
  // get_ennemies_positions_client = nh.serviceClient<r2robotics_srvs::GetEnnemiesPositions>("get_ennemies_positions");
  // get_table_elements_client = nh.serviceClient<r2robotics_srvs::GetEnnemiesPositions>("get_table_elements");
  set_position_firmware_client = nh.serviceClient<r2robotics_srvs::set_position>("setPosition");
  stop_control_client = nh.serviceClient<r2robotics_srvs::stopControl>("stop_control");
  resume_control_client = nh.serviceClient<r2robotics_srvs::resumeControl>("resume_control");

  // Create service
  // update_ennemies_position_service = nh.advertiseService("update_ennemies_positions", &TrajectoryPlanner::triggerUpdateEnnemiesPositions, this);
  waypoint_reached = nh.advertiseService("waypoint_reached", &TrajectoryPlanner::triggerNextMovement, this);
  // trajectory_consigne = nh.advertiseService("trajectory_consigne", &TrajectoryPlanner::serviceTrajectoryConsigne, this);
  set_position = nh.advertiseService("trajectory_set_position", &TrajectoryPlanner::trajectory_set_position, this);
  go_to_point = nh.advertiseService("trajectory_go_to_point", &TrajectoryPlanner::trajectory_go_to_point, this);
  as_if_done = nh.advertiseService("trajectory_as_if_done", &TrajectoryPlanner::trajectory_as_if_done, this);
  // get_consigne = nh.advertiseService("get_consigne", &TrajectoryPlanner::triggerGetConsigne, this);
  set_ERROR_STOP_srv = nh.advertiseService("trajectory_set_error_stop", &TrajectoryPlanner::set_ERROR_STOP, this);

  ros::service::waitForService("setPosition", ros::Duration(0.1));
  ros::service::waitForService("stop_control", ros::Duration(0.1));
  ros::service::waitForService("resume_control", ros::Duration(0.1));

  ROS_TRAJ_STREAM("Trajectory Node : services initialized");
}

void TrajectoryPlanner::initActions()
{
  ROS_TRAJ_STREAM("Trajectory Node : starting actions initialization");
  targetpoint_server.registerGoalCallback( boost::bind( &TrajectoryPlanner::targetpoint_goal_CB, this ) );
  targetpoint_server.registerPreemptCallback(boost::bind(&TrajectoryPlanner::targetpoint_preempt_CB, this));

  consigne_server.registerGoalCallback( boost::bind( &TrajectoryPlanner::consigne_goal_CB, this ) );
  consigne_server.registerPreemptCallback(boost::bind(&TrajectoryPlanner::consigne_preempt_CB, this));
  
  targetpoint_server.start();
  consigne_server.start();

  targetpoint_client.waitForServer();
  ROS_TRAJ_STREAM("Trajectory Node : actions initialized");
}

bool TrajectoryPlanner::loadGraph()
{
  return mapManager.loadGraph();
}

bool TrajectoryPlanner::loadRobot()
{
  return mapManager.loadRobot();
}

bool TrajectoryPlanner::loadTable()
{
  return mapManager.loadTable();
}

//
//
//

bool TrajectoryPlanner::targetReached()
{
  if( close( currentPose, targetPose ) )
  {
    return true;
  }
  return false;
}

//
// Process next action
//

void TrajectoryPlanner::loop()
{
    std::clock_t begin = std::clock();

    // Current action has already been sent and is being executed
    if (action_sent)
      return;
    // Next action is nothing
    else if (currentAction ==  Nothing)
      return;
    // Next action is a relative movement
    else if (currentAction == Move)
    {
      // Send Movement msg
      trajectory_topic.publish(consigne_goal.command);
    }
    // Next action is an absolute movement
    else if (currentAction == GoTo)
    {
        if( targetpoint_goal.mode == GLOBAL_PATH_PLANNING )
        {
          // Get target point that comes from Strategy
          string targetPoint = targetpoint_goal.pointID;
          // If movement is a pure self-rotation :
          if ( targetpoint_goal.self_rotation_only )
          {
            // Send Movement msg with rotation mode
            r2robotics_msgs::Movement msg;
            msg.mode = 1; // mode ROTATION = 1
            Node targetnode = node( targetPoint );
            if( targetnode.id == "" )
            {
              ROS_WARN_STREAM("Go to node of id '"<< targetPoint<<"' but node does not exist");
              return;
            }
            msg.final_yaw = targetnode.yaw; // get yaw of target point
            msg.stop = false;
            trajectory_topic.publish( msg );
          }
          // If movement is not a pure self-rotation :
          else
          {
            // Send Movement
            //ROS_TRAJ_STREAM("Trajectory Action : go to %i, %i, %i", target_x, target_y, target_cap);
            mapManager.setGoalPoint( targetPoint );
            currentTrajectory = mapManager.calculateAndGetGlobalTrajectory();
            sendNextMovement();
          }
          action_sent = true;
        }
        else if( targetpoint_goal.mode == LOCAL_PATH_PLANNING )
        {
          // check if we reached the target pose
          if( targetReached() && !already_reached)
          {
            ROS_TRAJ_STREAM(">>>>> Trajectory planner : target reached !!!!!");
            Velocity vel;
            vel.dx, vel.dy, vel.dyaw = 0.0, 0.0, 0.0;
            velocity_topic.publish( vel );
            targetpoint_server.setSucceeded();
            already_reached = true;
            // send "keep pose" to control TODO
            return;
          }
          // we didn't reach the target pose
          // calculate the new velocity to send to the control system
          Pose targetPose;
          if( targetpoint_goal.pointID == "pose" )
          {
            targetPose = targetpoint_goal.pose;
          }
          else
          {
            Node targetnode = node( targetpoint_goal.pointID );
            if( targetnode.id == "" )
            {
              ROS_WARN_STREAM("Go to node of id '"<< targetpoint_goal.pointID<<"' but node does not exist");
              return;
            }
            targetPose = Pose( targetnode.x, targetnode.y, targetnode.yaw );
          }
          mapManager.setGoalPoint( targetPose );
          ROS_DEBUG("calculateAndGetVelocity");
          currentVelocity = mapManager.calculateAndGetVelocity();
          ROS_DEBUG("calculateAndGetVelocity done");
          velocity_topic.publish( currentVelocity );
        }
    }

    clock_t end = std::clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    // std::cout << "TRAJECTORY LOOP : " << elapsed_secs << " s" << "\n";
        
    return;
}

bool TrajectoryPlanner::sendNextMovement()
{
  if( ERROR_STOP )
  {
    return false;
  }
  if (currentTrajectory.size() == 0)
  {
    // The current trajectory has been completely executed
    ROS_TRAJ_STREAM("Trajectory READY");
    // Set current action to succeeded
    if(currentAction == GoTo)
    {
      ROS_TRAJ_STREAM("GOTO SUCCEEDED");
      targetpoint_server.setSucceeded();
    }
    else if (currentAction == Move)
    {
      ROS_TRAJ_STREAM("MOVE SUCCEEDED");
      consigne_server.setSucceeded();
    }
    // Reset current action to nothing
    currentAction = Nothing;
    return false;
  }
  else if ( currentTrajectory.size() > 0 )
  {
    // There is at least one path not executed in the trajectory
    // Publish next movement if it is not static
    if( !isMovementStatic( currentTrajectory.at(0) ) )
    {
      ROS_TRAJ_STREAM("Current pose : " << currentPose.x << ", " << currentPose.y << ", " << currentPose.yaw);
      ROS_TRAJ_STREAM("SENDING NEXT PATH : " << currentTrajectory.at(0).mode );

      // ROS_TRAJ_STREAM("a : " << currentTrajectory.at(0).position_to_reach[0]*1000 );
      // ROS_TRAJ_STREAM("b : " << currentTrajectory.at(0).position_to_reach[1]*1000 );
      // ROS_TRAJ_STREAM("c : " << RAD2DEG(currentTrajectory.at(0).final_yaw ) );

      waypointPose = Pose( currentTrajectory.at(0).position_to_reach[0]*1000, // in mm
                          currentTrajectory.at(0).position_to_reach[1]*1000, // in mm
                          RAD2DEG(currentTrajectory.at(0).final_yaw) );
      if (  currentTrajectory.at(0).mode == 1 ) // ROTATION
      {
        ROS_TRAJ_STREAM( "Rotation, final yaw = " << RAD2DEG(currentTrajectory.at(0).final_yaw) );
      }
      trajectory_topic.publish( currentTrajectory.at(0) );
    }
    else
    {
      ROS_WARN("Movement static ! Not sending");
    }
    ROS_TRAJ_STREAM("size currentTrajectory : " << (int)currentTrajectory.size());
    // Remove from the trajectory the path that has been sent
    currentTrajectory.erase(currentTrajectory.begin());
    return true;
  }
  return false;
}

bool TrajectoryPlanner::isMovementStatic( r2robotics_msgs::Movement move)
{
  // ROS_TRAJ_STREAM("static ? " << move.position_to_reach[0] << " - " << currentPose.x );
  // ROS_TRAJ_STREAM("static ? " << move.position_to_reach[1] << " - " << currentPose.y );
  // ROS_TRAJ_STREAM("static ? " << move.final_yaw << " - " << currentPose.yaw );
  if( move.mode == 0 ) // STRAIGHT
  {
    if( close( move.position_to_reach[0], move.position_to_reach[1],
      currentPose.x, currentPose.y ) )
    {
      return true;
    }
  }
  // if( move.mode == 1 ) // ROTATION
  // {
  //   if( closecap( RAD2DEG(move.final_yaw), currentPose.yaw ) )
  //   {
  //     return true;
  //   }
  // }
  return false;
}

bool TrajectoryPlanner::close( double x1, double x2 )
{
  return close(x1,x2, EPSILON_XY);
}

bool TrajectoryPlanner::closecap( double cap1, double cap2 )
{
  return CLOSECAP( cap1, cap2, EPSILON_CAP );
}

bool TrajectoryPlanner::close( Pose p1, Pose p2 )
{
  if( close( p1.x, p2.x )
    && close( p1.y, p2.y )
    && closecap( p1.yaw, p2.yaw) )
    {
      return true;
    }
    return false;
}

bool TrajectoryPlanner::close( double x1, double x2, double delta )
{
  return fabs(x1-x2) < delta;
}

bool TrajectoryPlanner::close( double x1, double y1, double x2, double y2)
{
  Pose p1(x1,y1,0);
  Pose p2(x2,y2,0);
  return close(p1,p2);
}

bool TrajectoryPlanner::checkIfTheRobotIsApproximatelyWhereWeWant()
{
  // check if the robot is not too far from the objective waypoint
  double DISTANCE_MAX = 100; // 10cm
  if( waypointPose.x < 0 && waypointPose.y < 0)
  {
    // waypointPose has not been initialized, we ignore
    return true;
  }
  if( close(currentPose.x, waypointPose.x, DISTANCE_MAX)
      && close(currentPose.y, waypointPose.y, DISTANCE_MAX) )
  {
    return true;
  }
  return false;
}

//
// Callback functions for topics
//

void TrajectoryPlanner::currentPositionCallback(const r2robotics_msgs::Position pos)
{
  // ROS_TRAJ_STREAM("Traj planner : set current pose");
  int x = (int)(pos.x);
  int y = (int)(pos.y);
  double yaw = DEG_TO_180(RAD2DEG(pos.theta)); // theta en micro radian
  setCurrentPose(x, y, yaw);
  simulatorForceZPose();
}

void TrajectoryPlanner::linkStatesCallback(const gazebo_msgs::LinkStates feedback)
{
    // ROS_TRAJ_STREAM("Trajectory Node : link states callback !");
    if( feedback.name.size() == 0)
    {
      return;
    }
    int i = -1;
    int N = feedback.name.size();
    bool stop = false;
    while(i < N && !stop)
    {
        i++;
        if (feedback.name[i] == "gr::base_link")
        {
            stop = true;
        }
    }
    double x = (feedback.pose[i].position.x)*1000.0;
    double y = (feedback.pose[i].position.y)*1000.0;
    double yaw = RAD2DEG( asin(sqrt(feedback.pose[i].orientation.x * feedback.pose[i].orientation.x
                + feedback.pose[i].orientation.y * feedback.pose[i].orientation.y
                + feedback.pose[i].orientation.z * feedback.pose[i].orientation.z))*2 );
    // ROS_TRAJ_STREAM("callback : " << x << ", " << y << " - " << yaw);
    setCurrentPose(x, y, yaw);
}

//
// Callback functions for services
//

bool TrajectoryPlanner::set_ERROR_STOP ( std_srvs::SetBool::Request& req, std_srvs::SetBool::Response& res )
{
  ERROR_STOP = req.data;
  return true;
}

bool TrajectoryPlanner::triggerNextMovement ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
{
  ROS_TRAJ_STREAM("WAYPOINT REACHED TRIGGERED");
  if( checkIfTheRobotIsApproximatelyWhereWeWant() )
  {
    res.success = sendNextMovement();
  }
  else
  {
    res.success = false;
    ERROR_STOP = true; // DONT DO ANY MORE MOVEMENT
    ROS_WARN_STREAM("Robot too far from waypoint ! current (" << currentPose.x << ", " << currentPose.y
                    << ") instead of ("<< waypointPose.x << ", " << waypointPose.y << ")");
  }
  return true;
}

bool TrajectoryPlanner::trajectory_set_position ( r2robotics_srvs::set_position::Request& req, r2robotics_srvs::set_position::Response& res ) {
  // set position in table
  int x = (int)req.x;
  int y = (int)req.y;
  double yaw = (double)req.theta;
  ROS_TRAJ_STREAM( "---> Service Set Position : " << x << ", " << y << ", " << yaw );
  setCurrentPose(x,y,yaw);
  firmwareSetPosition(x,y,yaw);
  simulatorSetPosition(x,y,yaw);
  // ROS_DEBUG_STREAM("Distance to obstacles : " << mapManager.printDistanceToObstacles() );
  return true;
}

void TrajectoryPlanner::setCurrentPose(int x, int y, double yaw)
{
  currentPose.update(x, y, yaw);
  mapManager.setCurrentPose( Pose(x, y, yaw) );
  // firmwareSetPosition(x,y,yaw);
  // ROS_TRAJ_STREAM("Trajectory heard current position: %i, %i, %f", x, y, yaw );
}

void TrajectoryPlanner::firmwareSetPosition( int x, int y, double yaw )
{
  r2robotics_srvs::set_position srv;
  srv.request.x = x; // millimetre
  srv.request.y = y; // millimetre
  srv.request.theta = DEG2RAD(yaw); // radian
  // ROS_TRAJ_STREAM("Firmware set position : %i, %i, %i", srv.request.x, srv.request.y, srv.request.theta );
  set_position_firmware_client.call(srv);
}

void TrajectoryPlanner::simulatorSetPosition(double x, double y, double yaw)
{
  if(GAZEBO)
  {
    gazebo_msgs::LinkState msg;
    msg.link_name = "gr::base_link";
    msg.pose.position.x = (x/1000.0);
    msg.pose.position.y = (y/1000.0);
    msg.pose.position.z = 0.01;
    tf::Quaternion q;
    q.setEuler(0, 0, DEG2RAD(yaw));
    msg.pose.orientation.x = q.x();
    msg.pose.orientation.y = q.y();
    msg.pose.orientation.z = q.z();
    msg.pose.orientation.w = q.w();
    simulator_pose_publish.publish(msg);
  }
}

void TrajectoryPlanner::simulatorForceZPose()
{
  simulatorSetPosition(currentPose.x, currentPose.y, currentPose.yaw);
}

bool TrajectoryPlanner::trajectory_go_to_point ( r2robotics_srvs::GoToPoint::Request& req, r2robotics_srvs::GoToPoint::Response& res )
{
  // set position in table
  r2robotics_actions::TargetPointGoal goal;
  goal.pointID = req.id;
  goal.cap_needed = req.cap_needed;
  goal.self_rotation_only = req.self_rotation_only;
  goal.mode = 1;
  targetpoint_client.sendGoal( goal );
  ROS_TRAJ_STREAM("---> Service Go To Point : " << req.id << ", cap_needed = "
    << req.cap_needed << ", self_rotation_only = " << req.self_rotation_only);
  return true;
}

bool TrajectoryPlanner::trajectory_as_if_done ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
{
  ROS_TRAJ_STREAM("(SIMULATING) WAYPOINT REACHED TRIGGERED");
  int x = (int)waypointPose.x;
  int y = (int)waypointPose.y;
  double yaw = (double)waypointPose.yaw;
  ROS_TRAJ_STREAM( "---> Service Set Position : " << x << ", " << y << ", " << yaw );
  setCurrentPose(x,y,yaw);
  firmwareSetPosition(x,y,yaw);
  simulatorSetPosition(x,y,yaw);
  sendNextMovement();
  return true;
}

//
// Callback functions for actions
//

void TrajectoryPlanner::targetpoint_goal_CB ()
{
  //if (currentTrajectory.size() == 0) {
    ROS_TRAJ_STREAM("Setting target point");
    // accept the goal
    action_sent = false;
    currentAction = GoTo;
    if( targetpoint_server.isActive() )
    {
      targetpoint_server.setAborted();
    }
    // targetpoint_goal = *targetpoint_server.acceptNewGoal();
    targetpoint_goal = *targetpoint_server.acceptNewGoal();
    already_reached = false;
    ROS_TRAJ_STREAM("New TargetPointGoal accepted : " << targetpoint_goal.pointID);
    if( targetpoint_goal.mode == LOCAL_PATH_PLANNING )
    {
      // calculate the new velocity to send to the control system
      if( targetpoint_goal.pointID == "pose" )
      {
        targetPose = Pose(targetpoint_goal.pose);
        ROS_TRAJ_STREAM("New target pose set : " << targetPose.x << ", " << targetPose.y << " - " << targetPose.yaw);
      }
      else
      {
        ROS_TRAJ_STREAM("Accepting pointID : " << targetpoint_goal.pointID);
        Node targetnode = node( targetpoint_goal.pointID );
        targetPose = Pose( targetnode.x, targetnode.y, targetnode.yaw );
      }
    }
    ROS_TRAJ_STREAM("New target point set");
  //}
}

void TrajectoryPlanner::targetpoint_preempt_CB ()
{
  // ROS_DEBUG("%s: Preempted", action_name_.c_str());
  targetpoint_server.setPreempted();
}

void TrajectoryPlanner::consigne_goal_CB ()
{
  action_sent = false;
  currentAction = Move;
  consigne_goal = *consigne_server.acceptNewGoal();
}

void TrajectoryPlanner::consigne_preempt_CB ()
{
  // ROS_DEBUG("%s: Preempted", action_name_.c_str());
  consigne_server.setPreempted();
}

Node TrajectoryPlanner::node ( string id )
{
  return mapManager.node( id );
}