#include "ros/ros.h"
#include <ros/package.h>
#include <sstream>
#include <r2robotics_trajectory/trajectory_planner.h>
#include <r2robotics_trajectory/colors.h>

using namespace std;

#define CALCULATE_COSTS false
#define WRITE_ROBOT_MEASURES false
#define LOAD_COSTS false
#define LOAD_ROBOT_MEASURES false

int main(int argc, char **argv)
{
  ros::init(argc, argv, "trajectory");
  ros::NodeHandle n;

  clock_t start;
  double duration;
  start = clock();

  ROS_TRAJ_STREAM("Starting node Trajectory ......");

  // get color

  int x_init = 215;
  int y_init = 245;
  double yaw_init = 29.18;

  TrajectoryPlanner planner(n);
  planner.loadRobot();
  planner.loadTable();
  planner.loadGraph();
  planner.initPose(x_init, y_init, yaw_init); // those positions will be set to the right values by the strategy
  ROS_TRAJ_STREAM("Planner initialized");

  double freq;
  if ( n.getParam("trajectory_frequency", freq) ) {
    ROS_TRAJ_STREAM("Trajectory rate = " << freq);
  }
  else {
    ROS_ERROR("Failed to get trajectory rate !");
  }
  ros::Rate loop_rate(freq);
  int loop_per_second = 1.0 / freq;
  int count = 0;

  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;

  ROS_TRAJ_STREAM("......... Node Trajectory started !");
  ROS_TRAJ_STREAM("INITIALISATION TIME: "<< duration);

  while (ros::ok())
  {
    // ROS_INFO("planner loop");
    if (count >= loop_per_second) {
      //planner.displayloop();
      count = 0;
    }
    planner.loop();
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }
  return 0;
}
