#include <r2robotics_trajectory/map_manager.h>
#include <r2robotics_trajectory/colors.h>

using namespace std;

MapManager::MapManager( ros::NodeHandle node )
{
    ROS_TRAJ_STREAM("Map Manager : starting init");
    nh = node;
    targetPointID = "";
    // collisionManager = CollisionManager::create();
    previousPose.update(0, 0, 0.0);
    currentPose.update(0, 0, 0.0);
    MIN_DISTANCE_CONTROL = 5; // 0.5 cm
    // double ksi, rho, eta;
    // if( nh.getParam("potential_field_KSI", ksi)
    //     && nh.getParam("potential_field_RHO", rho)
    //     && nh.getParam("potential_field_ETA", eta) )
    // {
    //     ROS_TRAJ_STREAM("Got parameter for potential field :");
    //     ROS_TRAJ_STREAM("KSI = " << ksi);
    //     ROS_TRAJ_STREAM("RHO = " << rho);
    //     ROS_TRAJ_STREAM("ETA = " << eta);
    //     potentialField.init( collisionManager, ksi, rho, eta );
    // }
    // else
    // {
    //     potentialField.init( collisionManager );
    // }

    if( !nh.getParam("OMNIDIRECTIONAL_ROBOT", OMNIDIRECTIONAL))
    {
        OMNIDIRECTIONAL = true; // default is big robot, omnidirectional
    }

    if( !nh.getParam("GRAPH_MIN_DISTANCE", GRAPH_MIN_DISTANCE))
    {
        GRAPH_MIN_DISTANCE = 4000; // 4m
    }
    ROS_TRAJ_STREAM("GRAPH_MIN_DISTANCE = " << GRAPH_MIN_DISTANCE);
    ROS_TRAJ_STREAM("Map Manager : init done !");
}

bool MapManager::initPose( Pose start )
{
    currentPose.update( start );
    // initialize target point at the start position
    // we need a node with id "start" in the graph
    targetPointID = "start";
}

bool MapManager::setCurrentPose( Pose newPose)
{
    // ROS_TRAJ_STREAM("Map Manager : set current pose");
    previousPose.update( currentPose );
    currentPose.update ( newPose );
    // collisionManager->setRobotPose( newPose );
}

bool MapManager::setGoalPoint( string pointID )
{
    targetPointID = pointID;
}

bool MapManager::setGoalPoint( Pose targetPose_ )
{
    targetPose = targetPose_;
}

std::string MapManager::printDistanceToObstacles()
{
    // return potentialField.printDistanceToObstacles( currentPose );
}

CollisionPartsPtr_t MapManager::loadCollisionParts( string prefix )
{
    CollisionPartsPtr_t geoms;
    int nb_parts;
    if ( nh.getParam("number_" + prefix + "_parts", nb_parts) )
    {
        for (int n=0; n<nb_parts; n++) {
            string part_type;
            if ( nh.getParam(prefix+ "_part_" + to_string(n) +"_type" , part_type) )
            {
                ROS_TRAJ_DEBUG_STREAM("Got " << prefix << " part "<< n);
                geoms.push_back( load3DModel( part_type, prefix+ "_part_" + to_string(n) ) );
            }
            else
            {
                ROS_WARN_STREAM("Failed to get " << prefix << " part "<< n << " : " << part_type);
            }
        }
        return geoms;
    }
}

CollisionPartPtr_t MapManager::load3DModel( string type, string prefix )
{
    if( type == "mesh" )
    {
        string package;
        string file;
        if ( nh.getParam(prefix+ "_package" , package )
            &&  nh.getParam(prefix+ "_file" , file ) )
        {
            // ROS_TRAJ_STREAM("Load mesh file !");
            return loadModelFile( package, file );
        }
        else
        {
            ROS_WARN_STREAM("Failed to get " << prefix << " package/file location");
        }
    }
    else if( type == "half_space" )
    {
        double x, y, z, d;
        if ( nh.getParam(prefix+ "_x" , x )
            &&  nh.getParam(prefix+ "_y" , y )
            &&  nh.getParam(prefix+ "_z" , z )
            &&  nh.getParam(prefix+ "_d" , d ) )
        {
            ROS_TRAJ_STREAM("Load half_space ! " << x << " " << y << " " << z << " - " << d);
            HalfspacePtr_t half_space ( new Halfspace_t (Vector3d(x,y,z), d) );
            CollisionPartPtr_t colPart = CollisionPart::create( 
                "half_space", "half_space", half_space );
            return colPart;
        }
        else
        {
            ROS_WARN_STREAM("Failed to get part : " << type << "of " << prefix);
        }
    }
    else if( type == "box" )
    {
        double x, y, z, size_x, size_y, size_z, theta;
        if ( nh.getParam(prefix+ "_x" , x )
            &&  nh.getParam(prefix+ "_y" , y )
            &&  nh.getParam(prefix+ "_size_x" , size_x )
            &&  nh.getParam(prefix+ "_size_y" , size_y ) 
            &&  nh.getParam(prefix+ "_size_z" , size_z ) )
        {
            if(  !nh.getParam(prefix+ "_z" , z ) )
                z = 0;

            if( nh.getParam(prefix+ "_theta" , theta ) )
                theta = 0;
            ROS_TRAJ_DEBUG_STREAM("Load box !");
            BoxPtr_t box (new fcl::Box<double> (size_x, size_y, size_z));
            Pose p(x,y,theta);
            TransformD init_pose = CollisionManager::poseToTransform(p);
            CollisionPartPtr_t colPart = CollisionPart::create( 
                "box", "box", box, init_pose );
            return colPart;
        }
        else
        {
            ROS_WARN_STREAM("Failed to get part : " << type << " of " << prefix);
        }
    }
    else if( type == "cylinder" )
    {
        double x, y, z, r, h;
        if ( nh.getParam(prefix+ "_x" , x )
            &&  nh.getParam(prefix+ "_y" , y )
            &&  nh.getParam(prefix+ "_z" , z )
            &&  nh.getParam(prefix+ "_r" , r )
            &&  nh.getParam(prefix+ "_h" , h ) )
        {
            ROS_TRAJ_DEBUG_STREAM("Load cylinder !");
            CylinderPtr_t cyl (new fcl::Cylinder<double> (r, h));
            TransformD init_pose = TransformD::Identity();
            init_pose.translation() = Vector3d(x, y, z);
            CollisionPartPtr_t colPart = CollisionPart::create( 
                "cylinder", "cylinder", cyl, init_pose );
            return colPart;
        }
        else
        {
            ROS_WARN_STREAM("Failed to get part : " << type << " of " << prefix);
        }
    }
}

CollisionPartPtr_t MapManager::loadModelFile( string package, string filename )
{
    string path = ros::package::getPath(package);
    string filepath = path + "/meshes/" + filename;
    
    std::cout << "Loading file : " << filepath << std::endl;

    string delimiter = ".";
    string name = filename.substr(0, filename.find(delimiter));

    std::vector<Vector3d> vertices;
    std::vector<fcl::Triangle> triangles;
    loadOBJFile(filepath.c_str(), vertices, triangles);
    std::shared_ptr<Model> model( new Model() );
    int result;
    result = model->beginModel();
    if(result != fcl::BVH_OK) {std::cout << "FAILURE\n";}
    result = model->addSubModel(vertices, triangles);
    if(result != fcl::BVH_OK) {std::cout << "FAILURE\n";}
    result = model->endModel();
    if(result != fcl::BVH_OK) {std::cout << "FAILURE\n";}
    std::string type( "mesh" );
    CollisionPartPtr_t colPart = CollisionPart::create( name, type, model );
    return colPart;
}

bool MapManager::loadRobot()
{
    ROS_TRAJ_STREAM("...... LOADING ROBOT (for collision detection) ......");
    CollisionPartsPtr_t geoms = loadCollisionParts("robot");
    for( auto colPart : geoms )
    {
        // collisionManager->addRobotPart( colPart );
    }
}

bool MapManager::loadTable()
{
    ROS_TRAJ_STREAM("...... LOADING TABLE (for collision detection) ......");
    CollisionPartsPtr_t geoms = loadCollisionParts("table");
    for( auto colPart : geoms )
    {
        // collisionManager->addEnvPart( colPart );
    }
}

bool MapManager::loadGraph()
{
    // Get nodes and edges of the graph from ROS params
    points = loadGraphNodes();
    astar = AStar( points );
    vector< Edge > edges = loadGraphEdges();
    // Create AStar (and graph)
    astar = AStar( edges, points );
    ROS_TRAJ_STREAM("ASTAR initialized");
}

map< string, Node > MapManager::loadGraphNodes()
{
  ROS_TRAJ_STREAM("...... LOADING GRAPH POINTS .....");
  int nb_points;
  map< string, Node > graph_points;
  if ( nh.getParam("number_graph_points", nb_points) )
  {
    ROS_TRAJ_STREAM("Got " << nb_points << " graph points");
  }
  else {
    ROS_ERROR_STREAM("Failed to get graph points !");
    return graph_points;
  }
  for (int n=0; n<nb_points; n++) {
    double x = 0;
    double y = 0;
    double yaw = 0;
    bool yaw_needed = true;
    string id;
    if ( nh.getParam("graph_point_" + to_string(n) +"_x" , x) &&
            nh.getParam("graph_point_" + to_string(n) +"_y" , y) &&
            nh.getParam("graph_point_" + to_string(n) +"_id" , id)
            )
    {
        if( ( !nh.getParam("graph_point_" + to_string(n) +"_yaw_needed" , yaw_needed)
                | yaw_needed )
            && !nh.getParam("graph_point_" + to_string(n) +"_yaw" , yaw) )
        {
            ROS_WARN_STREAM("Failed to get graph point " << n << " : please precise yaw or if yaw is not needed");
        }
        if( yaw_needed )
        {
            ROS_TRAJ_STREAM("Got graph point "<<n<<" : "<<id<<" - "<<x<<", "<<y<<", "<<yaw);
        }
        else
        {
            ROS_TRAJ_STREAM("Got graph point "<<n<<" : "<<id<<" - "<<x<<", "<<y<<", yaw not needed");
        }
    }
    else {
      ROS_WARN_STREAM("Failed to get graph point " << n);
    }
    Node no = Node(id,x,y,yaw);
    no.set_final_yaw_needed(yaw_needed) ;
    graph_points.emplace( id, no);
    // updateCost(x,y,3); // pour la visu // TODO
  }
  ROS_TRAJ_STREAM("Got all graph points : good !");
  return graph_points;
}

vector< Edge > MapManager::loadNeighborNodesEdges( int i )
{
    vector< Edge > edges;
    int nb;
    if ( nh.getParam("graph_point_" + to_string(i) +"_number_neighbors" , nb) )
    {
        ROS_TRAJ_STREAM("Node " << i << " has " << nb << " neighbor");
        // getting the n neighbors
        for (int j=0; j<nb; j++)
        {
            string id1;
            string id2;
            if ( nh.getParam("graph_point_" + to_string(i) +"_neighbor_" + to_string(j) , id2) &&
                    nh.getParam("graph_point_" + to_string(i) +"_id" , id1) &&
                    id1 != id2 )
            {
                bool unidirectional = false;
                nh.getParam("graph_point_" + to_string(i) +"_neighbor_" + to_string(j) + "_unidirectional" , unidirectional );
                Node n1 = astar.node(id1);
                Node n2 = astar.node(id2);
                if( n1.id == "" )
                {
                    ROS_ERROR_STREAM("INCORRECT NEIGHBOR : no node '" << id1 << "'");
                    // ERROR
                }
                else if( n2.id == "" )
                {
                    ROS_ERROR_STREAM("INCORRECT NEIGHBOR : no node '" << id2 << "'");
                    // ERROR
                }
                if( n1.x == n2.x && n1.y == n2.y && n1.yaw == n2.yaw)
                {
                    ROS_WARN_STREAM("Warning : edge (" << j << ") for identical nodes ! " << id1 << " & " << id2);
                }
                else if( n1.x == n2.x && n1.y == n2.y && n1.yaw != n2.yaw)
                {
                    ROS_TRAJ_STREAM("Got rotation edge between nodes : " << id1 << " and " << id2);
                    edges.push_back( Edge(id1, id2, ROTATION) );
                    edges.push_back( Edge(id2, id1, ROTATION) );
                }
                else
                {
                    edges.push_back( Edge(id1, id2, STRAIGHT) );
                    if(!unidirectional)
                    {
                        edges.push_back( Edge(id2, id1, STRAIGHT) );
                        ROS_TRAJ_STREAM("Got straight edge between nodes : " << id1 << " and " << id2);
                    }
                    else
                    {
                        ROS_TRAJ_STREAM("Got (unilateral) straight edge between nodes : " << id1 << " and " << id2);
                    }
                }
            }
            else
            {
                ROS_WARN_STREAM("Failed to get id of neighbor nodes !");
            }
        }
    }
    else
    {
        // no neighbor
        ROS_TRAJ_STREAM("Node " << i << " has no neighbor");
    }
    return edges;
}

vector< Edge > MapManager::loadGraphEdges()
{
  ROS_TRAJ_STREAM("...... LOADING GRAPH EDGES ......");
  vector< Edge > graph_edges;

  int nb_nodes;
  if ( nh.getParam("number_graph_points", nb_nodes) )
  for( int i=0; i<nb_nodes; i++ )
  {
    vector< Edge > new_edges = loadNeighborNodesEdges(i);
    graph_edges.reserve(graph_edges.size() + distance(new_edges.begin(),new_edges.end()));
    graph_edges.insert(graph_edges.end(),new_edges.begin(),new_edges.end());
  }

  ROS_TRAJ_STREAM("...... LOADING MANUAL GRAPH EDGES ......");
  int nb_edges;
  if ( nh.getParam("number_graph_edges", nb_edges) ) {
    ROS_TRAJ_STREAM("Got " << nb_edges << " manual graph edges");
  }
  else {
    ROS_ERROR("Failed to get graph edges !");
    return graph_edges;
  }
  for (int n=0; n<nb_edges; n++) {
    string id1;
    string id2;
    string edge_type;
    if ( nh.getParam("graph_edge_" + to_string(n) +"_id1" , id1) &&
            nh.getParam("graph_edge_" + to_string(n) +"_id2" , id2) &&
            nh.getParam("graph_edge_" + to_string(n) +"_type" , edge_type) )
    {
        Node n1 = astar.node(id1);
        Node n2 = astar.node(id2);
        if( n1.id == "" )
        {
            ROS_ERROR_STREAM("INCORRECT NEIGHBOR : no node '" << id1 << "'");
            // ERROR
        }
        else if( n2.id == "" )
        {
            ROS_ERROR_STREAM("INCORRECT NEIGHBOR : no node '" << id2 << "'");
            // ERROR
        }
        if( edge_type == "arc_circle" )
        {
            int dir;
            double center_x;
            double center_y;
            if ( nh.getParam("graph_edge_" + to_string(n) +"_direction" , dir) &&
                 nh.getParam("graph_edge_" + to_string(n) +"_center_x" , center_x) &&
                 nh.getParam("graph_edge_" + to_string(n) +"_center_y" , center_y) )
            {
                ROS_TRAJ_STREAM("Got arc-circle edge " << n << " : " << id1 << " -- " << id2);
                graph_edges.push_back( Edge(id1,id2, CIRCLE, dir, center_x, center_y) );
            }
            else
            {
                ROS_WARN_STREAM("Failed to get arc-circle manual edge " << n);
            }
        }
        else
        {
            ROS_WARN_STREAM("Failed to get manual edge " << n << " (check the type)");
        }
    }
    else {
      ROS_WARN_STREAM("Failed to get manual graph edge " << n);
    }
  }
  ROS_TRAJ_STREAM("Got all manual graph edges : good !");
  return graph_edges;
}

Trajectory MapManager::calculateAndGetGlobalTrajectory()
{
  calculateGlobalTrajectory();
  return currentTrajectory;
}

Trajectory MapManager::getGlobalTrajectory()
{
    return currentTrajectory;
}

Velocity MapManager::calculateAndGetVelocity()
{
    calculateVelocity();
    return currentVelocity;
}

Velocity MapManager::getVelocity()
{
    return currentVelocity;
}

bool MapManager::calculateVelocity()
{
    ROS_TRAJ_DEBUG_STREAM("Getting trajectory with LOCAL method");
    currentVelocity = getOptimalVelocity( currentPose, targetPose );
}

Velocity MapManager::getOptimalVelocity( Pose current, Pose target )
{
    // std::cout << "Current pose : " << current << "\n";
    // potentialField.setGoal( target );
    // Pose du = potentialField.dU( current );
    Velocity vel;
    // vel.dx = du.x;
    // vel.dy = du.y;
    // vel.dyaw = du.yaw;
    return vel;
}

bool MapManager::calculateGlobalTrajectory()
{
    ROS_TRAJ_DEBUG_STREAM("Getting trajectory with GLOBAL method");
    string currentPointID = getCurrentPointID();

    if( currentPointID == targetPointID )
    {
        // we're close but maybe not exactly at the target point
        // so we create a temporary node at our current pose, link it to the target pose
        // and continue
        ROS_TRAJ_STREAM("Not quite at target point ! Creating a temporary new node ...");
        astar.addNode("newNode", currentPose.x,currentPose.y, currentPose.yaw, targetPointID );
        currentPointID = "newNode";
    }
    // ROS_TRAJ_STREAM("currentPointID : " << currentPointID);
    // ROS_TRAJ_STREAM("target : " << targetPointID);
    // ROS_TRAJ_STREAM("Target point not current point");
    astar.search( currentPointID, targetPointID );
    // ROS_TRAJ_STREAM("test");
    NodeTrajectory nodetraj = astar.search( currentPointID, targetPointID );
    printNodeTrajectory( nodetraj );
    EdgeTrajectory edgetraj = astar.nodeTrajectoryToEdgeTrajectory( nodetraj );
    Trajectory traj = edgeTrajectoryToMovementTrajectory( edgetraj );

    printTrajectory( traj );

    currentTrajectory.clear();
    currentTrajectory = traj;
    
    astar.deleteNode("newNode");
}

void MapManager::printNodeTrajectory( NodeTrajectory nodetraj )
{
    ROS_TRAJ_STREAM("--- NODE TRAJECTORY ---");
    for(auto id: nodetraj)
    {
        ROS_TRAJ_STREAM(id);
    }
    ROS_TRAJ_STREAM("------------------------");
}

void MapManager::printTrajectory( Trajectory traj )
 {
    ROS_TRAJ_STREAM("-----  TRAJECTORY  -----");
    for(int idx=0; idx < traj.size(); idx++) {
        printMovement( traj.at(idx) );
    }
    ROS_TRAJ_STREAM("------------------------");
}

void MapManager::printMovement( r2robotics_msgs::Movement move ) {
    // if( move.stop == true )
    // {
    //     ROS_TRAJ_STREAM("Movement : STOP\n");
    //     return;
    // }
    if( move.mode == 0)
    {
        ROS_TRAJ_STREAM("Movement : STRAIGHT, position_to_reach " << move.position_to_reach[0] << " - " << move.position_to_reach[1] << " , final yaw " << move.final_yaw );
    }
    else if( move.mode == 1)
    {   
        ROS_TRAJ_STREAM("Movement : ROTATION, final yaw " << move.final_yaw );
    }
    else if( move.mode == 2)
    {
        ROS_TRAJ_STREAM("Movement : ARC CIRCLE, center " << move.rotation_center_position[0] << " - " << move.rotation_center_position[1] <<
        ", direction " << move.direction << ", final yaw " << move.final_yaw );
    }
}

Trajectory MapManager::edgeTrajectoryToMovementTrajectory( EdgeTrajectory edge_traj )
{
    // BEWARE
    // This method assumes the trajectory is starting at the CURRENT position of the robot
    // which is assimilated to the first node of the trajectory,
    // but the coordinates of the current position will be used for calculation
    Trajectory traj;
    int i = 0;
    for(auto e: edge_traj)
    {
        Node n1 = astar.node(e.n1);
        if( i == 0 ) // first edge of the trajectory
        {
            // we replace the first node coordinates by those of the current position
            // in case we're not exactly on the node
            n1.x = currentPose.x;
            n1.y = currentPose.y;
            n1.yaw = currentPose.yaw;
            i++;
        }
        Node n2 = astar.node(e.n2);
        float n2x = n2.x/1000.0; // in meters
        float n2y = n2.y/1000.0; // in meters
        float n1x = n1.x/1000.0; // in meters
        float n1y = n1.y/1000.0; // in meters
        float n2yaw = DEG2RAD(n2.yaw); // in radians
        float n1yaw = DEG2RAD(n1.yaw); // in radians
        ROS_TRAJ_STREAM("goal : " << n2.x << ", " << n2.y << " (" << n2x << ", " << n2y << ")");
        ROS_TRAJ_STREAM("Edge : " << e.n1 << " --> " << e.n2 << " -- type : " << e.type);
        Movement mv;
        mv.rotation_center_position.clear();
        mv.rotation_center_position.push_back(0);
        mv.rotation_center_position.push_back(0);
        mv.position_to_reach.clear();
        mv.stop = false;
        if( e.type == STRAIGHT)
        {
            float intermediate_yaw = n1yaw; // yaw at the start and end of the straight line (in radians)
            // if the robot is NOT OMNIDIRECTIONAL, we have to turn before going in a straight line to the goal point
            // we don't turn if we're already at the right angle (or +/- 180 degrees)
            if( fabs(n1.x - n2.x) >= MIN_DISTANCE_CONTROL | fabs(n1.y - n2.y) >= MIN_DISTANCE_CONTROL ) 
            {
                if( ! OMNIDIRECTIONAL )
                {
                    double temp1 = RAD2DEG(atan2( n2y - n1y, n2x - n1x ) ); // in radians
                    double temp2 = ADD_DEG(temp1, 180);
                    ROS_TRAJ_STREAM("intermediate yaw : " << temp1 << " or " << temp2);
                    if ( fabs( ADD_DEG(temp1, -n1.yaw) ) < fabs( ADD_DEG(temp2, -n1.yaw) ) )
                    {
                        intermediate_yaw = DEG2RAD( temp1 );
                    }
                    else
                    {
                        intermediate_yaw = DEG2RAD( temp2 );
                    }
                    // compare orientation to move with current orientation and make first rotation only if we have to
                    if( !CLOSECAP( RAD2DEG(intermediate_yaw), n1.yaw, EPSILON_CAP ) )
                    {
                        Movement first_rot = mv;
                        first_rot.final_yaw = intermediate_yaw ;
                        first_rot.mode = 1;
                        first_rot.position_to_reach.push_back(n1x);
                        first_rot.position_to_reach.push_back(n1y);
                        traj.push_back(first_rot);
                    }
                }
                // for omnidirectional and not omnidirectional robot, we goin a straight line to the goal poiint
                // except if we're already there
                    mv.mode = 0;
                    mv.final_yaw = intermediate_yaw;
                    mv.position_to_reach.push_back(n2x);
                    mv.position_to_reach.push_back(n2y);
                    traj.push_back(mv);
            }
            // for omnidirectional and not omnidirectional robot, we have to turn after the straight line
            // to align with the final yaw wanted (if the final yaw is needed)
            if( ! CLOSECAP( RAD2DEG(intermediate_yaw), n2.yaw, EPSILON_CAP) && // comparison in degrees
                n2.final_yaw_needed )
            {
                // add straight pass without changing the yaw then a rotation to change yaw
                Movement rot = mv;
                rot.final_yaw = n2yaw;
                rot.mode = 1;
                rot.position_to_reach.push_back(n2x);
                rot.position_to_reach.push_back(n2y);
                traj.push_back(rot);
            }
        }
        else if(e.type == ROTATION)
        {
            Movement rot = mv;
            rot.final_yaw = n2yaw;
            rot.mode = 1;
            rot.position_to_reach.push_back(n2x);
            rot.position_to_reach.push_back(n2y);
            traj.push_back(rot);
        }
        else if(e.type == CIRCLE)
        {
            mv.mode = 2;
            mv.rotation_center_position.clear();
            mv.rotation_center_position.push_back(e.center_x);
            mv.rotation_center_position.push_back(e.center_y);
            mv.position_to_reach.push_back(n2x);
            mv.position_to_reach.push_back(n2y);
            mv.direction = e.direction;
            mv.final_yaw = e.final_yaw;
            traj.push_back(mv);
        }
    }
    return traj;
}

Node MapManager::node( string id )
{
    return astar.node( id );
}

string MapManager::getCurrentPointID()
{  
    double d = 0;
    string id = astar.closestNode( currentPose, d );
    if(sqrt(d) < GRAPH_MIN_DISTANCE)
    {
        ROS_TRAJ_STREAM("Current pose : " << currentPose.x << ", " << currentPose.y << ", " << currentPose.yaw);
        ROS_TRAJ_STREAM("Closest point : " << id << " at " << sqrt(d) << " mm");
        return id;
    }
    // TODO : create a new node with edges and return its id
    return id;
}